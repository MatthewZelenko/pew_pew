Shader(vertex)
#version 300 es
precision highp float;

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec4 colour;

uniform mat4 u_viewProjection;

out vec2 vTexCoords;
out vec4 vColour;

void main()
{
    vColour = colour;
    vTexCoords = texCoords;
    gl_Position = u_viewProjection * vec4(vertex.x, vertex.y, vertex.z, 1);
}

Shader(fragment)
#version 300 es
precision highp float;

in vec2 vTexCoords;
in vec4 vColour;

uniform sampler2D u_texture0;

out vec4 fragColour;

void main()
{
    fragColour = texture(u_texture0, vTexCoords) * vColour;
    if(fragColour.a <= 0.001f)
        discard;
}