Shader(vertex)
#version 300 es
precision highp float;

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texCoords;

uniform mat4 u_mvp;

out vec2 vTexCoords;

void main()
{
    vTexCoords = texCoords;
    gl_Position = u_mvp * vec4(position, 0, 1);
}

Shader(fragment)
#version 300 es
precision highp float;

in vec2 vTexCoords;

uniform sampler2D u_texture0;

out vec4 fragColour;

void main()
{
    fragColour = texture(u_texture0, vTexCoords);
}