#include "CircleColliderComponent.h"

CircleColliderComponent::CircleColliderComponent() : OnCollision(nullptr)
{

}
CircleColliderComponent::~CircleColliderComponent()
{

}

void CircleColliderComponent::Load()
{
}
void CircleColliderComponent::Unload()
{
}

void CircleColliderComponent::Update(double a_deltaTime)
{
}
void CircleColliderComponent::FixedUpdate(double a_timeStep)
{
}

void CircleColliderComponent::SetTag(unsigned char a_tag)
{
    m_tag = a_tag;
}

void CircleColliderComponent::AddTag(unsigned char a_tag)
{
    m_tag |= a_tag;
}

void CircleColliderComponent::RemoveTag(unsigned char a_tag)
{
    m_tag &= ~a_tag;
}

bool CircleColliderComponent::HasTag(unsigned char a_tag)
{
    return m_tag & a_tag;
}

unsigned char CircleColliderComponent::GetTag()
{
    return m_tag;
}
void CircleColliderComponent::SetCircle(const Circle& a_circle)
{
    m_circle = a_circle;
}