#include "TransformComponent.h"
#include <glm/gtc/matrix_transform.hpp>

TransformComponent::TransformComponent(): m_position(glm::vec2(0.0f,0.0f)),
                                          m_scale(glm::vec2(-1.0f, -1.0f)),
                                          m_rotation(0.0f),
                                          m_zIndex(0)
{

}
TransformComponent::~TransformComponent()
{

}

void TransformComponent::Load()
{
}
void TransformComponent::Unload()
{
}

void TransformComponent::Update(double a_deltaTime)
{
    m_isSleeping = true;
}
void TransformComponent::FixedUpdate(double a_timeStep)
{
}

const glm::mat4 TransformComponent::GetModel() const
{
    //TODO::Store pos scale and rot in matrix instead
    glm::mat4 model(1.0f);

    model = glm::translate(model, glm::vec3(m_position.x, m_position.y, m_zIndex));
    model = glm::rotate(model, m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, glm::vec3(m_scale.x, m_scale.y, 0.0f));
    return model;
}