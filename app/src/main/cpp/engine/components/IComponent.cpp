#include "IComponent.h"
#include "Engine.h"
#include "EntityManager.h"
#include "IEntity.h"

IEntity* IComponent::GetParent()
{
    return Engine::Get()->GetEntityManager()->GetEntity<IEntity>(m_parentHandle);
}