#include "Log.h"
#include <glm/gtc/type_ptr.hpp>
#include "Engine.h"
#include "SpriteComponent.h"
#include "ResourceManager.h"

SpriteComponent::SpriteComponent() : m_isActive(true), m_currentSprite(0), m_shader(nullptr), m_colour(glm::vec4(1, 1, 1, 1)), m_isPlayingAnimation(false), m_frameElapse(0.0f)
{

}
SpriteComponent::~SpriteComponent()
{

}

void SpriteComponent::Load()
{
}

void SpriteComponent::Unload()
{
    auto iter = m_uniforms.begin();
    for (; iter != m_uniforms.end(); ++iter)
    {
        free(iter->second.m_data);
    }
    m_uniforms.clear();
    m_shader = nullptr;
    m_sprites.clear();
}

void SpriteComponent::FixedUpdate(double a_timeStep)
{
    if (m_isPlayingAnimation)
    {
        m_frameElapse += a_timeStep;
        int breakCounter = 0;   //TODO: Remove when fixed timestep is implemented
        while (m_frameElapse >= m_frameSpeeds[m_currentSprite])
        {
            breakCounter++;
            m_frameElapse -= m_frameSpeeds[m_currentSprite];
            m_currentSprite++;
            if (m_currentSprite >= m_sprites.size())
            {
                if (!m_isLooping)
                {
                    m_currentSprite = m_sprites.size() - 1;
                    m_isPlayingAnimation = false;
                }
                else
                {
                    m_currentSprite = 0;
                }
            }
            if(breakCounter >= 20)
            {
                m_frameElapse = 0.0f;
                break;
            }
        }
    }
}

void SpriteComponent::LoadAnimation(const std::string &a_name, bool a_play, bool a_isLooping)
{
    m_currentAnimationName = a_name;
    Animation animation = Engine::Get()->GetResourceManager()->GetAnimation(a_name);
    m_sprites = animation.GetSprites();
    m_frameSpeeds = animation.GetFrameSpeeds();
    if(a_play)
        Play(a_isLooping);
    m_currentSprite = 0;
    m_frameElapse = 0.0f;
}
void SpriteComponent::AddSprite(const std::string &a_name)
{
    m_sprites.push_back(Engine::Get()->GetResourceManager()->GetSprite(a_name));
}
void SpriteComponent::SetSprite(const std::string &a_name)
{
    m_sprites.clear();
    m_sprites.push_back(Engine::Get()->GetResourceManager()->GetSprite(a_name));
}

Sprite SpriteComponent::GetSprite() const
{
    if(m_sprites.empty() || m_currentSprite >= m_sprites.size())
        return Sprite();
    return m_sprites[m_currentSprite];
}

void SpriteComponent::SetShader(const std::string &a_name)
{
    m_shader = Engine::Get()->GetResourceManager()->GetShader(a_name);
}

void SpriteComponent::SetUniform(const std::string& a_name, const float a_data)
{
    size_t size = sizeof(float);
    const void* d = &a_data;
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void SpriteComponent::SetUniform(const std::string& a_name, const glm::vec2& a_data)
{
    size_t size = sizeof(glm::vec2);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT2;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void SpriteComponent::SetUniform(const std::string& a_name, const glm::vec3& a_data)
{
    size_t size = sizeof(glm::vec3);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT3;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void SpriteComponent::SetUniform(const std::string& a_name, const glm::vec4& a_data)
{
    size_t size = sizeof(glm::vec4);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT4;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void SpriteComponent::SetUniform(const std::string& a_name, const glm::mat4& a_data)
{
    size_t size = sizeof(glm::mat4);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::MAT4;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}

void SpriteComponent::Play(bool a_loop)
{
    m_isLooping = a_loop;
    m_isPlayingAnimation = (m_sprites.size() >= 2 && m_frameSpeeds[0] != 0);
    m_currentSprite = 0;
    m_frameElapse = 0.0f;
}
void SpriteComponent::Stop()
{
    m_isPlayingAnimation = false;
}