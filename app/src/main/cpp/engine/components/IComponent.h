#ifndef PEWPEW_ICOMPONENT_H
#define PEWPEW_ICOMPONENT_H

#include "Types.h"

class IEntity;

class IComponent
{
public:
    IComponent(){}
    virtual ~IComponent(){}

    virtual void Load(){}
    virtual void Unload(){}

    virtual void Update(double a_deltaTime){}
    virtual void FixedUpdate(double a_timeStep){}


    IEntity* GetParent();
    inline Handle GetParentHandle() const { return m_parentHandle; }
    inline Handle GetHandle() const { return m_handle; }

private:
    friend class ComponentManager;

    Handle m_handle;
    Handle m_parentHandle;
};


#endif //PEWPEW_ICOMPONENT_H