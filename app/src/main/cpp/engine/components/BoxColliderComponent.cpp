#include "BoxColliderComponent.h"

BoxColliderComponent::BoxColliderComponent() : OnCollision(nullptr)
{

}
BoxColliderComponent::~BoxColliderComponent()
{

}

void BoxColliderComponent::Load()
{
}
void BoxColliderComponent::Unload()
{
}

void BoxColliderComponent::Update(double a_deltaTime)
{
}
void BoxColliderComponent::FixedUpdate(double a_timeStep)
{
}

void BoxColliderComponent::SetTag(unsigned char a_tag)
{
    m_tag = a_tag;
}

void BoxColliderComponent::AddTag(unsigned char a_tag)
{
m_tag |= a_tag;
}

void BoxColliderComponent::RemoveTag(unsigned char a_tag)
{
    m_tag &= ~a_tag;
}

bool BoxColliderComponent::HasTag(unsigned char a_tag)
{
    return m_tag & a_tag;
}

unsigned char BoxColliderComponent::GetTag()
{
    return m_tag;
}

void BoxColliderComponent::SetRect(const Rect& a_rect)
{
    m_rect = a_rect;
}
