#include "Log.h"
#include <glm/gtc/type_ptr.hpp>
#include "Engine.h"
#include "FontComponent.h"
#include "ResourceManager.h"

FontComponent::FontComponent() : m_isActive(true), m_shader(nullptr), m_font(nullptr), m_spacing(0.0f), m_colour(glm::vec4(1, 1, 1, 1)), m_alignment(Alignment::LEFT)
{

}
FontComponent::~FontComponent()
{

}

void FontComponent::Load()
{
}

void FontComponent::Unload()
{
    auto iter = m_uniforms.begin();
    for (; iter != m_uniforms.end(); ++iter)
    {
        free(iter->second.m_data);
    }
    m_uniforms.clear();
    m_shader = nullptr;
    m_font = nullptr;
}


void FontComponent::SetFont(const std::string &a_name)
{
    m_font = Engine::Get()->GetResourceManager()->GetFont(a_name);

}
void FontComponent::SetShader(const std::string &a_name)
{
    m_shader = Engine::Get()->GetResourceManager()->GetShader(a_name);
}

void FontComponent::SetUniform(const std::string& a_name, const float a_data)
{
    size_t size = sizeof(float);
    const void* d = &a_data;
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void FontComponent::SetUniform(const std::string& a_name, const glm::vec2& a_data)
{
    size_t size = sizeof(glm::vec2);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT2;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void FontComponent::SetUniform(const std::string& a_name, const glm::vec3& a_data)
{
    size_t size = sizeof(glm::vec3);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT3;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void FontComponent::SetUniform(const std::string& a_name, const glm::vec4& a_data)
{
    size_t size = sizeof(glm::vec4);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::FLOAT4;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}
void FontComponent::SetUniform(const std::string& a_name, const glm::mat4& a_data)
{
    size_t size = sizeof(glm::mat4);
    const void* d = glm::value_ptr(a_data);
    if(m_uniforms.find(a_name) == m_uniforms.end())
    {
        UniformData data;
        data.m_data = malloc(size);
        data.m_type = UniformData::Type::MAT4;
        m_uniforms[a_name] = data;
    }
    memcpy(m_uniforms[a_name].m_data, d, size);
}