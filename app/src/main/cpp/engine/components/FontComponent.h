#ifndef PEWPEW_FONTCOMPONENT_H
#define PEWPEW_FONTCOMPONENT_H

#include <string>
#include <unordered_map>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <UniformData.h>
#include "Component.h"


class Shader;
class Font;

class FontComponent : public Component<FontComponent>
{
public:
    FontComponent();
    ~FontComponent();

    void Load() override;
    void Unload() override;

    void SetFont(const std::string& a_name);
    void SetShader(const std::string& a_shader);
    inline Shader* GetShader() const { return m_shader; }
    inline Font* GetFont() { return m_font; }
    inline const std::string& GetText(){return m_text;}
    inline void SetText(const std::string& a_text){m_text = a_text;}
    void SetSpacing(float a_value){m_spacing = a_value;}
    float GetSpacing(){return m_spacing;}

    void SetColour(const glm::vec4& a_colour) { m_colour = a_colour; }
    glm::vec4 GetColour() { return m_colour; }


    void SetUniform(const std::string& a_name, float a_data);
    void SetUniform(const std::string& a_name, const glm::vec2& a_data);
    void SetUniform(const std::string& a_name, const glm::vec3& a_data);
    void SetUniform(const std::string& a_name, const glm::vec4& a_data);
    void SetUniform(const std::string& a_name, const glm::mat4& a_data);

    inline const std::unordered_map<std::string, UniformData>& GetUniforms() const { return m_uniforms; }

    bool IsActive(){return m_isActive;}
    void SetActive(bool a_val){m_isActive = a_val;}

    enum class Alignment
    {
        LEFT,
        CENTER,
        RIGHT,
        SIZE
    };

    inline Alignment GetAlignment() { return m_alignment; }
    inline void SetAlignment(Alignment a_alignement) { m_alignment =  a_alignement; }
private:
    Alignment m_alignment;
    float m_spacing;
    std::string m_text;
    Font* m_font;
    Shader* m_shader;

    std::unordered_map<std::string, UniformData> m_uniforms;

    glm::vec4 m_colour;

    bool m_isActive;
};


#endif //PEWPEW_FONTCOMPONENT_H
