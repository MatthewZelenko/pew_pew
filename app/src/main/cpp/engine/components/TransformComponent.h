#ifndef PEWPEW_TRANSFORMCOMPONENT_H
#define PEWPEW_TRANSFORMCOMPONENT_H

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <glm/glm.hpp>
#include "Component.h"

class TransformComponent : public Component<TransformComponent>
{
public:
    TransformComponent();
    ~TransformComponent();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    inline void SetPosition(const glm::vec2& a_position) {m_isSleeping = false; m_position = a_position;}
    inline void AddPosition(const glm::vec2& a_position) {m_isSleeping = false; m_position += a_position;}
    inline void SetZIndex(float a_zIndex) {m_isSleeping = false; m_zIndex = a_zIndex;}
    inline void AddZIndex(float a_zIndex) {m_isSleeping = false; m_zIndex += a_zIndex;}
    inline void SetScale(const glm::vec2& a_scale) {m_isSleeping = false; m_scale = a_scale;}
    inline void AddScale(const glm::vec2& a_scale) {m_isSleeping = false; m_scale += a_scale;}
    inline void SetRotationRadians(const float a_rotation) {m_isSleeping = false; m_rotation = a_rotation;}
    inline void SetRotationDegrees(const float a_rotation) {m_isSleeping = false; m_rotation = glm::radians(a_rotation);}
    inline void AddRotation(const float a_rotation) {m_isSleeping = false; m_rotation += a_rotation;}

    inline const glm::vec2& GetPosition() const {return m_position;}
    inline const float GetZIndex() const { return m_zIndex; }
    inline const glm::vec2& GetScale() const {return m_scale;}
    inline const float GetRotation() const {return m_rotation;}

    const glm::mat4 GetModel() const;

    bool IsSleeping() { return m_isSleeping; }

private:
    bool m_isSleeping;
    glm::vec2 m_position;
    float m_zIndex;
    glm::vec2 m_scale;
    float m_rotation;
};


#endif //PEWPEW_TRANSFORMCOMPONENT_H