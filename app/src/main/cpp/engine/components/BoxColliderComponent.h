#ifndef PEW_PEW_BOXCOLLIDERCOMPONENT_H
#define PEW_PEW_BOXCOLLIDERCOMPONENT_H

#include <components/Component.h>
#include <glm/vec2.hpp>
#include <functional>
#include "Rect.h"

class BoxColliderComponent : public Component<BoxColliderComponent>
{
public:
    BoxColliderComponent();
    ~BoxColliderComponent();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void SetRect(const Rect& a_rect);
    const Rect& GetRect() {return m_rect;}

    void SetTag(unsigned char a_tag);
    void AddTag(unsigned char a_tag);
    void RemoveTag(unsigned char a_tag);
    bool HasTag(unsigned char a_tag);
    unsigned char GetTag();

    std::function<void(IEntity* a_otherEntity, unsigned char a_tag)> OnCollision;

private:
    Rect m_rect;
    unsigned char m_tag;
};

#endif //PEW_PEW_BOXCOLLIDERCOMPONENT_H