#ifndef PEWPEW_COMPONENT_H
#define PEWPEW_COMPONENT_H

#include "Helper.h"
#include "IComponent.h"

template<class T>
class Component : public IComponent
{
public:
    Component(){}
    virtual ~Component(){}

    static TypeID ComponentTypeID();
};

template<class T>
TypeID Component<T>::ComponentTypeID()
{
    static TypeID typeID = Helper::CreateComponentTypeID();
    return typeID;
}

#endif //PEWPEW_COMPONENT_H