#ifndef PEW_PEW_CIRCLECOLLIDERCOMPONENT_H
#define PEW_PEW_CIRCLECOLLIDERCOMPONENT_H


#include <functional>
#include <glm/vec2.hpp>
#include <Circle.h>
#include "Component.h"

class CircleColliderComponent : public Component<CircleColliderComponent>
{
public:
    CircleColliderComponent();
    ~CircleColliderComponent();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void SetCircle(const Circle& a_circle);

    const Circle& GetCircle(){return m_circle;}

    void SetTag(unsigned char a_tag);
    void AddTag(unsigned char a_tag);
    void RemoveTag(unsigned char a_tag);
    bool HasTag(unsigned char a_tag);
    unsigned char GetTag();

    std::function<void(IEntity* a_otherEntity, unsigned char a_tag)> OnCollision;

private:
    Circle m_circle;
    unsigned char m_tag;
};

#endif //PEW_PEW_CIRCLECOLLIDERCOMPONENT_H