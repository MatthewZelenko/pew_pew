#ifndef PEWPEW_SPRITECOMPONENT_H
#define PEWPEW_SPRITECOMPONENT_H

#include <string>
#include <unordered_map>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <UniformData.h>
#include <vector>
#include "Component.h"
#include <Sprite.h>

class Shader;
class Texture;

class SpriteComponent : public Component<SpriteComponent>
{
public:
    SpriteComponent();
    ~SpriteComponent();

    void Load() override;
    void FixedUpdate(double a_timeStep) override;
    void Unload() override;

    void LoadAnimation(const std::string& a_name, bool a_play = false, bool a_isLooping = false);
    void AddSprite(const std::string& a_name);
    void SetSprite(const std::string& a_name);
    Sprite GetSprite() const;

    void SetShader(const std::string& a_shader);
    Shader* GetShader() const { return m_shader; }

    void SetColour(const glm::vec4& a_colour) { m_colour = a_colour; }
    glm::vec4 GetColour() { return m_colour; }

    void SetUniform(const std::string& a_name, float a_data);
    void SetUniform(const std::string& a_name, const glm::vec2& a_data);
    void SetUniform(const std::string& a_name, const glm::vec3& a_data);
    void SetUniform(const std::string& a_name, const glm::vec4& a_data);
    void SetUniform(const std::string& a_name, const glm::mat4& a_data);

    inline const std::unordered_map<std::string, UniformData>& GetUniforms() const { return m_uniforms; }
    bool HasUniforms() { return !m_uniforms.empty(); }

    bool IsActive(){return m_isActive;}
    void SetActive(bool a_val){m_isActive = a_val;}

    void Play(bool a_loop);
    void Stop();
    bool IsPlaying() { return m_isPlayingAnimation; }


private:
    std::unordered_map<std::string, UniformData> m_uniforms;


    std::string m_currentAnimationName;
    bool m_isPlayingAnimation, m_isLooping;
    float m_frameElapse;
    unsigned int m_currentSprite;
    std::vector<Sprite> m_sprites;
    std::vector<float> m_frameSpeeds;
    Shader* m_shader;

    glm::vec4 m_colour;

    bool m_isActive;
};

#endif //PEWPEW_SPRITECOMPONENT_H