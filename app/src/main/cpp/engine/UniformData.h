#ifndef PEW_PEW_UNIFORMDATA_H
#define PEW_PEW_UNIFORMDATA_H

struct UniformData
{
    enum class Type
    {
        FLOAT,
        FLOAT2,
        FLOAT3,
        FLOAT4,
        MAT4
    };
    Type m_type;
    void* m_data;
};

#endif //PEW_PEW_UNIFORMDATA_H