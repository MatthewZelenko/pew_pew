#include <GLES3/gl3.h>
#include <android/asset_manager.h>
#include <stb/stb_image.h>
#include "Texture.h"
#include "Application.h"
#include "Log.h"

Texture::Texture(const std::string &a_filepath)
{
    Create(a_filepath);
}
Texture::~Texture()
{
    Destroy();
}

void Texture::Create(const std::string &a_filepath)
{
    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      a_filepath.c_str(), AASSET_MODE_BUFFER);
    if (file == nullptr)
    {
        Log::Error("Texture file not find: %s", a_filepath.c_str());
        return;
    }

    off_t size = AAsset_getLength(file);
    unsigned char* bytes = new unsigned char[size];
    AAsset_read(file, bytes, size);


    stbi_set_flip_vertically_on_load(true);
    unsigned char* textureData = stbi_load_from_memory(bytes, size, &m_width, &m_height, &m_channels, 4);
    AAsset_close(file);

    glGenTextures(1, &m_textureID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);

    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(textureData);
    delete[] bytes;
}
void Texture::Destroy()
{
    if(m_textureID != 0)
        glDeleteTextures(1, &m_textureID);
}

void Texture::Bind()const
{
    glBindTexture(GL_TEXTURE_2D, m_textureID);
}
void Texture::Unbind()
{
    glBindTexture(GL_TEXTURE_2D, 0);
}