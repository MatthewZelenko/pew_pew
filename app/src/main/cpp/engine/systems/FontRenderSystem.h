#ifndef PEW_PEW_FONTRENDERSYSTEM_H
#define PEW_PEW_FONTRENDERSYSTEM_H

#include <string>
#include <components/FontComponent.h>
#include <Glyph.h>
#include "System.h"

class Camera;
class Shader;
class SpriteComponent;
class TransformComponent;

class FontRenderSystem : public System<FontRenderSystem>
{
public:
    FontRenderSystem();
    ~FontRenderSystem();

    void Load() override;
    void Unload()override;
    void Render() override;

    void RenderFont(TransformComponent* a_trans, FontComponent* a_fontComponent);


private:
    struct Vertex
    {
        glm::vec3 m_position;
        glm::vec2 m_texCoords;
        glm::vec4 m_colour;
    };
    unsigned int m_vao, m_vbo;

    void BindUniforms(Shader* a_shader, FontComponent& a_fontComponent) const;
};


#endif //PEW_PEW_FONTRENDERSYSTEM_H