#include "FontRenderSystem.h"
#include <GLES3/gl3.h>
#include <IEntity.h>
#include <android/asset_manager.h>
#include <Application.h>
#include <sstream>
#include <Glyph.h>
#include <components/FontComponent.h>
#include "Engine.h"
#include "Shader.h"
#include "Log.h"
#include "Texture.h"
#include "Camera.h"
#include "ComponentManager.h"
#include "components/SpriteComponent.h"
#include "components/TransformComponent.h"

#include "Font.h"

FontRenderSystem::FontRenderSystem() : System(), m_vao(0), m_vbo(0)
{
}
FontRenderSystem::~FontRenderSystem()
{
}

void FontRenderSystem::Load()
{
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (sizeof(float) * 9), (const void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (sizeof(float) * 9), (const void*)(sizeof(float) * 3));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (sizeof(float) * 9), (const void*)(sizeof(float) * 5));

    glBindVertexArray(0);
}
void FontRenderSystem::Unload()
{
    if (m_vbo != 0)
    {
        glDeleteBuffers(1, &m_vbo);
        m_vbo = 0;
    }
    if (m_vao != 0)
    {
        glDeleteBuffers(1, &m_vao);
        m_vao = 0;
    }
}

void FontRenderSystem::Render()
{
    auto iter = Engine::Get()->GetComponentManager()->Begin<FontComponent>();
    auto end = Engine::Get()->GetComponentManager()->End<FontComponent>();
    for (;iter != end; ++iter)
    {
        if(!(*iter) || !((FontComponent*)(*iter))->IsActive())
            continue;

        TransformComponent* trans = (*iter)->GetParent()->GetTransformComponent();
        RenderFont(trans, (FontComponent*)*iter);
    }
}
void FontRenderSystem::RenderFont(TransformComponent *a_trans, FontComponent *a_fontComponent)
{
    assert(Camera::MainCamera());
    Font* font = a_fontComponent->GetFont();
    Shader* shader = a_fontComponent->GetShader();
    if(!font|| !shader)
        return;
    const std::string& text = a_fontComponent->GetText();
    float aspectRatio = Camera::MainCamera()->GetGameSize().x / Camera::MainCamera()->GetGameSize().y * 4;

    //TODO:Add each char as a seperate vertex, 4 verts, x, y, texX, texY

    Vertex verts[6 * text.size()];

    float spacing = a_fontComponent->GetSpacing() / font->GetTexture()->GetWidth();

    glm::vec2 scale = a_trans->GetScale();

    int n = 0;
    float xStartPosition = a_trans->GetPosition().x;
    float yStartPosition = a_trans->GetPosition().y;
    float zStartPosition = a_trans->GetZIndex();


    //TODO: Set position and scale first.
    //TODO: Use number of characters / trans->scale.w
    glm::vec2 pos;
    glm::vec2 size;

    for (int i = 0; i < text.size(); ++i)
    {
        char c = text[i];

        pos = glm::vec2(xStartPosition, yStartPosition);
        size = glm::vec2(font->GetGlpyh(c)->GetW() * scale.x, (font->GetGlpyh(c)->GetH() * scale.y) / aspectRatio);


        verts[n++] = (Vertex){glm::vec3(pos.x, pos.y, zStartPosition),                        glm::vec2(font->GetGlpyh(c)->GetX(), font->GetGlpyh(c)->GetY()),                                                            a_fontComponent->GetColour() };                                //BOTTOM_LEFT
        verts[n++] = (Vertex){glm::vec3(pos.x + size.x, pos.y, zStartPosition),               glm::vec2(font->GetGlpyh(c)->GetX() + font->GetGlpyh(c)->GetW(), font->GetGlpyh(c)->GetY()),                                a_fontComponent->GetColour() };                                //BOTTOM_LEFT
        verts[n++] = (Vertex){glm::vec3(pos.x + size.x, pos.y + size.y, zStartPosition),      glm::vec2(font->GetGlpyh(c)->GetX() + font->GetGlpyh(c)->GetW(), font->GetGlpyh(c)->GetY() + font->GetGlpyh(c)->GetH()),    a_fontComponent->GetColour() };                                //BOTTOM_LEFT

        verts[n++] = (Vertex){glm::vec3(pos.x + size.x, pos.y + size.y, zStartPosition),      glm::vec2(font->GetGlpyh(c)->GetX() + font->GetGlpyh(c)->GetW(), font->GetGlpyh(c)->GetY() + font->GetGlpyh(c)->GetH()),    a_fontComponent->GetColour() };                                //BOTTOM_LEFT
        verts[n++] = (Vertex){glm::vec3(pos.x, pos.y + size.y, zStartPosition),               glm::vec2(font->GetGlpyh(c)->GetX(), font->GetGlpyh(c)->GetY() + font->GetGlpyh(c)->GetH()),                                a_fontComponent->GetColour() };                                //BOTTOM_LEFT
        verts[n++] = (Vertex){glm::vec3(pos.x, pos.y, zStartPosition),                        glm::vec2(font->GetGlpyh(c)->GetX(), font->GetGlpyh(c)->GetY()),                                                            a_fontComponent->GetColour() };                                //BOTTOM_LEFT

        xStartPosition += size.x + (spacing * scale.x);
    }

    FontComponent::Alignment alignment = a_fontComponent->GetAlignment();
    if(alignment == FontComponent::Alignment::RIGHT)
    {
        float recenter = xStartPosition - a_trans->GetPosition().x;
        for (int i = 0; i < n; ++i)
        {
            verts[i].m_position.x -= recenter;
        }
    }
    else if(alignment == FontComponent::Alignment::CENTER)
    {
        float recenter = (xStartPosition * 0.5f) - a_trans->GetPosition().x;
        for (int i = 0; i < n; ++i)
        {
            verts[i].m_position.x -= recenter;
        }
    }



        shader->Bind();
    shader->SetUniform4x4(shader->GetViewProjectionLocation(),  Camera::MainCamera()->GetProjection() * Camera::MainCamera()->GetView());

    BindUniforms(shader, *a_fontComponent);

    font->GetTexture()->Bind();

    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STREAM_DRAW);
    glDrawArrays(GL_TRIANGLES,0, n);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void FontRenderSystem::BindUniforms(Shader* a_shader, FontComponent& a_fontComponent) const
{
    auto uniforms = a_fontComponent.GetUniforms();
    auto iter = uniforms.begin();
    for (; iter != uniforms.end(); ++iter)
    {
        switch (iter->second.m_type)
        {
            case UniformData::Type::FLOAT:
                a_shader->SetUniform1f(iter->first, *((float *) iter->second.m_data));
                break;
            case UniformData::Type::FLOAT2:
                a_shader->SetUniform2f(iter->first, *((glm::vec2 *) iter->second.m_data));
                break;
            case UniformData::Type::FLOAT3:
                a_shader->SetUniform3f(iter->first, *((glm::vec3 *) iter->second.m_data));
                break;
            case UniformData::Type::FLOAT4:
                a_shader->SetUniform4f(iter->first, *((glm::vec4 *) iter->second.m_data));
                break;
            case UniformData::Type::MAT4:
                a_shader->SetUniform4x4(iter->first, *((glm::mat4 *) iter->second.m_data));
                break;
        }
    }
}