#include <components/TransformComponent.h>
#include <Engine.h>
#include "TransformSystem.h"
#include "ComponentManager.h"

TransformSystem::TransformSystem()
{

}
TransformSystem::~TransformSystem()
{

}

void TransformSystem::Load()
{

}
void TransformSystem::Unload()
{

}

void TransformSystem::Update(double a_deltaTime)
{
    auto iter = Engine::Get()->GetComponentManager()->Begin<TransformComponent>();
    auto end = Engine::Get()->GetComponentManager()->End<TransformComponent>();
    for (;iter != end; ++iter)
    {
        if(!(*iter))
            continue;
        (*iter)->Update(a_deltaTime);
    }
}
void TransformSystem::LateUpdate(double a_deltaTime)
{

}
void TransformSystem::Render()
{

}