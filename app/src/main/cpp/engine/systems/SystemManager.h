#ifndef PEWPEW_SYSTEMMANAGER_H
#define PEWPEW_SYSTEMMANAGER_H

#include <vector>
#include <unordered_map>
#include <Types.h>
#include "ISystem.h"

class SystemManager
{
public:
    SystemManager();
    ~SystemManager();

    void Destroy();

    template<class T>
    T* AddSystem(int a_priority);
    template<class T>
    T* GetSystem();

    void Update(double a_deltaTime);
    void LateUpdate(double a_deltaTIme);
    void FixedUpdate(double a_timeStep);
    void Render();

private:
    std::vector<ISystem*> m_prioritySystems;
    std::unordered_map<TypeID, ISystem*> m_systems;
};


template<class T>
T* SystemManager::AddSystem(int a_priority)
{
    TypeID typeID = T::StaticComponentID();

    if(m_systems.find(typeID) != m_systems.end())
        return static_cast<T*>(m_systems[typeID]);

    m_systems[typeID] = new T();
    m_systems[typeID]->m_priority = a_priority;

    auto it = std::lower_bound(m_prioritySystems.begin(), m_prioritySystems.end(), m_systems[typeID], [](ISystem* lhs, ISystem* rhs) -> bool {
        return lhs->GetPriority() > rhs->GetPriority();
    });
    m_prioritySystems.insert(it, m_systems[typeID]);

    m_systems[typeID]->Load();

    return static_cast<T*>(m_systems[typeID]);
}
template<class T>
T *SystemManager::GetSystem()
{
    TypeID typeID = T::StaticComponentID();

    if(m_systems.find(typeID) != m_systems.end())
        return static_cast<T*>(m_systems[typeID]);
    return nullptr;
}

#endif //PEWPEW_SYSTEMMANAGER_H