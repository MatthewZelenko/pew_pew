#ifndef PEW_PEW_TRANSFORMSYSTEM_H
#define PEW_PEW_TRANSFORMSYSTEM_H

#include "systems/System.h"

class TransformSystem : public System<TransformSystem>
{
public:
    TransformSystem();
    ~TransformSystem();

private:
    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void LateUpdate(double a_deltaTime) override;
    void Render() override;

private:


};


#endif //PEW_PEW_TRANSFORMSYSTEM_H
