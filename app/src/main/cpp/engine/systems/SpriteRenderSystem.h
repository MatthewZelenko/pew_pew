#ifndef PEWPEW_SPRITERENDERSYSTEM_H
#define PEWPEW_SPRITERENDERSYSTEM_H

#include "System.h"
#include <vector>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>


class Camera;
class Shader;
class SpriteComponent;
class TransformComponent;
class Texture;

class SpriteRenderSystem : public System<SpriteRenderSystem>
{
public:
    SpriteRenderSystem();
    ~SpriteRenderSystem();

    void Load() override;
    void Unload() override;

    void FixedUpdate(double a_timeStep) override;
    void Render() override;

    void RenderSprite(TransformComponent* a_transformComponent, SpriteComponent* a_spriteComponent);

private:
    struct Vertex
    {
        glm::vec3 m_position;
        glm::vec2 m_texCoords;
        glm::vec4 m_colour;
    };

    unsigned int m_vao, m_vbo;
    Texture const* m_currentTexture;
    Shader* m_currentShader;
    int m_numberOfVerts;
    std::vector<Vertex> m_vertices;

    const int MAX_VERTICES = 600;

    void BindVertexArray();
    void BindUniforms(Shader* a_shader, SpriteComponent& a_spriteComponent) const;
    void Flush();
};

#endif //PEWPEW_SPRITERENDERSYSTEM_H