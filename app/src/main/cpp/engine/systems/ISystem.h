#ifndef PEWPEW_ISYSTEM_H
#define PEWPEW_ISYSTEM_H

#include "Types.h"

class ISystem
{
public:
    ISystem(){}
    virtual ~ISystem(){}

    virtual void Load(){}
    virtual void Unload(){}

    virtual void Update(double a_deltaTime){}
    virtual void LateUpdate(double a_deltaTime){}
    virtual void FixedUpdate(double a_timeStep){}
    virtual void Render(){}

    void SetPriority(int a_value){m_priority = a_value;}
    int GetPriority(){return m_priority;}

private:
    friend class SystemManager;
    int m_priority;
};


#endif //PEWPEW_ISYSTEM_H