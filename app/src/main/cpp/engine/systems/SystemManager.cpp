#include "SystemManager.h"
#include "SpriteRenderSystem.h"
#include "TransformSystem.h"

SystemManager::SystemManager()
{
}
SystemManager::~SystemManager()
{
    Destroy();
}
void SystemManager::Destroy()
{
    for (int i = 0; i < m_systems.size(); ++i)
    {
        if(m_systems[i] == nullptr)
            continue;

        m_systems[i]->Unload();
        delete m_systems[i];
    }
    m_systems.clear();
    m_prioritySystems.clear();
}


void SystemManager::Update(double a_deltaTime)
{
    for (int i = 0; i < m_prioritySystems.size(); ++i)
    {
        m_prioritySystems[i]->Update(a_deltaTime);
    }
}
void SystemManager::LateUpdate(double a_deltaTime)
{
    for (int i = 0; i < m_prioritySystems.size(); ++i)
    {
        m_prioritySystems[i]->LateUpdate(a_deltaTime);
    }
}
void SystemManager::FixedUpdate(double a_timeStep)
{
    for (int i = 0; i < m_prioritySystems.size(); ++i)
    {
        m_prioritySystems[i]->FixedUpdate(a_timeStep);
    }
}
void SystemManager::Render()
{
    for (int i = 0; i < m_prioritySystems.size(); ++i)
    {
        m_prioritySystems[i]->Render();
    }
}
