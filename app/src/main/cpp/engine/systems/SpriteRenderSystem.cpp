#include "SpriteRenderSystem.h"
#include <GLES3/gl3.h>
#include <IEntity.h>
#include "Engine.h"
#include "Shader.h"
#include "Log.h"
#include "Texture.h"
#include "Camera.h"
#include "ComponentManager.h"
#include "components/SpriteComponent.h"
#include "components/TransformComponent.h"
#include "glm/gtx/rotate_vector.hpp"

SpriteRenderSystem::SpriteRenderSystem() : System(),
                                           m_vbo(0),
                                           m_vao(0),
                                           m_numberOfVerts(0),
                                           m_currentTexture(nullptr),
                                           m_currentShader(nullptr)
{
}
SpriteRenderSystem::~SpriteRenderSystem()
{
}


void SpriteRenderSystem::Load()
{
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, MAX_VERTICES * sizeof(Vertex), nullptr, GL_STREAM_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (sizeof(float) * 9), (const void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (sizeof(float) * 9), (const void*)(sizeof(float) * 3));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (sizeof(float) * 9), (const void*)(sizeof(float) * 5));

    glBindVertexArray(0);
}

void SpriteRenderSystem::Unload()
{
    m_numberOfVerts = 0;
    m_currentTexture = nullptr;
    m_currentShader = nullptr;
    m_vbo = 0;
    if (m_vbo != 0)
    {
        glDeleteBuffers(1, &m_vbo);
    }
    if (m_vao != 0)
    {
        glDeleteBuffers(1, &m_vao);
        m_vao = 0;
    }
}

void SpriteRenderSystem::FixedUpdate(double a_timeStep)
{
    std::vector<IComponent*> vec = Engine::Get()->GetComponentManager()->GetVector<SpriteComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        SpriteComponent* sprite = (SpriteComponent*)vec[i];
        if(!sprite->IsActive())
            continue;

        sprite->FixedUpdate(a_timeStep);
    }

}
void SpriteRenderSystem::Render()
{
    std::vector<IComponent*> vec = Engine::Get()->GetComponentManager()->GetVector<SpriteComponent>();
    std::sort(vec.begin(), vec.end(), [](IComponent* a_lhs, IComponent* a_rhs)
    {
        if(a_lhs == nullptr)
            return false;
        if(a_rhs == nullptr)
            return true;

        SpriteComponent* lhs = (SpriteComponent*)a_lhs;
        SpriteComponent* rhs = (SpriteComponent*)a_rhs;

        if(!lhs->GetSprite().GetTexture())
            return true;
        else if(!rhs->GetSprite().GetTexture())
            return false;

        int lID = lhs->GetSprite().GetTexture()->GetID();
        int rID = rhs->GetSprite().GetTexture()->GetID();

        float lZ = a_lhs->GetParent()->GetTransformComponent()->GetZIndex();
        float rZ = a_rhs->GetParent()->GetTransformComponent()->GetZIndex();

        if(lZ == rZ)
        {
            return lID < rID;
        }
        else
        {
            return lZ < rZ;
        }
    });

    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        SpriteComponent* sprite = (SpriteComponent*)vec[i];
        if(!sprite->IsActive())
            continue;

        RenderSprite(vec[i]->GetParent()->GetTransformComponent(), sprite);
    }
    Flush();
}
void SpriteRenderSystem::RenderSprite(TransformComponent *a_transformComponent, SpriteComponent *a_spriteComponent)
{
    assert(Camera::MainCamera());

    Shader* shader = a_spriteComponent->GetShader();
    if(shader != m_currentShader)
    {
        Flush();
        m_currentShader = shader;
    }

    Sprite sprite = a_spriteComponent->GetSprite();
    if(sprite.GetTexture() != m_currentTexture)
    {
        Flush();
        m_currentTexture = sprite.GetTexture();
    }

    if(m_numberOfVerts == MAX_VERTICES)
        Flush();

    glm::vec2 size = a_transformComponent->GetScale();
    if(size.x < 0 && size.y < 0)
    {
        size = sprite.GetTextureRect().GetSize();
    }
    else if(size.x < 0)
    {
        size.x = size.y;
    }
    else if(size.y < 0)
    {
        size.y = size.x;
    }
    glm::vec2 pos = a_transformComponent->GetPosition();
    Rect rect = sprite.GetRect();


    m_vertices.push_back((Vertex){glm::vec3(0, 0, a_transformComponent->GetZIndex()), glm::vec2(rect.Left(), rect.Bottom()), a_spriteComponent->GetColour()});      //BOTTOM LEFT
    m_vertices.push_back((Vertex){glm::vec3(1, 0, a_transformComponent->GetZIndex()), glm::vec2(rect.Right(), rect.Bottom()), a_spriteComponent->GetColour()});     //BOTTOM RIGHT
    m_vertices.push_back((Vertex){glm::vec3(1, 1, a_transformComponent->GetZIndex()), glm::vec2(rect.Right(), rect.Top()), a_spriteComponent->GetColour()});        //TOP RIGHT
    m_vertices.push_back((Vertex){glm::vec3(1, 1, a_transformComponent->GetZIndex()), glm::vec2(rect.Right(), rect.Top()), a_spriteComponent->GetColour()});        //TOP RIGHT
    m_vertices.push_back((Vertex){glm::vec3(0, 1, a_transformComponent->GetZIndex()), glm::vec2(rect.Left(), rect.Top()), a_spriteComponent->GetColour()});         //TOP LEFT
    m_vertices.push_back((Vertex){glm::vec3(0, 0, a_transformComponent->GetZIndex()), glm::vec2(rect.Left(), rect.Bottom()), a_spriteComponent->GetColour()});      //BOTTOM ELFT

    for (int i = m_numberOfVerts; i < m_numberOfVerts + 6; ++i)
    {
        glm::vec2 vPos = (glm::vec2(m_vertices[i].m_position.x, m_vertices[i].m_position.y) - sprite.GetOrigin()) * size;
        vPos = glm::rotate(vPos, a_transformComponent->GetRotation());

        m_vertices[i].m_position.x = vPos.x + pos.x;
        m_vertices[i].m_position.y = vPos.y + pos.y;
    }
    //TODO: Flush when uniforms are bound.

    m_numberOfVerts += 6;
}
void SpriteRenderSystem::BindUniforms(Shader* a_shader, SpriteComponent& a_spriteComponent) const
{
    auto uniforms = a_spriteComponent.GetUniforms();
    auto iter = uniforms.begin();
    for (; iter != uniforms.end(); ++iter)
    {
        switch (iter->second.m_type)
        {
            case UniformData::Type::FLOAT:
                a_shader->SetUniform1f(iter->first, *((float*)iter->second.m_data));
                break;
            case UniformData::Type::FLOAT2:
                a_shader->SetUniform2f(iter->first, *((glm::vec2*)iter->second.m_data));
                break;
            case UniformData::Type::FLOAT3:
                a_shader->SetUniform3f(iter->first, *((glm::vec3*)iter->second.m_data));
                break;
            case UniformData::Type::FLOAT4:
                a_shader->SetUniform4f(iter->first, *((glm::vec4*)iter->second.m_data));
                break;
            case UniformData::Type::MAT4:
                a_shader->SetUniform4x4(iter->first, *((glm::mat4*)iter->second.m_data));
                break;
        }
    }
}

void SpriteRenderSystem::BindVertexArray()
{
    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, m_vertices.size() * sizeof(Vertex), m_vertices.data());
}

void SpriteRenderSystem::Flush()
{
    if(!m_numberOfVerts || !m_currentShader || !m_currentTexture)
        return;

    m_currentShader->Bind();
    m_currentShader->SetUniform4x4(m_currentShader->GetViewProjectionLocation(),  Camera::MainCamera()->GetProjection() * Camera::MainCamera()->GetView());
    m_currentTexture->Bind();

    BindVertexArray();
    glDrawArrays(GL_TRIANGLES, 0, m_numberOfVerts);
    glBindVertexArray(0);

    m_numberOfVerts = 0;
    m_vertices.clear();
}