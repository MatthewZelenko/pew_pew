#ifndef PEWPEW_SYSTEM_H
#define PEWPEW_SYSTEM_H

#include "ISystem.h"
#include "Helper.h"

template <class T>
class System : public ISystem
{
public:
    System(){}
    virtual ~System(){}

    static TypeID StaticComponentID();
};

template<class T>
TypeID System<T>::StaticComponentID()
{
    static TypeID typeID = Helper::CreateComponentTypeID();
    return typeID;
}

#endif //PEWPEW_SYSTEM_H