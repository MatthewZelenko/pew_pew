#ifndef PEW_PEW_UTILITIES_H
#define PEW_PEW_UTILITIES_H

#include <sys/stat.h>
#include <string>

inline bool FileExists(const std::string& a_filename)
{
    struct stat buf;
    return stat(a_filename.c_str(), &buf) != -1;
}
inline bool DirectoryExists(const std::string& a_filename)
{
    struct stat buf;
    return ((stat(a_filename.c_str(), &buf) == 0) && ((buf.st_mode & S_IFDIR) != 0));
}
inline bool CreateDirectory(const std::string& a_filepath)
{
    return mkdir(a_filepath.c_str(), 0770) != -1;
}

inline int64_t FastRandSeed()
{
    srand(time(nullptr));
    return rand();
}

inline int64_t FastRand()
{
    static int64_t seed = FastRandSeed();

    seed = (214013*seed+2531011);
    return (seed>>16)&0x7FFF;
}

#endif //PEW_PEW_UTILITIES_H