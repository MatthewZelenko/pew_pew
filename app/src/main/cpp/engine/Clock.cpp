#include "Clock.h"

Clock::Clock():
    m_elapsedTimeStep(0.0f),
    m_timeStep(0.01f),
    m_deltaTime(0.0f)
{
    gettimeofday(&m_systemTime, NULL);
}

void Clock::Update()
{
    timeval newTime;
    gettimeofday(&newTime, NULL);
    m_deltaTime = (newTime.tv_sec - m_systemTime.tv_sec) * 1000.0;
    m_deltaTime += (newTime.tv_usec - m_systemTime.tv_usec) * 0.001;
    m_deltaTime *= 0.001f;
    m_systemTime = newTime;

    m_elapsedTime += m_deltaTime;
    m_elapsedTimeStep += m_deltaTime;
}

void Clock::ResetDeltaTime()
{
    m_deltaTime = 0;
    m_elapsedTimeStep = 0;
}