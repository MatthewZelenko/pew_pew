#include "Engine.h"
#include "EntityManager.h"
#include "ComponentManager.h"
#include "Input.h"
#include "Clock.h"
#include "ResourceManager.h"
#include "systems/SystemManager.h"
#include <EventManager.h>
#include <Audio/AudioManager.h>

Engine* Engine::m_instance = nullptr;

Engine::Engine()
{
    m_clock = new Clock();
    m_input = new Input();
    m_resourceManager = new ResourceManager();
    m_entityManager = new EntityManager();
    m_componentManager = new ComponentManager();
    m_systemManager = new SystemManager();
    m_eventManager = new EventManager();
    m_audioManager = new AudioManager();
}
Engine::~Engine()
{
    if(m_audioManager)
        delete m_audioManager;
    if(m_systemManager)
        delete m_systemManager;
    if (m_entityManager)
        delete m_entityManager;
    if (m_componentManager)
        delete m_componentManager;
    if(m_eventManager)
        delete m_eventManager;
    if(m_resourceManager)
        delete m_resourceManager;
    if(m_clock)
        delete m_clock;
    if(m_input)
        delete m_input;
}

void Engine::Update(double a_deltaTime)
{
    m_clock->Update();
    m_input->Update();
    m_entityManager->Update(a_deltaTime);
    m_systemManager->Update(a_deltaTime);
}
void Engine::FixedUpdate(double a_timeStep)
{
    m_entityManager->FixedUpdate(a_timeStep);
    m_systemManager->FixedUpdate(a_timeStep);
}
void Engine::LateUpdate(double a_deltaTime)
{
    m_input->LateUpdate();
    m_entityManager->LateUpdate(a_deltaTime);
    m_systemManager->LateUpdate(a_deltaTime);
    m_entityManager->ProcessDestroys();
}
void Engine::Render()
{
    m_systemManager->Render();
}

void Engine::TerminateEngine()
{
    if(m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }
}

Engine *Engine::Get()
{
    if(!m_instance) m_instance = new Engine();
        return m_instance;
}
