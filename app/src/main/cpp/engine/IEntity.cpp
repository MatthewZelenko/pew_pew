#include "IEntity.h"

IEntity::IEntity() : m_isActive(true), m_handle(-1), m_transform(nullptr)
{
}
IEntity::~IEntity()
{
    m_transform = nullptr;
}
void IEntity::RemoveAllComponents()
{
    Engine::Get()->GetComponentManager()->RemoveAllComponents(m_handle);
}

TransformComponent *IEntity::GetTransformComponent()
{
    return m_transform;
}