#include "Log.h"
#include <android/log.h>
#include <sstream>

void Log::Info(const char* a_message, ...)
{
    va_list varArgs;
    va_start(varArgs, a_message);

    __android_log_vprint(ANDROID_LOG_INFO, "Game", a_message, varArgs);

    va_end(varArgs);
}
void Log::Error(const char* a_message, ...)
{
    va_list varArgs;
    va_start(varArgs, a_message);

    __android_log_vprint(ANDROID_LOG_ERROR, "Game", a_message, varArgs);

    va_end(varArgs);
}
void Log::Warning(const char* a_message, ...)
{
    va_list varArgs;
    va_start(varArgs, a_message);

    __android_log_vprint(ANDROID_LOG_WARN, "Game", a_message, varArgs);

    va_end(varArgs);
}
void Log::Debug(const char* a_message, ...)
{
    va_list varArgs;
    va_start(varArgs, a_message);

    __android_log_vprint(ANDROID_LOG_DEBUG, "Game", a_message, varArgs);

    va_end(varArgs);
}

std::string Log::IntToString(int a_value)
{
    std::ostringstream os;
    os << a_value;
    return os.str();
}