#include "Rect.h"

Rect::Rect() : m_x(0), m_y(0), m_w(0), m_h(0)
{

}
Rect::Rect(const glm::vec2& a_pos, const glm::vec2& a_size) : m_x(a_pos.x), m_y(a_pos.y), m_w(a_size.x), m_h(a_size.y)
{

}
Rect::Rect(float a_x, float a_y, float a_w, float a_h) : m_x(a_x), m_y(a_y), m_w(a_w), m_h(a_h)
{

}
Rect::~Rect()
{

}

void Rect::SetPosition(const glm::vec2 &a_position)
{
    m_x = a_position.x;
    m_y = a_position.y;
}
void Rect::SetPosition(float a_x, float a_y)
{
    m_x = a_x;
    m_y = a_y;
}
void Rect::SetSize(const glm::vec2 &a_size)
{
    m_w = a_size.x;
    m_h = a_size.y;
}
void Rect::SetSize(float a_w, float a_h)
{
    m_w = a_w;
    m_h = a_h;
}

bool Rect::Contains(const glm::vec2 &a_position)
{
    return !(a_position.x < m_x || a_position.x > m_x + m_w ||
        a_position.y < m_y || a_position.y > m_y + m_h);
}
bool Rect::Intersects(const Rect &a_rect)
{
    return !(Left() > a_rect.Right() || a_rect.Left() > Right() ||
        Bottom() > a_rect.Top() || a_rect.Bottom() > Top());
}

Rect& Rect::operator+=(const glm::vec2 &a_rhs)
{
    m_x += a_rhs.x;
    m_y += a_rhs.y;
    return *this;
}