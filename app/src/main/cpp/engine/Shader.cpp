#include <GLES3/gl3.h>
#include <android/asset_manager.h>
#include "Shader.h"
#include "Application.h"
#include "Log.h"
#include <sstream>
#include <glm/gtc/type_ptr.hpp>

struct ShaderProgramSource
{
    std::string VertexSource;
    std::string FragmentSource;
};
enum class ShaderType
{
    NONE = -1,
    VERTEX = 0,
    FRAGMENT = 1,
    SIZE = 2
};

Shader::Shader(const std::string& a_shaderPath)
{
    Create(a_shaderPath);
}
Shader::~Shader()
{
    Destroy();
}

void Shader::Create(const std::string &a_shaderPath)
{
    m_id = glCreateProgram();
    ReadShaderFromFile(a_shaderPath);
}

void Shader::Destroy()
{
    if(m_id != 0)
        glDeleteProgram(m_id);
}


void Shader::Bind() const
{
    glUseProgram(m_id);
}
void Shader::UnBind()
{
    glUseProgram(0);
}

void Shader::ReadShaderFromFile(const std::string& a_filepath)
{
    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      a_filepath.c_str(), AASSET_MODE_BUFFER);
    if(file == nullptr)
        Log::Error("Shader file not find: %s", a_filepath.c_str());
    off_t size = AAsset_getLength(file);
    char *content = new char[size + 1];
    AAsset_read(file, content, size);
    content[size] = '\0';
    AAsset_close(file);
    ShaderProgramSource source = ParseShader(content);
    delete[] content;

    int ids[(int)ShaderType::SIZE];

    ids[(int)ShaderType::VERTEX] = CompileShader(GL_VERTEX_SHADER, source.VertexSource);
    ids[(int)ShaderType::FRAGMENT] = CompileShader(GL_FRAGMENT_SHADER, source.FragmentSource);

    for (int i = 0; i < (int)ShaderType::SIZE; ++i)
    {
        if (ids[i] != -1)
            glAttachShader(m_id, ids[i]);
    }

    LinkProgram(ids);
}
ShaderProgramSource Shader::ParseShader(const std::string&  a_string)
{
    std::stringstream shaders[2];

    std::stringstream stream;
    stream << a_string;
    std::string line;

    ShaderType shaderType = ShaderType::NONE;

    while (getline(stream, line))
    {
        if(line.find("Shader(") != std::string::npos)
        {
            if(line.find("vertex") != std::string::npos)
            {
                shaderType = ShaderType::VERTEX;
            }
            else if(line.find("fragment") != std::string::npos)
            {
                shaderType = ShaderType::FRAGMENT;
            }
        }
        else if(shaderType >= ShaderType::VERTEX)
        {
            shaders[(int)shaderType] << line << '\n';
        }
    }

    return {shaders[0].str(), shaders[1].str()};
}

int Shader::CompileShader(unsigned int a_shaderType, const std::string& a_source)
{
    if(a_source.empty())
        return -1;


    int shaderId = glCreateShader(a_shaderType);

    const char* source = a_source.c_str();
    glShaderSource(shaderId, 1, &source, 0);
    glCompileShader(shaderId);
    int success = 0;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
    if (success != GL_TRUE)
    {
        char infoLog[512];
        glGetShaderInfoLog(shaderId, 512, 0, infoLog);
        infoLog[511] = '\0';
        Log::Error("Shader(Type=%i): %s", a_shaderType, infoLog);
        glDeleteShader(shaderId);
        return -1;
    }
    return shaderId;
}

void Shader::LinkProgram(const int* a_ids)
{
    glLinkProgram(m_id);

    int success = 0;
    glGetProgramiv(m_id, GL_LINK_STATUS, &success);
    if (success != GL_TRUE)
    {
        char infoLog[512];
        glGetProgramInfoLog(m_id, 512, NULL, infoLog);
        infoLog[511] = '\0';
        Log::Error("Program: %s", infoLog);
    }

    for (int i = 0; i < (int)ShaderType::SIZE; i++)
    {
        if(a_ids[i] != -1)
            glDeleteShader(a_ids[i]);
    }
}


void Shader::SetUniform1f(const std::string &a_name, float a_value)
{
    int loc = Getlocation(a_name);
    if(loc == -1)
        return;
    glUniform1f(loc, a_value);
}
void Shader::SetUniform2f(const std::string &a_name, glm::vec2 a_value)
{
    int loc = Getlocation(a_name);
    if(loc == -1)
        return;
    glUniform2f(loc, a_value.x, a_value.y);
}
void Shader::SetUniform3f(const std::string &a_name, glm::vec3 a_value)
{
    int loc = Getlocation(a_name);
    if(loc == -1)
        return;
    glUniform3f(loc, a_value.x,a_value.y,a_value.z);
}
void Shader::SetUniform4f(const std::string &a_name, glm::vec4 a_value)
{
    int loc = Getlocation(a_name);
    if(loc == -1)
        return;
    glUniform4f(loc, a_value.x, a_value.y, a_value.z, a_value.w);
}

int Shader::Getlocation(const std::string &a_name)
{
    if (m_uniformLocations.find(a_name) == m_uniformLocations.end())
        m_uniformLocations[a_name] = glGetUniformLocation(m_id, a_name.c_str());
    return m_uniformLocations[a_name];
}

void Shader::SetUniform4x4(const std::string &a_name, const glm::mat4x4 a_value)
{
    int loc = Getlocation(a_name);
    if(loc == -1)
        return;
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(a_value));
}
