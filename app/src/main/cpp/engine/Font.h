#ifndef PEW_PEW_FONT_H
#define PEW_PEW_FONT_H
#include "Glyph.h"
#include <unordered_map>
#include <string>

class Texture;

class Font
{
public:
    Font(const std::string &a_filepath);
    ~Font();

    const Texture* GetTexture()const {return m_texture;}
    const Glyph* GetGlpyh(const char a_glyph);

private:
    Texture* m_texture;
    std::unordered_map<char, Glyph> m_glyphs;
};


#endif //PEW_PEW_FONT_H