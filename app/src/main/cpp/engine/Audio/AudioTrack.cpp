#include <android/asset_manager.h>
#include <Application.h>
#include <vorbis/vorbisfile.h>
#include <Log.h>
#include <glm/gtc/constants.hpp>
#include "AudioTrack.h"
#include "AudioManager.h"


size_t AudioReadCallback(void *a_buffer, size_t a_elementSize, size_t a_elementCount, void *a_dataSource)
{
    AudioFileData* data = reinterpret_cast<AudioFileData*>(a_dataSource);

    long long readSize = a_elementCount * a_elementSize;

    if(data->currentPtr + readSize >= data->filePtr + data->fileSize)
    {
        readSize = data->filePtr + data->fileSize - data->currentPtr;
    }

    memcpy(a_buffer, data->currentPtr, readSize);
    data->currentPtr += readSize;
    return readSize;

}


AudioTrack::AudioTrack(const std::string& a_path) : m_sampleRate(0), m_data(nullptr), m_dataSize(0), m_path(a_path)
{
    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      a_path.c_str(), AASSET_MODE_UNKNOWN);

    assert(file != nullptr && ("AudioManager::Audio file not found" + a_path).c_str());

    off_t start, length;
    length = AAsset_getLength(file);
    char* data = new char[length + 1];
    AAsset_read(file, data, length);
    AAsset_close(file);
    data[length] = 0;

    OggVorbis_File vf;
    int eof = 0;
    int currentSection;

    ov_callbacks callbacks;
    callbacks.read_func = AudioReadCallback;
    callbacks.close_func = NULL;
    callbacks.seek_func = NULL;
    callbacks.tell_func = NULL;

    AudioFileData audioDataStruct;
    audioDataStruct.filePtr = audioDataStruct.currentPtr = data;
    audioDataStruct.fileSize = length;

    if(ov_open_callbacks(&audioDataStruct, &vf, NULL, 0, callbacks) < 0)
    {
        Log::Error("Input does not appear to be an Ogg bitstream.");
        return;
    }


    char uncompressedChunk[4096];

    vorbis_info* vi = ov_info(&vf, -1);
    int current_section = 0;
    size_t bytes_read = 0;

    while((bytes_read = (size_t)ov_read(&vf, uncompressedChunk, 4096, 0, 2, 1, &current_section)) > 0)
    {
        if(m_dataSize == 0)
        {
            m_data = new char[bytes_read + 1];
            memcpy(m_data, uncompressedChunk, bytes_read);
            m_dataSize = bytes_read;
            m_data[bytes_read] = 0;
        }
        else
        {
            char* newBuffer = (char*)realloc(m_data, m_dataSize + bytes_read + 1);
            if(!newBuffer)
            {
                Log::Error("AudioManager::Could not reallocate enough memory for decompression");
                delete[] data;
                delete m_data;
                m_data = nullptr;
                ov_clear(&vf);
                return;
            }
            m_data = newBuffer;
            memcpy(&m_data[m_dataSize / sizeof(char)], uncompressedChunk, bytes_read);
            m_dataSize += bytes_read;
            m_data[m_dataSize] = 0;
        }
    }
    m_sampleRate = vi->rate;
    ov_clear(&vf);
    delete[] data;

    Resample();
}
AudioTrack::~AudioTrack()
{
    if(m_data)
        delete m_data;
}
void AudioTrack::Resample()
{

    /*
    //TODO: Resampling simply puts audio on a fast track.
    if(ANDROID_SAMPLE_RATE == m_sampleRate)
        return;

    long long newSize = (long long)((double)m_dataSize * ((double)ANDROID_SAMPLE_RATE / (double)m_sampleRate));
    char* resampledAudio = new char[newSize + 1];
    resampledAudio[newSize] = 0;

    long double sizeConversion = (double)m_dataSize / (double)newSize;

    for (int i = 0; i < newSize; ++i)
    {
        long double d = i * sizeConversion;
        long long lowIndex = (long long)floorl(d);
        long long highIndex = lowIndex + 1;
        if(highIndex >= m_dataSize)
            highIndex = 0;

        long double percentage = d - lowIndex;

        char lowValue = round(m_data[lowIndex] * (1 - percentage));
        char highValue = 0;
        if(highIndex < m_dataSize)
        {
            highValue = round(m_data[highIndex] * percentage);
        }

        resampledAudio[i] = lowValue + highValue;

        //Log::Error("index:%i  -   %i:%i", i, resampledAudio[i], m_data[lowIndex]);
    }

    delete m_data;
    m_data = resampledAudio;
    m_sampleRate = ANDROID_SAMPLE_RATE;
    m_dataSize = newSize;*/
}
