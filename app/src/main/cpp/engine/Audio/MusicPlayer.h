#ifndef PEW_PEW_MUSICPLAYER_H
#define PEW_PEW_MUSICPLAYER_H


#include <SLES/OpenSLES.h>
#include <string>

class MusicPlayer
{
public:
    enum class MusicState
    {
        STOPPED,
        PAUSED,
        PLAYING,
        SIZE
    };


    MusicPlayer(const std::string& a_path, const SLEngineItf a_engineInterface, const SLObjectItf a_outputMixInterface);
    ~MusicPlayer();

    MusicState GetMusicPlayState(){return m_musicState;}

    void Play(bool a_loop = false);
    void Pause();
    void Resume();
    void Stop();


private:
    std::string m_path;
    int m_fileDescriptor;
    long long m_start, m_length;

    MusicState m_musicState;

    SLObjectItf m_musicPlayerInterface;
    SLPlayItf m_musicPlayInterface;
    SLSeekItf m_musicSeekInterface;
    SLVolumeItf m_musicVolumeInterface;


};

#endif //PEW_PEW_MUSICPLAYER_H