#ifndef PEW_PEW_AUDIOTRACK_H
#define PEW_PEW_AUDIOTRACK_H

#include <string>

struct AudioFileData
{
    char* currentPtr;
    char* filePtr;
    long long fileSize;
};

class AudioTrack
{
public:
    AudioTrack(const std::string& a_path);
    ~AudioTrack();

    void Resample();

    inline const std::string& GetPath() const { return m_path; }
    inline const char* const GetData() const { return m_data; }
    inline const unsigned long long GetDataSize() const { return m_dataSize; }
    inline const long GetSampleRate() const { return m_sampleRate; }

private:
    std::string m_path;

    char* m_data;
    unsigned long long m_dataSize;
    long m_sampleRate;
};


#endif //PEW_PEW_AUDIOTRACK_H