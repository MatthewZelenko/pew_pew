#include <assert.h>
#include <SLES/OpenSLES_Android.h>
#include <android/asset_manager.h>
#include <Application.h>
#include "MusicPlayer.h"

MusicPlayer::MusicPlayer(const std::string& a_path, const SLEngineItf a_engineInterface, const SLObjectItf a_outputMixInterface) :
        m_musicPlayerInterface(nullptr),
        m_musicPlayInterface(nullptr),
        m_musicSeekInterface(nullptr),
        m_musicVolumeInterface(nullptr),
        m_musicState(MusicState::STOPPED),
        m_path(a_path),
        m_fileDescriptor(0),
        m_length(0),
        m_start(0)
{
    SLresult result;

    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      m_path.c_str(), AASSET_MODE_UNKNOWN);

    assert(file != nullptr && ("AudioManager::Music file not found" + m_path).c_str());

    m_fileDescriptor = AAsset_openFileDescriptor(file, (off_t*)&m_start,  (off_t*)&m_length);
    assert(m_fileDescriptor > 0);
    AAsset_close(file);

    SLDataLocator_AndroidFD loc_fd = {SL_DATALOCATOR_ANDROIDFD, m_fileDescriptor, m_start, m_length};
    SLDataFormat_MIME format_mime = {SL_DATAFORMAT_MIME, NULL, SL_CONTAINERTYPE_OGG};
    SLDataSource audioSrc = {&loc_fd, &format_mime};

    SLDataLocator_OutputMix outputMixDataLocator = {SL_DATALOCATOR_OUTPUTMIX, a_outputMixInterface};
    SLDataSink audioSink = {&outputMixDataLocator, NULL};

    const SLInterfaceID ids[3] = {SL_IID_SEEK, SL_IID_VOLUME};
    const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    result = (*a_engineInterface)->CreateAudioPlayer(a_engineInterface, &m_musicPlayerInterface, &audioSrc, &audioSink, 2,ids, req);
    assert(result == 0 && "AudioManager::Could not create audio player.");
    result = (*m_musicPlayerInterface)->Realize(m_musicPlayerInterface, SL_BOOLEAN_FALSE);
    assert(result == 0 && "AudioManager::Could not create audio player.");

    // get the play interface
    result = (*m_musicPlayerInterface)->GetInterface(m_musicPlayerInterface, SL_IID_PLAY, &m_musicPlayInterface);
    assert(result == 0 && "AudioManager::Could not get play interface.");

    // get the seek interface
    result = (*m_musicPlayerInterface)->GetInterface(m_musicPlayerInterface, SL_IID_SEEK, &m_musicSeekInterface);
    assert(result == 0 && "AudioManager::Could not get seek interface.");

    // get the volume interface
    result = (*m_musicPlayerInterface)->GetInterface(m_musicPlayerInterface, SL_IID_VOLUME, &m_musicVolumeInterface);
    assert(result == 0 && "AudioManager::Could not get volume interface.");
}
MusicPlayer::~MusicPlayer()
{
    Stop();
    if(m_musicPlayerInterface)
        (*m_musicPlayerInterface)->Destroy(m_musicPlayerInterface);
}



void MusicPlayer::Play(bool a_loop)
{
    if (!m_musicSeekInterface)
        return;

    if(m_musicState != MusicState::STOPPED)
        Stop();

    SLresult result = (*m_musicSeekInterface)->SetLoop(m_musicSeekInterface, a_loop ? SL_BOOLEAN_TRUE : SL_BOOLEAN_FALSE, 0, SL_TIME_UNKNOWN);
    assert(result == 0 && "Could not loop the music file");

    Resume();
}

void MusicPlayer::Resume()
{
    if(!m_musicPlayInterface || m_musicState == MusicState::PLAYING)
        return;

    m_musicState = MusicState::PLAYING;
    SLresult result = (*m_musicPlayInterface)->SetPlayState(m_musicPlayInterface, SL_PLAYSTATE_PLAYING);
    assert(result == 0 && "AudioManager::Could not resume music");
}

void MusicPlayer::Pause()
{
    if(!m_musicPlayInterface ||m_musicState != MusicState::PLAYING)
        return;


    m_musicState = MusicState::PAUSED;
    SLresult result = (*m_musicPlayInterface)->SetPlayState(m_musicPlayInterface, SL_PLAYSTATE_PAUSED);
    assert(result == 0 && "AudioManager::Could not pause music");
}

void MusicPlayer::Stop()
{
    if (!m_musicPlayInterface || m_musicState == MusicState::STOPPED)
        return;

    m_musicState = MusicState::STOPPED;
    SLresult result = (*m_musicPlayInterface)->SetPlayState(m_musicPlayInterface, SL_PLAYSTATE_STOPPED);
    assert(result == 0 && "AudioManager::Could not stop music");
}