#ifndef PEW_PEW_AUDIOPLAYER_H
#define PEW_PEW_AUDIOPLAYER_H


#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include "AudioTrack.h"

class AudioPlayer
{
public:
    enum class AudioState
    {
        STOPPED,
        PAUSED,
        PLAYING,
        SIZE
    };

    AudioPlayer(const SLEngineItf a_engineInterface, const SLObjectItf a_outputMixInterface, long a_sampleRate);
    ~AudioPlayer();

    void LoadTrack(const AudioTrack* a_track);
    void Play();
    void Pause();
    void Resume();
    void Stop();

    long GetSampleRate(){ return m_sampleRate; }
    bool Empty(){ return m_isEmpty; }

    void AudioBufferQueueCallback(SLAndroidSimpleBufferQueueItf a_bufferQueue);

private:
    bool m_isEmpty;

    AudioState m_audioState;

    SLObjectItf m_audioPlayerInterface;
    SLPlayItf m_audioPlayInterface;
    SLVolumeItf m_audioVolumeInterface;
    SLAndroidSimpleBufferQueueItf m_audioBufferQueueInterface;
    long m_sampleRate;
};

extern void BufferQueuePlayerCallback(SLAndroidSimpleBufferQueueItf a_bufferQueue, void *a_data);


#endif //PEW_PEW_AUDIOPLAYER_H