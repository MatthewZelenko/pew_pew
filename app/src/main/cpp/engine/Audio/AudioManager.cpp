#include "AudioManager.h"
#include "Log.h"
#include "Application.h"
#include <android/asset_manager.h>
#include <vorbis/vorbisfile.h>
#include <jni.h>
#include "AudioTrack.h"
#include "AudioPlayer.h"
#include "MusicPlayer.h"

long ANDROID_SAMPLE_RATE = 0;

AudioManager::AudioManager() :
        m_outputMixInterface(nullptr),
        m_engineInterface(nullptr),
        m_volumeInterface(nullptr),
        m_engine(nullptr)
{
    CreateEngine();

    //Get Android Sample Rate
    ANativeActivity* activity = Application::m_androidState->activity;
    JNIEnv *env;
    activity->vm->AttachCurrentThread( &env, NULL );
    //Retrieve app name
    jclass nativeClass = env->GetObjectClass( activity->clazz );
    jfieldID fid = env->GetFieldID(nativeClass, "ANDROID_SAMPLE_RATE", "I" );
    ANDROID_SAMPLE_RATE = env->GetIntField(activity->clazz, fid);
    activity->vm->DetachCurrentThread();
}

AudioManager::~AudioManager()
{
    Clear();
    //output mix
    if(m_outputMixInterface)
        (*m_outputMixInterface)->Destroy(m_outputMixInterface);

    //engine
    if(m_engine)
        (*m_engine)->Destroy(m_engine);
}

void AudioManager::CreateEngine()
{
    SLresult result = slCreateEngine(&m_engine, 0, NULL, 0, NULL, NULL);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not create engine. 1");
        return;
    }
    result = (*m_engine)->Realize(m_engine, SL_BOOLEAN_FALSE);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not create engine. 2");
        return;
    }
    result = (*m_engine)->GetInterface(m_engine, SL_IID_ENGINE, &m_engineInterface);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not create engine. 3");
        return;
    }



    const SLInterfaceID  ids[] = { };
    const SLboolean req[] = { };

    result = (*m_engineInterface)->CreateOutputMix(m_engineInterface, &m_outputMixInterface, 0, ids, req);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not create engine. 4");
        return;
    }

    result = (*m_outputMixInterface)->Realize(m_outputMixInterface, SL_BOOLEAN_FALSE);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not create engine. 5");
        return;
    }
}


const AudioTrack* const AudioManager::PreloadAudio(const std::string &a_path)
{
    if(m_loadedAudioTracks.find(a_path) == m_loadedAudioTracks.end())
    {
        m_loadedAudioTracks[a_path] = new AudioTrack(a_path);
    }
    return m_loadedAudioTracks[a_path];
}
AudioPlayer* const AudioManager::AttachAudioToPlayer(const std::string &a_path)
{
    const AudioTrack* track = PreloadAudio(a_path);
    for (int i = 0; i < m_audioPlayers.size(); ++i)
    {
        if(m_audioPlayers[i]->Empty() && m_audioPlayers[i]->GetSampleRate() == track->GetSampleRate())
        {
            m_audioPlayers[i]->LoadTrack(track);
            return m_audioPlayers[i];
        }
    }

    if(m_audioPlayers.size() >= 32)
    {
        //TODO:Handle old CACHE
        Log::Debug("AudioManager::Reached max audio player count");
    }
    else
    {
        AudioPlayer* audioPlayer = new AudioPlayer(m_engineInterface, m_outputMixInterface, track->GetSampleRate());
        audioPlayer->LoadTrack(track);
        m_audioPlayers.push_back(audioPlayer);
        return audioPlayer;
    }
    return nullptr;
}
void AudioManager::ClearAudioTracks()
{
    for (int i = 0; i < m_audioPlayers.size(); ++i)
    {
        m_audioPlayers[i]->Stop();
    }

    for (auto iter = m_loadedAudioTracks.begin(); iter != m_loadedAudioTracks.end(); ++iter)
    {
        delete (*iter).second;
    }
    m_loadedAudioTracks.clear();
}
void AudioManager::ClearAudioPlayers()
{
    for (int i = 0; i < m_audioPlayers.size(); ++i)
    {
        delete m_audioPlayers[i];
    }
    m_audioPlayers.clear();
}

MusicPlayer* const AudioManager::GetMusicPlayer(const std::string &a_path)
{
    if(m_loadledMusicPlayers.find(a_path) == m_loadledMusicPlayers.end())
        m_loadledMusicPlayers[a_path] = new MusicPlayer(a_path, m_engineInterface, m_outputMixInterface);
    return m_loadledMusicPlayers[a_path];
}
void AudioManager::ClearMusicPlayers()
{
    for (auto iter = m_loadledMusicPlayers.begin(); iter != m_loadledMusicPlayers.end(); ++iter)
    {
        delete (*iter).second;
    }
    m_loadledMusicPlayers.clear();
}
void AudioManager::ClearMusicPlayer(const std::string &a_path)
{
    if(m_loadledMusicPlayers.find(a_path) != m_loadledMusicPlayers.end())
    {
        delete m_loadledMusicPlayers[a_path];
        m_loadledMusicPlayers.erase(a_path);
    }

}

void AudioManager::Clear()
{
    ClearAudioPlayers();
    ClearAudioTracks();
    ClearMusicPlayers();
}