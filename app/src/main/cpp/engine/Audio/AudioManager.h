#ifndef PEW_PEW_AUDIOMANAGER_H
#define PEW_PEW_AUDIOMANAGER_H


#include <SLES/OpenSLES.h>
#include <string>
#include <SLES/OpenSLES_Android.h>
#include <unordered_map>
#include <vector>

extern long ANDROID_SAMPLE_RATE;

class AudioTrack;
class AudioPlayer;
class MusicPlayer;

class AudioManager
{
public:

    AudioManager();
    ~AudioManager();

    void Clear();

    //==========_AUDIO_PLAYER_==========//
    const AudioTrack* const PreloadAudio(const std::string& a_path);
    AudioPlayer* const AttachAudioToPlayer(const std::string& a_path);
    void ClearAudioTracks();
    void ClearAudioPlayers();




    //==========_MUSIC_PLAYER_==========//
    /*Call to preload music. Call to get loaded music player or load in music player and return. */
    MusicPlayer* const GetMusicPlayer(const std::string& a_path);
    void ClearMusicPlayer(const std::string& a_path);
    void ClearMusicPlayers();




private:
    std::unordered_map<std::string, MusicPlayer*> m_loadledMusicPlayers;
    std::unordered_map<std::string, AudioTrack*> m_loadedAudioTracks;
    std::vector<AudioPlayer*> m_audioPlayers;

    //std::unordered_map<std::string, int/*Index to AudioPlayer vector*/> m_occupiedAudioPlayers;


    SLObjectItf m_engine, m_outputMixInterface;
    SLVolumeItf m_volumeInterface;
    SLEngineItf m_engineInterface;

    void CreateEngine();
};


#endif //PEW_PEW_AUDIOMANAGER_H