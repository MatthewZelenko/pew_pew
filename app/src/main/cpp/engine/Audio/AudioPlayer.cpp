#include <assert.h>
#include <Log.h>
#include "AudioPlayer.h"

AudioPlayer::AudioPlayer(const SLEngineItf a_engineInterface, const SLObjectItf a_outputMixInterface, long a_sampleRate) : m_audioPlayerInterface(nullptr),
                             m_audioPlayInterface(nullptr),
                             m_audioVolumeInterface(nullptr),
                             m_audioBufferQueueInterface(nullptr),
                             m_sampleRate(a_sampleRate), m_audioState(AudioState::STOPPED)
{
    SLDataLocator_AndroidSimpleBufferQueue locatorBufferQueue = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, a_sampleRate == 48000 ? SL_SAMPLINGRATE_48 : SL_SAMPLINGRATE_44_1, SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16, SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};

    SLDataSource audioSource = {&locatorBufferQueue, &format_pcm};

    SLDataLocator_OutputMix locatorOutputMix = {SL_DATALOCATOR_OUTPUTMIX, a_outputMixInterface};
    SLDataSink audioSink = { &locatorOutputMix, NULL };


    const SLInterfaceID ids[] = {SL_IID_BUFFERQUEUE, SL_IID_VOLUME};
    const SLboolean req[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};

    SLresult result = (*a_engineInterface)->CreateAudioPlayer(a_engineInterface, &m_audioPlayerInterface, &audioSource, &audioSink, 2, ids, req);
    assert(result == 0 && "AudioManager::Could not create audio. 1");

    result = (*m_audioPlayerInterface)->Realize(m_audioPlayerInterface, SL_BOOLEAN_FALSE);
    assert(result == 0 && "AudioManager::Could not create audio. 2");

    result = (*m_audioPlayerInterface)->GetInterface(m_audioPlayerInterface, SL_IID_PLAY, &m_audioPlayInterface);
    assert(result == 0 && "AudioManager::Could not create audio. 3");

    result = (*m_audioPlayerInterface)->GetInterface(m_audioPlayerInterface, SL_IID_BUFFERQUEUE, &m_audioBufferQueueInterface);
    assert(result == 0 && "AudioManager::Could not create audio. 4");

    result = (*m_audioPlayerInterface)->GetInterface(m_audioPlayerInterface, SL_IID_VOLUME, &m_audioVolumeInterface);
    assert(result == 0 && "AudioManager::Could not create audio. 5");

    result = (*m_audioBufferQueueInterface)->RegisterCallback(m_audioBufferQueueInterface, BufferQueuePlayerCallback, this);
    assert(result == 0 && "AudioManager::Could not create audio. 6");
}
AudioPlayer::~AudioPlayer()
{
    Stop();
    if(m_audioPlayerInterface)
        (*m_audioPlayerInterface)->Destroy(m_audioPlayerInterface);
}

void AudioPlayer::AudioBufferQueueCallback(SLAndroidSimpleBufferQueueItf a_bufferQueue)
{
    Stop();
}

void AudioPlayer::Play()
{
    if (!m_audioPlayInterface)
        return;

    if(m_audioState != AudioState::STOPPED)
        Stop();

    Resume();
}

void AudioPlayer::Stop()
{
    SLresult result = (*m_audioPlayInterface)->SetPlayState(m_audioPlayInterface, SL_PLAYSTATE_STOPPED);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not stop audio track");
        return;
    }
    (*m_audioBufferQueueInterface)->Clear(m_audioBufferQueueInterface);
    m_isEmpty = true;
}

void AudioPlayer::Pause()
{
    SLresult result = (*m_audioPlayInterface)->SetPlayState(m_audioPlayInterface, SL_PLAYSTATE_PAUSED);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not play audio track");
        return;
    }
}

void AudioPlayer::Resume()
{
    SLresult result = (*m_audioPlayInterface)->SetPlayState(m_audioPlayInterface, SL_PLAYSTATE_PLAYING);
    if(result != 0)
    {
        Log::Error("AudioManager::Could not play audio track");
        return;
    }
}

void AudioPlayer::LoadTrack(const AudioTrack *a_track)
{
    Stop();
    SLresult result = (*m_audioBufferQueueInterface)->Enqueue(m_audioBufferQueueInterface, a_track->GetData(), (SLuint32)a_track->GetDataSize());
    if(result != 0)
    {
        Log::Error("AudioManager::Could not load audio track:%s", a_track->GetPath().c_str());
        return;
    }
}


void BufferQueuePlayerCallback(SLAndroidSimpleBufferQueueItf a_bufferQueue, void *a_data)
{
    static_cast<AudioPlayer*>(a_data)->AudioBufferQueueCallback(a_bufferQueue);
}
