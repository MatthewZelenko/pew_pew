#include <glm/gtc/matrix_transform.hpp>
#include "Camera.h"
#include "Application.h"

Camera* Camera::m_mainCamera = nullptr;
Camera::Camera() : m_position(glm::vec2(0.0f,0.0f)), m_scale(glm::vec2(1.0f,1.0f)), m_rotation(0.0f)
{
    if(m_mainCamera == nullptr)
        m_mainCamera = this;
}

Camera::~Camera()
{

}

glm::mat4 Camera::GetView()
{
    //TODO::Store pos scale and rot in matrix instead
    glm::mat4 view(1.0f);
    view = glm::translate(view, glm::vec3(-m_position.x, -m_position.y, 0.0f));
    view = glm::scale(view, glm::vec3(m_scale.x, m_scale.y, 1.0f));
    view = glm::rotate(view, m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));
    return view;
}

void Camera::SetProjection(float a_left, float a_right, float a_bottom, float a_top, float a_near, float a_far)
{
    m_gameWidth = a_right - a_left;
    m_gameHeight = a_top - a_bottom;
    m_projection = glm::ortho(a_left, a_right, a_bottom, a_top, a_near, a_far);
}

glm::vec2 Camera::DisplayToGame(const glm::vec2 &a_value)
{
    if(a_value.x == 0 && a_value.y == 0)
        return glm::vec2();
    glm::vec2 ret = a_value;
    ret.x /= Application::m_window.GetDisplayWidth();
    ret.y /= Application::m_window.GetDisplayHeight();

    ret.x *= m_gameWidth;
    ret.y *= m_gameHeight;

    return ret;
}
