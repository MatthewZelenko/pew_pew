#ifndef PEW_PEW_GLYPH_H
#define PEW_PEW_GLYPH_H

class Glyph
{
public:
    Glyph();
    Glyph(float a_texX, float a_texY, float a_texW, float a_texH);
    ~Glyph();

    float GetX()const { return m_texX; }
    float GetY()const { return m_texY; }
    float GetW()const { return m_texW; }
    float GetH()const { return m_texH; }

private:
    float m_texX, m_texY, m_texW, m_texH;

};

#endif //PEW_PEW_GLYPH_H