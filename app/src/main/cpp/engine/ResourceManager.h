#ifndef PEWPEW_RESOURCEMANAGER_H
#define PEWPEW_RESOURCEMANAGER_H


#include <unordered_map>
#include <string>
#include <Font.h>
#include <Sprite.h>
#include <Animation.h>
#include <Shader.h>
#include <Texture.h>

class ResourceManager
{
public:
    ResourceManager();
    ~ResourceManager();

    Font* LoadFont(const std::string& a_path, const std::string& a_name);
    Texture* LoadTexture(const std::string& a_path, const std::string& a_name);
    Shader* LoadShader(const std::string& a_path, const std::string& a_name);

    Font* GetFont(const std::string& a_name);
    Sprite GetSprite(const std::string& a_name);
    Animation GetAnimation(const std::string& a_name);
    Texture* GetTexture(const std::string& a_name);
    Shader* GetShader(const std::string& a_name);

    void Destroy();

private:
    std::unordered_map<std::string, Texture*> m_textures;
    std::unordered_map<std::string, Sprite> m_sprites;
    std::unordered_map<std::string, Animation> m_animations;
    std::unordered_map<std::string, Shader*> m_shaders;
    std::unordered_map<std::string, Font*> m_fonts;

    bool LoadSprites(Texture* a_texture,const std::string& a_path);
    bool LoadAnimations(const std::string& a_path);
};


#endif //PEWPEW_RESOURCEMANAGER_H