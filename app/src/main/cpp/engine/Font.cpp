#include <android/asset_manager.h>
#include <sstream>
#include "Font.h"
#include "Texture.h"
#include "Application.h"
#include "Log.h"

Font::Font(const std::string &a_filepath) : m_texture(nullptr)
{
    std::string name = a_filepath.substr(0, a_filepath.find('.'));

    m_texture = new Texture(a_filepath);
    std::string fullPath = name + ".txt";

    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      fullPath.c_str(), AASSET_MODE_BUFFER);
    if (file == nullptr)
    {
        Log::Error("Font file not find: %s", fullPath.c_str());
        return;
    }

    off_t size = AAsset_getLength(file);
    char* bytes = new char[size + 1];
    AAsset_read(file, bytes, size);
    AAsset_close(file);
    bytes[size] = 0;

    std::stringstream stream(bytes);

    char c;

    int setting = 0;
    char glyph = 0;
    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;

    while(stream.get(c))
    {
        if(c >= 48 && c <= 57)
        {
            if(setting == 0)
            {
                glyph = c;
            }
            else if(setting == 1)
            {
                x *= 10;
                x += (c - 48);
            }
            else if(setting == 2)
            {
                y *= 10;
                y += (c - 48);
            }
            else if(setting == 3)
            {
                w *= 10;
                w += (c - 48);
            }
            else if(setting == 4)
            {
                h *= 10;
                h += (c - 48);
            }
        }
        else if(c == ':' || c == ',')
        {
            setting++;
        }
        else if(c == ' ' || c == '\r' || c == '\n')
        {
            if(glyph != 0)
            {
                m_glyphs[glyph] = Glyph(x / (float)m_texture->GetWidth(), y / (float)m_texture->GetHeight(), w / (float)m_texture->GetWidth(), h / (float)m_texture->GetHeight());
            }

            glyph = 0;
            x = 0;
            y = 0;
            w = 0;
            h = 0;
            setting = 0;
        }
    }
    if(glyph != 0)
    {
        m_glyphs[glyph] = Glyph(x / (float)m_texture->GetWidth(), y / (float)m_texture->GetHeight(), w / (float)m_texture->GetWidth(), h/ (float)m_texture->GetHeight());
    }

    delete[] bytes;
}
Font::~Font()
{
    if (m_texture)
        delete m_texture;
    m_texture = nullptr;
}

const Glyph* Font::GetGlpyh(const char a_glyph)
{
    if(m_glyphs.find(a_glyph) != m_glyphs.end())
        return &(m_glyphs[a_glyph]);
    return nullptr;
}
