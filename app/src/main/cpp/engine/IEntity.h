#ifndef PEWPEW_IENTITY_H
#define PEWPEW_IENTITY_H

#include "Engine.h"
#include "Types.h"
#include "ComponentManager.h"

class TransformComponent;

class IEntity
{
public:
    IEntity();
    virtual ~IEntity();

    virtual void Load(){}
    virtual void Unload(){}

    virtual void Update(double a_deltaTime){}
    virtual void LateUpdate(double a_deltaTime){}
    virtual void FixedUpdate(double a_timeStep){}


    template <class T>
    T* AddComponent();
    template <class T, class ...Args>
    T* AddComponent(Args&&... args);
    template<class T>
    T* GetComponent();
    template<class T>
    void RemoveComponent();
    void RemoveAllComponents();

    Handle GetHandle() { return m_handle; }

    void SetActive(bool a_val) { m_isActive = a_val; }
    bool GetActive() { return m_isActive; }

    TransformComponent* GetTransformComponent();

private:
    friend class EntityManager;

    Handle m_handle;
    bool m_isActive;

    TransformComponent* m_transform;
};

template <class T>
T *IEntity::AddComponent()
{
    //TODO:: Add parameters to constructor using ARGS&&
    return Engine::Get()->GetComponentManager()->CreateComponent<T>(m_handle);
}
template <class T, class ...Args>
T *IEntity::AddComponent(Args&&... args)
{
    //TODO:: Add parameters to constructor using ARGS&&
    return Engine::Get()->GetComponentManager()->CreateComponent<T>(m_handle, std::forward<Args>(args)...);
}

template<class T>
T *IEntity::GetComponent()
{
    return Engine::Get()->GetComponentManager()->GetComponent<T>(m_handle);
}
template<class T>
void IEntity::RemoveComponent()
{
    Engine::Get()->GetComponentManager()->RemoveComponent<T>(m_handle);
}


#endif //PEWPEW_IENTITY_H