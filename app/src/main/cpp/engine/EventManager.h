#ifndef PEW_PEW_EVENTMANAGER_H
#define PEW_PEW_EVENTMANAGER_H

#include <unordered_map>
#include <string>
#include <vector>

class EventManager;

class IEventListener
{
public:
    IEventListener();
    virtual ~IEventListener();

    virtual void OnEvent(const std::string& a_event, const void* a_data) = 0;

private:
    friend EventManager;
    int m_eventIndex;
};

class EventManager
{
public:


    EventManager();
    ~EventManager();

    //Event to listen to and the listener being assigned to it.
    void Subscribe(const std::string& a_event, IEventListener* a_listener);
    void Unsubscribe(const std::string& a_event, IEventListener* a_listener);
    void Send(const std::string& a_event, const void* a_data);

    void Clear();

private:
    std::unordered_map<std::string, std::vector<IEventListener*>> m_listeners;
};


#endif //PEW_PEW_EVENTMANAGER_H