#include "EventManager.h"

IEventListener::IEventListener(): m_eventIndex(-1)
{
}
IEventListener::~IEventListener()
{

}



EventManager::EventManager()
{

}
EventManager::~EventManager()
{

}

void EventManager::Subscribe(const std::string &a_event, IEventListener *a_listener)
{
    a_listener->m_eventIndex = m_listeners[a_event].size();
    m_listeners[a_event].push_back(a_listener);
}

void EventManager::Unsubscribe(const std::string &a_event, IEventListener *a_listener)
{
    assert(m_listeners[a_event].size() != 0 && "Unsubscribing to no events");

    std::vector<IEventListener*>& listeners =  m_listeners[a_event];


    if(listeners.size() == 1 || a_listener->m_eventIndex == listeners.size() - 1)
    {
        listeners.pop_back();
    }
    else
    {
        IEventListener *lastListener = listeners[listeners.size() - 1];
        listeners[a_listener->m_eventIndex] = lastListener;
        lastListener->m_eventIndex = a_listener->m_eventIndex;
        listeners.pop_back();
    }
}

void EventManager::Send(const std::string &a_event, const void *a_data)
{
    const std::vector<IEventListener*>& listeners=  m_listeners[a_event];
    for (int i = 0; i < listeners.size(); ++i)
    {
        listeners[i]->OnEvent(a_event, a_data);
    }
}

void EventManager::Clear()
{
    m_listeners.clear();
}