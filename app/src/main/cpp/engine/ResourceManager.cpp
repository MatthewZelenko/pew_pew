#include <android/asset_manager.h>
#include <sstream>
#include "ResourceManager.h"
#include "Application.h"
#include "Log.h"
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

ResourceManager::ResourceManager()
{

}
ResourceManager::~ResourceManager()
{

}

void ResourceManager::Destroy()
{
    for (auto iter = m_fonts.begin(); iter != m_fonts.end(); ++iter)
    {
        delete iter->second;
    }
    m_fonts.clear();

    for (auto iter = m_textures.begin(); iter != m_textures.end(); ++iter)
    {
        delete iter->second;
    }
    m_textures.clear();
    m_sprites.clear();

    for (auto iter = m_shaders.begin(); iter != m_shaders.end(); ++iter)
    {
        delete iter->second;
    }
    m_shaders.clear();
}

Font *ResourceManager::LoadFont(const std::string& a_path, const std::string& a_name)
{
    std::string name = a_name.substr(0, a_name.find("."));

    if(m_fonts.find(name) == m_fonts.end())
        m_fonts[name] = new Font(a_path + a_name);
    return m_fonts[name];
}
Texture *ResourceManager::LoadTexture(const std::string& a_path, const std::string& a_name)
{
    std::string name = a_name.substr(0, a_name.find("."));

    if(m_textures.find(name) == m_textures.end())
    {
        m_textures[name] = new Texture(a_path + a_name);
        if(!LoadSprites(m_textures[name], a_path + name + ".sprites"))
        {
            m_sprites[name] = Sprite(m_textures[name]);
        }
        LoadAnimations(a_path + name + ".animation");
    }
    return m_textures[name];
}
Shader *ResourceManager::LoadShader(const std::string& a_path, const std::string& a_name)
{
    std::string name = a_name.substr(0, a_name.find("."));

    if(m_shaders.find(name) == m_shaders.end())
        m_shaders[name] = new Shader(a_path + a_name);
    return m_shaders[name];
}
bool ResourceManager::LoadSprites(Texture* a_texture, const std::string& a_path)
{
    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      a_path.c_str(), AASSET_MODE_BUFFER);
    if (file == nullptr)
    {
        return false;
    }

    off_t size = AAsset_getLength(file);
    char* bytes = new char[size + 1];
    AAsset_read(file, bytes, size);
    AAsset_close(file);

    bytes[size] = 0;

    rapidjson::Document doc;
    doc.Parse(bytes);
    for (auto iter = doc.Begin(); iter != doc.End(); ++iter)
    {
        const rapidjson::Value& object = *iter;

        std::string name;
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        float ox = 0;
        float oy = 0;


        if(object.HasMember("name"))
        {
            name = object["name"].GetString();
        }
        if(object.HasMember("x"))
        {
            x = object["x"].GetInt();
        }
        if(object.HasMember("y"))
        {
            y = object["y"].GetInt();
        }
        if(object.HasMember("w"))
        {
            w = object["w"].GetInt();
        }
        if(object.HasMember("h"))
        {
            h = object["h"].GetInt();
        }
        if(object.HasMember("ox"))
        {
            ox = object["ox"].GetFloat();
        }
        if(object.HasMember("oy"))
        {
            oy = object["oy"].GetFloat();
        }

        if(name != "")
        {
            m_sprites[name] = Sprite(a_texture, Rect(x, y, w, h), glm::vec2(ox, oy));
        }
    }


    delete[] bytes;
    return true;
}
bool ResourceManager::LoadAnimations(const std::string &a_path)
{
    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                      a_path.c_str(), AASSET_MODE_BUFFER);
    if (file == nullptr)
    {
        return false;
    }

    off_t size = AAsset_getLength(file);
    char* bytes = new char[size + 1];
    AAsset_read(file, bytes, size);
    AAsset_close(file);
    bytes[size] = 0;


    rapidjson::Document doc;
    doc.Parse(bytes);
    for (auto iter = doc.Begin(); iter != doc.End(); ++iter)
    {
        const rapidjson::Value& object = *iter;

        std::string animationName;

        if(object.HasMember("name"))
        {
            animationName = object["name"].GetString();
        }
        if(object.HasMember("sprites"))
        {
            std::vector<Sprite> sprites;
            std::vector<float> frameTimes;

            const auto spritesArray = object["sprites"].GetArray();
            for (auto iterSprite = spritesArray.Begin(); iterSprite != spritesArray.End(); ++iterSprite)
            {
                const rapidjson::Value& spriteObject = *iterSprite;

                if(spriteObject.HasMember("name"))
                {
                    sprites.push_back(m_sprites[spriteObject["name"].GetString()]);
                }
                if(spriteObject.HasMember("frameTime"))
                {
                    frameTimes.push_back(spriteObject["frameTime"].GetFloat());
                }
            }

            m_animations[animationName] = Animation(sprites, frameTimes);
        }
    }

    delete[] bytes;
    return true;

}

Font *ResourceManager::GetFont(const std::string &a_name)
{
    if (m_fonts.find(a_name) != m_fonts.end())
    {
        return m_fonts[a_name];
    }
    return nullptr;
}
Shader* ResourceManager::GetShader(const std::string &a_name)
{
    if(m_shaders.find(a_name) != m_shaders.end())
    {
        return m_shaders[a_name];
    }
    return nullptr;
}
Texture *ResourceManager::GetTexture(const std::string &a_name)
{
    if(m_textures.find(a_name) != m_textures.end())
    {
        return m_textures[a_name];
    }
    return nullptr;
}
Sprite ResourceManager::GetSprite(const std::string &a_name)
{
    if(m_sprites.find(a_name) != m_sprites.end())
    {
        return m_sprites[a_name];
    }
    return Sprite();
}
Animation ResourceManager::GetAnimation(const std::string &a_name)
{
    if(m_animations.find(a_name) != m_animations.end())
    {
        return m_animations[a_name];
    }
    return Animation();
}