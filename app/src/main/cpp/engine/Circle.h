#ifndef PEW_PEW_CIRCLE_H
#define PEW_PEW_CIRCLE_H


#include <glm/vec2.hpp>

class Circle
{
public:
    Circle();
    Circle(float a_x, float a_y, float a_radius);
    ~Circle();

    Circle& operator+=(const glm::vec2& a_rhs);

    void SetPosition(float a_x, float a_y);
    void SetPosition(const glm::vec2& a_position);
    void SetRadius(float a_radius);

    glm::vec2 GetPosition(){return glm::vec2(m_x, m_y);}
    float GetRadius(){return m_radius;}

    bool Contains(const glm::vec2& a_position);
    bool Intersects(const Circle& a_circle);

private:
    float m_x, m_y, m_radius;
};


#endif //PEW_PEW_CIRCLE_H