#include "EntityManager.h"

EntityManager::EntityManager()
{

}
EntityManager::~EntityManager()
{
    Destroy();
}

void EntityManager::Update(double a_deltaTime)
{
    int size = m_entities.size();
    for (int i = size - 1; i >= 0; --i)
    {
        if(m_entities[i] && m_entities[i]->m_isActive)
            m_entities[i]->Update(a_deltaTime);
    }
}
void EntityManager::LateUpdate(double a_deltaTime)
{
    int size = m_entities.size();
    for (int i = size - 1; i >= 0; --i)
    {
        if(m_entities[i] && m_entities[i]->m_isActive)
            m_entities[i]->LateUpdate(a_deltaTime);
    }
}
void EntityManager::FixedUpdate(double a_timeStep)
{
    int size = m_entities.size();
    for (int i = size - 1; i >= 0; --i)
    {
        if(m_entities[i] && m_entities[i]->m_isActive)
            m_entities[i]->FixedUpdate(a_timeStep);
    }
}


void EntityManager::Destroy()
{
    for (int i = 0; i < m_entities.size(); ++i)
    {
        if(!m_entities[i])
            continue;
        m_entities[i]->Unload();
        m_entities[i]->RemoveAllComponents();
        delete m_entities[i];
    }
    m_entities.clear();
    m_freeHandles.clear();
}

void EntityManager::ProcessDestroys()
{
    for (int i = 0; i < m_deletes.size(); ++i)
    {
        DestroyEntityImmediately(m_deletes[i]);
    }
    m_deletes.clear();
}

void EntityManager::DestroyEntityImmediately(Handle a_handle)
{
    if(a_handle >= m_entities.size() || m_entities[a_handle] == nullptr)
        return;

    m_entities[a_handle]->Unload();
    m_entities[a_handle]->RemoveAllComponents();
    m_entities[a_handle]->m_transform = nullptr;
    delete m_entities[a_handle];
    m_entities[a_handle] = nullptr;
    m_freeHandles.push_back(a_handle);
}

void EntityManager::DestroyEntity(Handle a_handle)
{
    m_deletes.push_back(a_handle);
}