#ifndef PEWPEW_TIME_H
#define PEWPEW_TIME_H

#include <sys/time.h>
class Clock
{
public:
    Clock();

    double GetDeltaTime() { return m_deltaTime; }
    double GetTotalTime() { return m_elapsedTime; }
    double GetTimeStep() { return m_timeStep; }

    void ResetDeltaTime();

private:
    friend class Engine;
    timeval m_systemTime;

    double m_deltaTime, m_elapsedTime;
    double m_elapsedTimeStep, m_timeStep;

    void Update();
};


#endif //PEWPEW_TIME_H
