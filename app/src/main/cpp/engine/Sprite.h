#ifndef PEW_PEW_SPRITE_H
#define PEW_PEW_SPRITE_H

#include "Rect.h"

class Texture;

class Sprite
{
public:
    Sprite();
    Sprite(Texture* a_texture);
    Sprite(Texture* a_texture, const Rect& a_textureRect, const glm::vec2& a_origin = glm::vec2(0.0f, 0.0f));
    ~Sprite();

    inline const Texture* GetTexture() { return m_texture; }
    //Rect in ndc space
    inline const Rect& GetRect() { return m_rect; }
    //Rect in pixel space
    inline const Rect& GetTextureRect() { return m_textureRect; }
    inline const glm::vec2& GetOrigin() { return m_origin; }

private:
    Texture* m_texture;
    Rect m_rect, m_textureRect;
    glm::vec2 m_origin;
};


#endif //PEW_PEW_SPRITE_H