#ifndef PEWPEW_COMPONENTMANAGER_H
#define PEWPEW_COMPONENTMANAGER_H

#include <unordered_map>
#include <vector>
#include "components/IComponent.h"
#include "Types.h"

class ComponentManager
{
public:
    ComponentManager();
    ~ComponentManager();

    template <class T>
    T* CreateComponent(Handle a_entityHandle);
    template <class T, class ...Args>
    T* CreateComponent(Handle a_entityHandle, Args&&... args);
    template <class T>
    T* GetComponent(Handle a_entityHandle);
    template <class T>
    void RemoveComponent(Handle a_entityHandle);
    void RemoveAllComponents(Handle a_entityHandle);

    void Destroy();

    template <class T>
    std::vector<IComponent*>::iterator Begin();
    template <class T>
    std::vector<IComponent*>::iterator End();

    template <class T>
    std::vector<IComponent*> GetVector();

private:
    std::unordered_map<Handle, std::unordered_map<TypeID, Handle>> m_entityComponentMap;
    std::unordered_map<TypeID , std::vector<Handle>> m_freeComponents;
    std::unordered_map<TypeID , std::vector<IComponent*>> m_components;


};

template <class T>
T *ComponentManager::CreateComponent(Handle a_entityHandle)
{
    T* component = nullptr;

    TypeID typeID = T::ComponentTypeID();

    //Check if entity already has component
    component = GetComponent<T>(a_entityHandle);
    if(component != nullptr)
        return component;

    component = new T();

    //Check free slots
    if(!m_freeComponents[typeID].empty())
    {
        int handle = m_freeComponents[typeID][m_freeComponents[typeID].size() - 1];
        m_freeComponents[typeID].pop_back();
        m_components[typeID][handle] = component;
        component->m_handle = handle;
    }
    else //Push back
    {
        component->m_handle = m_components[typeID].size();
        m_components[typeID].push_back(component);
    }
    //Set handles, Load and return
    component->m_parentHandle = a_entityHandle;
    m_entityComponentMap[a_entityHandle][typeID] = component->m_handle;
    component->Load();
    assert(component && "NO COMPONENT DELIVERED DA FUQ");
    return component;
}
template <class T, class ...Args>
T *ComponentManager::CreateComponent(Handle a_entityHandle, Args&&... args)
{
    T* component = nullptr;

    TypeID typeID = T::ComponentTypeID();

    //Check if entity already has component
    component = GetComponent<T>(a_entityHandle);
    if(component != nullptr)
        return component;

    component = new T();

    //Check free slots
    if(!m_freeComponents[typeID].empty())
    {
        int handle = m_freeComponents[typeID][m_freeComponents[typeID].size() - 1];
        m_freeComponents[typeID].pop_back();
        m_components[typeID][handle] = component;
        component->m_handle = handle;
    }
    else //Push back
    {
        component->m_handle = m_components[typeID].size();
        m_components[typeID].push_back(component);
    }
    //Set handles, Load and return
    component->m_parentHandle = a_entityHandle;
    m_entityComponentMap[a_entityHandle][typeID] = component->m_handle;
    component->Load(std::forward<Args>(args)...);
    return component;
}

template<class T>
T *ComponentManager::GetComponent(Handle a_entityHandle)
{
    TypeID typeID = T::ComponentTypeID();

    if(m_entityComponentMap.find(a_entityHandle) == m_entityComponentMap.end() || m_entityComponentMap[a_entityHandle].find(typeID) == m_entityComponentMap[a_entityHandle].end())
        return nullptr;

    return static_cast<T*>(m_components[typeID][m_entityComponentMap[a_entityHandle][typeID]]);
}

template<class T>
void ComponentManager::RemoveComponent(Handle a_entityHandle)
{
    TypeID typeID = T::ComponentTypeID();

    if(m_entityComponentMap.find(a_entityHandle) == m_entityComponentMap.end() || m_entityComponentMap[a_entityHandle].find(typeID) == m_entityComponentMap[a_entityHandle].end())
    {
        return;
    }
    Handle handle = m_entityComponentMap[a_entityHandle][typeID];
    m_entityComponentMap[a_entityHandle].erase(typeID);

    m_components[typeID][handle]->Unload();
    delete m_components[typeID][handle];
    m_components[typeID][handle] = nullptr;

    m_freeComponents[typeID].push_back(handle);
}

template<class T>
std::vector<IComponent*>::iterator ComponentManager::Begin()
{
    TypeID typeID = T::ComponentTypeID();
    return m_components[typeID].begin();
}

template<typename T>
std::vector<IComponent*>::iterator ComponentManager::End()
{
    TypeID typeID = T::ComponentTypeID();
    return m_components[typeID].end();
}

template<class T>
std::vector<IComponent*> ComponentManager::GetVector()
{
    TypeID typeID = T::ComponentTypeID();
    return m_components[typeID];
}


#endif //PEWPEW_COMPONENTMANAGER_H