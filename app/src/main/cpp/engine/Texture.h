#ifndef PEWPEW_TEXTURE_H
#define PEWPEW_TEXTURE_H


#include <string>

class Texture
{
public:
    Texture(const std::string &a_filepath);
    ~Texture();


    void Bind()const;

    static void Unbind();

    inline const int GetWidth()const { return m_width; }
    inline const int GetHeight()const { return m_height; }
    inline const int GetChannels()const { return m_channels; }
    inline const int GetID()const { return m_textureID; }


private:
    unsigned int m_textureID;
    int m_width, m_height, m_channels;

    void Create(const std::string &a_filepath);
    void Destroy();
};


#endif //PEWPEW_TEXTURE_H