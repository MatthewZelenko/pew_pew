#ifndef PEWPEW_ENGINE_H
#define PEWPEW_ENGINE_H

class SystemManager;
class ComponentManager;
class EntityManager;
class ResourceManager;
class Input;
class Clock;
class EventManager;
class AudioManager;

class Engine
{
public:
    static Engine* Get();
    static void TerminateEngine();

    void Update(double a_deltaTime);
    void LateUpdate(double a_deltaTime);
    void FixedUpdate(double a_timeStep);
    void Render();

    inline EventManager* GetEventManager() { return m_eventManager; }
    inline SystemManager* GetSystemManager(){return m_systemManager;}
    inline ComponentManager* GetComponentManager(){return m_componentManager;}
    inline EntityManager* GetEntityManager(){return m_entityManager;}
    inline ResourceManager* GetResourceManager() { return m_resourceManager; }
    inline Input* GetInput() { return m_input; }
    inline Clock* GetClock() { return m_clock; }
    inline AudioManager* GetAudioManager(){return m_audioManager;}

private:
    static Engine* m_instance;

    EventManager* m_eventManager;
    SystemManager* m_systemManager;
    ComponentManager* m_componentManager;
    EntityManager* m_entityManager;
    ResourceManager* m_resourceManager;
    Input* m_input;
    Clock* m_clock;
    AudioManager* m_audioManager;

    Engine();
    ~Engine();
};


#endif //PEWPEW_ENGINE_H