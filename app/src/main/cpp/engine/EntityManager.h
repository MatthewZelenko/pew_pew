#ifndef PEWPEW_ENTITYMANAGER_H
#define PEWPEW_ENTITYMANAGER_H

#include <vector>
#include <unordered_map>
#include "IEntity.h"
#include "components/TransformComponent.h"

class EntityManager
{
public:
    EntityManager();
    ~EntityManager();

    void Update(double a_deltaTime);
    void LateUpdate(double a_deltaTime);
    void FixedUpdate(double a_timeStep);
    void ProcessDestroys();

    template <class T, class ...Args>
    T* CreateEntity(Args&&... args);
    template<class T>
    T* GetEntity(Handle a_handle);
    void DestroyEntityImmediately(Handle a_handle);
    void DestroyEntity(Handle a_handle);
    void Destroy();

private:
    std::vector<Handle> m_freeHandles;
    std::vector<IEntity*> m_entities;

    std::vector<Handle> m_deletes;
};


template <class T, class ...Args>
T *EntityManager::CreateEntity(Args&& ...args)
{
    //TypeID typeID = T::EntityTypeID();

    T* entity = new T();
    int handle = -1;

    if(!m_freeHandles.empty())
    {
        handle = m_freeHandles[m_freeHandles.size() - 1];
        m_freeHandles.pop_back();
        m_entities[handle] = entity;
    }
    else
    {
        handle = m_entities.size();
        m_entities.push_back(entity);
    }
    entity->m_handle = handle;
    entity->m_transform = entity->template AddComponent<TransformComponent>();
    assert(entity->m_transform && "UH OH NO TRANSFORM DA FUQ");
    entity->Load(std::forward<Args>(args)...);

    return entity;
}
template<class T>
T *EntityManager::GetEntity(Handle a_handle)
{
    //TypeID  typeID = T::EntityTypeID();
    if(a_handle >= m_entities.size())
        return nullptr;
    return static_cast<T*>(m_entities[a_handle]);
}

#endif //PEWPEW_ENTITYMANAGER_H