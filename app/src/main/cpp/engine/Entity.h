#ifndef PEWPEW_ENTITY_H
#define PEWPEW_ENTITY_H

#include "Helper.h"
#include "IEntity.h"

template<class T>
class Entity : public IEntity
{
public:
    Entity(){}
    virtual ~Entity(){}

    static TypeID EntityTypeID();

private:

};

template<class T>
TypeID  Entity<T>::EntityTypeID()
{
    static TypeID typeID = Helper::CreateEntityTypeID();
    return typeID;
}


#endif //PEWPEW_ENTITY_H