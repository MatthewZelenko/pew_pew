#ifndef PEWPEW_APPLICATION_H
#define PEWPEW_APPLICATION_H

#include "android_native_app_glue.h"
#include "EntityManager.h"
#include <EGL/egl.h>

class Application;

class Window
{
public:
    Window();

    int GetDisplayWidth(){return m_displayWidth;}
    int GetDisplayHeight(){return m_displayHeight;}

private:
    friend Application;

    int m_displayWidth, m_displayHeight;
};

class Application
{
public:
    Application();
    virtual ~Application();

    void Run(android_app* a_app);
    void Quit();

    bool IsApplicationReady(){return m_isApplicationReady;}

    static android_app* m_androidState;
    static Window m_window;

protected:
    virtual void Load(){}
    virtual void Update(double a_deltaTime){}
    virtual void LateUpdate(double a_deltaTime){}
    virtual void FixedUpdate(double a_timeStep){}
    virtual void Render(){}
    virtual void PostRender(){}
    virtual void UnLoad(){}


private:
    void SystemInit();
    void SystemUpdate();
    void SystemLateUpdate();
    void SystemFixedUpdate();
    void SystemRender();
    void SystemDestroy();

    //Creates and OpenGL context from device
    bool CreateDisplay();
    void DestroyDisplay();

    static void OnAppCmd(android_app* a_app, int32_t a_cmd);
    static int32_t OnInputEvent(android_app* a_app, AInputEvent* a_event);

    double fixedElapse;

    EGLDisplay m_display;
    EGLSurface m_surface;
    EGLContext m_context;

    bool m_isApplicationReady;
    bool m_windowResumed, m_windowFocused, m_windowCreated;
};


#endif //PEWPEW_APPLICATION_H
