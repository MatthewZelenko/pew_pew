#ifndef PEWPEW_CAMERA_H
#define PEWPEW_CAMERA_H


#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

class Camera
{
public:
    Camera();
    ~Camera();
    static void Destroy(){ m_mainCamera = nullptr; }

    void SetPosition(const glm::vec2& a_value){m_position = a_value;}
    void SetPositionX(float a_value){m_position.x = a_value;}
    void SetPositionY(float a_value){m_position.y = a_value;}
    void SetScale(const glm::vec2 &a_value){m_scale = a_value;}
    void SetRotation(float a_value){m_rotation = a_value;}

    void SetProjection(float a_left, float a_right, float a_bottom, float a_top, float a_near = 0.0f, float a_far = 1000.0f);

    glm::vec2 GetGameSize(){return glm::vec2(m_gameWidth, m_gameHeight);}

    glm::mat4 GetView();
    const glm::mat4& GetProjection() { return m_projection; }

    glm::vec2 DisplayToGame(const glm::vec2 &a_value);


    inline static Camera* MainCamera() { return m_mainCamera; }
    inline static void SetMainCamera(Camera* a_camera){m_mainCamera = a_camera;}

private:
    static Camera* m_mainCamera;

    glm::vec2 m_position, m_scale;
    float m_rotation;

    float m_gameWidth, m_gameHeight;
    glm::mat4 m_projection;
};


#endif //PEWPEW_CAMERA_H