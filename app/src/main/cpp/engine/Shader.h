#ifndef PEWPEW_SHADER_H
#define PEWPEW_SHADER_H

#include <GLES3/gl3.h>
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

struct ShaderProgramSource;

class Shader
{
public:
    Shader(const std::string& a_shaderPath);
    ~Shader();

    void Bind()const;
    static void UnBind();

    void SetUniform1f(const std::string& a_name, float a_value);
    void SetUniform2f(const std::string& a_name, glm::vec2 a_value);
    void SetUniform3f(const std::string& a_name, glm::vec3 a_value);
    void SetUniform4f(const std::string& a_name, glm::vec4 a_value);
    void SetUniform4x4(const std::string& a_name, glm::mat4x4 a_value);

    inline void SetViewProjectionLocation(const std::string &a_mvpName) { m_viewProjectionLocation = a_mvpName; }
    inline const std::string& GetViewProjectionLocation() const { return m_viewProjectionLocation; }

private:
    unsigned int m_id;
    std::unordered_map<std::string, int> m_uniformLocations;
    std::string m_viewProjectionLocation;

    void ReadShaderFromFile(const std::string& a_filepath);
    ShaderProgramSource ParseShader(const std::string& a_string);
    int CompileShader(unsigned int a_shaderType, const std::string& a_source);
    void LinkProgram(const int*a_ids);


    int Getlocation(const std::string& a_name);

    void Create(const std::string& a_shaderPath);
    void Destroy();
};


#endif //PEWPEW_SHADER_H