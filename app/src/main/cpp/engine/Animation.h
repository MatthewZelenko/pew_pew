#ifndef PEW_PEW_ANIMATION_H
#define PEW_PEW_ANIMATION_H


#include <vector>
#include <Sprite.h>

class Animation
{
public:
    Animation();
    Animation(const std::vector<Sprite>& a_sprites, const std::vector<float>& a_frameTimes);
    ~Animation();

    inline const std::vector<Sprite>& GetSprites() { return m_sprites; }
    inline const std::vector<float>& GetFrameSpeeds() { return m_frameTimes; }

private:
    std::vector<Sprite> m_sprites;
    std::vector<float> m_frameTimes;

};

#endif //PEW_PEW_ANIMATION_H