#include "Sprite.h"
#include <Texture.h>

Sprite::Sprite() : m_texture(nullptr), m_rect(0, 0, 1, 1), m_textureRect(0,0,0,0), m_origin(glm::vec2(0.0f,0.0f))
{

}

Sprite::Sprite(Texture *a_texture): m_texture(a_texture), m_rect(Rect(0, 0, 1, 1)), m_textureRect(0, 0, a_texture->GetWidth(), a_texture->GetHeight()), m_origin(glm::vec2(0.0f,0.0f))
{

}
Sprite::Sprite(Texture *a_texture, const Rect& a_textureRect, const glm::vec2& a_origin) :
        m_texture(a_texture),
        m_textureRect(a_textureRect),
        m_origin(a_origin)
{
    glm::vec2 pos = a_textureRect.GetPosition();
    glm::vec2 size = a_textureRect.GetSize();
    float width = a_texture->GetWidth();
    float height = a_texture->GetHeight();


    m_rect = Rect(pos.x / width, pos.y / height, size.x / width, size.y / height);
}
Sprite::~Sprite()
{

}