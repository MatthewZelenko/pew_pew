#include "Application.h"
#include "Clock.h"
#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include "Log.h"
#include "Input.h"
#include "Camera.h"

android_app* Application::m_androidState = nullptr;
Window Application::m_window;

Application::Application() :
        m_display(EGL_NO_DISPLAY), m_context(EGL_NO_CONTEXT), m_surface(EGL_NO_SURFACE),
        m_isApplicationReady(false), m_windowResumed(false), m_windowCreated(false), m_windowFocused(false),
        fixedElapse(0.0)
{

}

Application::~Application()
{
    Log::Error("APPLICATION DESTROYUED");
}

void Application::Run(android_app *a_app)
{
    Application::m_androidState = a_app;
    a_app->userData = this;
    a_app->onAppCmd = OnAppCmd;
    a_app->onInputEvent = OnInputEvent;
    int32_t events;
    android_poll_source *source;
    //Log::Info("Main Loop Started");
    while (true)
    {
        while (ALooper_pollAll(0, NULL, &events, (void **) &source) >= 0)
        {
            if (source != NULL)
            {
                source->process(m_androidState, source);
            }
            if (m_androidState->destroyRequested)
            {
                SystemDestroy();
                return;
            }
        }

        if (IsApplicationReady())
        {
            if(!(m_windowCreated && m_windowResumed))
            {
                SystemDestroy();
            }
            else if(m_windowFocused)
            {
                SystemUpdate();
                SystemFixedUpdate();
                SystemRender();
                eglSwapBuffers(m_display, m_surface);
                SystemLateUpdate();
            }
        }
        else
        {
            if(m_windowFocused && m_windowCreated && m_windowResumed)
            {
                SystemInit();
            }
        }
    }
}
bool Application::CreateDisplay()
{
    const EGLint attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_DEPTH_SIZE, 24,
            EGL_NONE
    };
    EGLint format = 0;
    EGLint numConfigs = 0;
    EGLConfig config = NULL;

    //Get the display and init with it
    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    if (!eglInitialize(m_display, NULL, NULL))
    {
        Log::Error("Could not initialize EGL");
        return false;
    }

    //Find the best configs that match the attribs
    eglChooseConfig(m_display, attribs, nullptr, 0, &numConfigs);
    if (!numConfigs)
    {
        Log::Error("Could not find appropriate configs");
        return false;
    }

    EGLConfig *supportedConfigs = new EGLConfig[numConfigs];

    //Get those configs
    eglChooseConfig(m_display, attribs, supportedConfigs, numConfigs, &numConfigs);

    if (numConfigs <= 0)
    {
        Log::Error("No supported configs");

        delete[] supportedConfigs;
        return false;
    }

    //Iterate through all the configs and find the matching one
    int i = 0;
    for (; i < numConfigs; ++i)
    {
        EGLConfig &cfg = supportedConfigs[i];
        EGLint r, g, b, d;
        if (eglGetConfigAttrib(m_display, cfg, EGL_RED_SIZE, &r) &&
            eglGetConfigAttrib(m_display, cfg, EGL_GREEN_SIZE, &g) &&
            eglGetConfigAttrib(m_display, cfg, EGL_BLUE_SIZE, &b) &&
            eglGetConfigAttrib(m_display, cfg, EGL_DEPTH_SIZE, &d) &&
            r == 8 && g == 8 && b == 8 && d == 24)
        {
            config = cfg;
            break;
        }
    }

    if (i == numConfigs)
    {
        config = supportedConfigs[0];
    }

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    eglGetConfigAttrib(m_display, config, EGL_NATIVE_VISUAL_ID, &format);
    if (ANativeWindow_setBuffersGeometry(m_androidState->window, 0, 0, format) != 0)
    {
        Log::Error("Could not set buffer geometry");
    }


    //create a new EGL window surface
    m_surface = eglCreateWindowSurface(m_display, config, m_androidState->window, NULL);
    if (!m_surface)
    {
        Log::Error("Could not create window surface");
        delete[] supportedConfigs;
        return false;
    }

    //create a new EGL rendering context
    const EGLint contextAttribs[] = {
            EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE
    };
    m_context = eglCreateContext(m_display, config, EGL_NO_CONTEXT, contextAttribs);
    if (!m_context)
    {
        Log::Error("Could not create context");
        delete[] supportedConfigs;
        return false;
    }
    if (eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_FALSE)
    {
        Log::Error("Could not make context current");
        delete[] supportedConfigs;
        return false;
    }

    //Query the width and height of the surface in pixels
    if(!eglQuerySurface(m_display, m_surface, EGL_WIDTH, &m_window.m_displayWidth) || !eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &m_window.m_displayHeight))
    {
        Log::Error("Could not get window sizes");
        delete[] supportedConfigs;
        return false;
    }

    delete[] supportedConfigs;

    //GL
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
    eglSwapInterval(m_display, 0);

    return true;
}
void Application::DestroyDisplay()
{
    glFlush();
    if (m_display != EGL_NO_DISPLAY)
    {
        eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (m_context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(m_display, m_context);
            m_context = EGL_NO_CONTEXT;
        }
        if (m_surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(m_display, m_surface);
            m_surface = EGL_NO_SURFACE;
        }
        eglTerminate(m_display);
        m_display = EGL_NO_DISPLAY;
    }
}
void Application::SystemInit()
{
    CreateDisplay();
    AConfiguration_setOrientation(m_androidState->config, ACONFIGURATION_ORIENTATION_LAND);
    Engine::Get();
    Engine::Get()->GetInput()->SetScreenSize(m_window.m_displayWidth, m_window.m_displayHeight);
    Load();
    m_isApplicationReady = true;
}
void Application::SystemDestroy()
{
    m_isApplicationReady = false;
    UnLoad();
    Camera::Destroy();
    Engine::TerminateEngine();
    DestroyDisplay();
}
void Application::SystemUpdate()
{
    double deltaTime = Engine::Get()->GetClock()->GetDeltaTime();
    Engine::Get()->Update(deltaTime);
    Update(deltaTime);
}
void Application::SystemFixedUpdate()
{
    double deltaTime = Engine::Get()->GetClock()->GetDeltaTime();
    fixedElapse += deltaTime;

    if(fixedElapse >= 1)
        fixedElapse = 1;

    double timeStep = Engine::Get()->GetClock()->GetTimeStep();
    while(fixedElapse >= timeStep)
    {
        Engine::Get()->FixedUpdate(timeStep);
        FixedUpdate(timeStep);
        fixedElapse -= timeStep;
    }
}
void Application::SystemLateUpdate()
{
    double deltaTime = Engine::Get()->GetClock()->GetDeltaTime();
    Engine::Get()->LateUpdate(deltaTime);
    LateUpdate(deltaTime);
}

void Application::SystemRender()
{
    Render();
    Engine::Get()->Render();
    PostRender();
}
void Application::OnAppCmd(android_app *a_app, int32_t a_cmd)
{
    Application* app = (Application *) a_app->userData;
    switch (a_cmd)
    {
        case APP_CMD_START:
        {
            Log::Debug("START");
            break;
        }
        case APP_CMD_RESUME:
        {
            Log::Debug("RESUME");
            app->m_windowResumed = true;
            break;
        }
        case APP_CMD_INIT_WINDOW:
        {
            app->m_windowCreated = true;
            Log::Debug("INIT WINDOW");
            break;
        }
        case APP_CMD_PAUSE:
        {
            app->m_windowResumed = false;
            Log::Debug("PAUSE");
            break;
        }
        case APP_CMD_STOP:
        {
            Log::Debug("STOP");
            break;
        }
        case APP_CMD_TERM_WINDOW:
        {
            app->m_windowCreated = false;
            Log::Debug("TERM WINDOW");
            break;
        }
        case APP_CMD_DESTROY:
        {
            Log::Debug("DESTROY");
            break;
        }
        case APP_CMD_GAINED_FOCUS:
        {
            Log::Debug("FOCUS");
            app->m_windowFocused = true;
            break;
        }
        case APP_CMD_LOST_FOCUS:
        {
            Log::Debug("LOST FOCUS");
            app->m_windowFocused = false;
            break;
        }
        default:
            break;
    }
}
int32_t Application::OnInputEvent(android_app *a_app, AInputEvent *a_event)
{
    Application* app = (Application *) a_app->userData;
    if(!app->IsApplicationReady())
        return 0;


    int32_t eventType = AInputEvent_getType(a_event);
    switch (eventType)
    {
        case AINPUT_EVENT_TYPE_MOTION:
        {
            switch (AInputEvent_getSource(a_event))
            {
                case AINPUT_SOURCE_TOUCHSCREEN:
                {
                    int action = AKeyEvent_getAction(a_event) & AMOTION_EVENT_ACTION_MASK;
                    switch (action)
                    {
                        case AMOTION_EVENT_ACTION_DOWN:
                        {
                            Engine::Get()->GetInput()->OnActionDown(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            return 1;
                        }
                        case AMOTION_EVENT_ACTION_UP:
                        {
                            Engine::Get()->GetInput()->OnActionUp(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            return 1;
                        }
                        case AMOTION_EVENT_ACTION_MOVE:
                        {
                            Engine::Get()->GetInput()->OnActionMove(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            return 1;
                        }
                        default:
                            break;
                    }
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    return 0;
}
void Application::Quit()
{
    ANativeActivity_finish(m_androidState->activity);
}

Window::Window():
    m_displayWidth(0), m_displayHeight(0)
{


}

//void Window::CalculateRatios()
//{
//    m_gameRatio = (float)m_gameWidth / (float)m_gameHeight;
//
////    if (m_gameRatio < m_displayRatio)
////    {
////        float newHeight = m_displayWidth / m_gameRatio;
////
////        float heightDiff = newHeight - m_displayHeight;
////        m_yDisplayExt = heightDiff * 0.5f;
////        glViewport(0, -m_yDisplayExt, m_displayWidth, m_displayHeight + heightDiff);
////
////        m_window.m_yGameExt = m_yDisplayExt / newHeight * (float)m_window.m_gameHeight;
////    }
////    else
////    {
////        float newWidth = m_window.m_displayHeight * ratio;
////
////        float widthDiff = newWidth - m_window.m_displayWidth;
////        m_xDisplayExt = widthDiff * 0.5f;
////        glViewport(-m_xDisplayExt, 0, m_window.m_displayWidth + widthDiff, m_window.m_displayHeight);
////
////        m_window.m_xGameExt = m_xDisplayExt / newWidth * (float)m_window.m_gameWidth;
////    }
//
//}