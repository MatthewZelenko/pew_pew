#ifndef PEW_PEW_RECT_H
#define PEW_PEW_RECT_H


#include <glm/vec2.hpp>

class Rect
{
public:
    Rect();
    Rect(const glm::vec2& a_pos, const glm::vec2& a_size);
    Rect(float a_x, float a_y, float a_w, float a_h);
    ~Rect();

    Rect& operator+=(const glm::vec2& a_rhs);

    void SetPosition(float a_x, float a_y);
    void SetPosition(const glm::vec2& a_position);
    void SetSize(float a_w, float a_h);
    void SetSize(const glm::vec2& a_size);


    float Top()const {return m_y + m_h;}
    float Bottom()const {return m_y;}
    float Right()const {return m_x + m_w;}
    float Left()const {return m_x;}

    glm::vec2 GetPosition()const {return glm::vec2(m_x, m_y);}
    glm::vec2 GetSize()const {return glm::vec2(m_w, m_h);}

    bool Contains(const glm::vec2& a_position);
    bool Intersects(const Rect& a_rect);

private:
    float m_x, m_y, m_w, m_h;
};


#endif //PEW_PEW_RECT_H