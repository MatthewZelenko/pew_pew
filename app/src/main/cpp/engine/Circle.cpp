#include <glm/geometric.hpp>
#include "Circle.h"


Circle::Circle() : m_x(0), m_y(0), m_radius(0)
{

}
Circle::Circle(float a_x, float a_y, float a_radius) : m_x(a_x), m_y(a_y), m_radius(a_radius)
{

}
Circle::~Circle()
{

}

void Circle::SetPosition(const glm::vec2 &a_position)
{
    m_x = a_position.x;
    m_y = a_position.y;
}
void Circle::SetPosition(float a_x, float a_y)
{
    m_x = a_x;
    m_y = a_y;
}
void Circle::SetRadius(float a_radius)
{
    m_radius = a_radius;
}

bool Circle::Contains(const glm::vec2 &a_position)
{
    return glm::pow(a_position.x - m_x, 2) + glm::pow(a_position.y - m_y, 2) < m_radius * m_radius;
}
bool Circle::Intersects(const Circle &a_circle)
{
    float radii = m_radius + a_circle.m_radius;
    return glm::pow(a_circle.m_x - m_x, 2) + glm::pow(a_circle.m_y - m_y, 2) < radii * radii;
}

Circle& Circle::operator+=(const glm::vec2 &a_rhs)
{
    m_x += a_rhs.x;
    m_y += a_rhs.y;
    return *this;
}