#include "ComponentManager.h"

ComponentManager::ComponentManager()
{

}
ComponentManager::~ComponentManager()
{
    Destroy();
}

void ComponentManager::Destroy()
{
    for (auto iter = m_components.begin(); iter != m_components.end(); ++iter)
    {
        for (int i = 0; i < iter->second.size(); ++i)
        {
            if(iter->second[i])
            {
                iter->second[i]->Unload();
                delete iter->second[i];
            }
        }
    }
    m_components.clear();
    m_freeComponents.clear();
    m_entityComponentMap.clear();
}

void ComponentManager::RemoveAllComponents(Handle a_entityHandle)
{
    if(m_entityComponentMap.find(a_entityHandle) == m_entityComponentMap.end())
        return;

    auto iter = m_entityComponentMap[a_entityHandle].begin();
    //Iterate all components and erase
    for (; iter != m_entityComponentMap[a_entityHandle].end() ; ++iter)
    {
        m_components[iter->first][iter->second]->Unload();
        delete m_components[iter->first][iter->second];
        m_components[iter->first][iter->second] = nullptr;
        m_freeComponents[iter->first].push_back(iter->second);
    }
    m_entityComponentMap.erase(a_entityHandle);
}
