#ifndef PEWPEW_HELPER_H
#define PEWPEW_HELPER_H
#include "Types.h"

class Helper
{
public:
    inline static TypeID CreateComponentTypeID() {static TypeID m_idCounter = 0; return m_idCounter++;}
    inline static TypeID CreateEntityTypeID() {static TypeID m_idCounter = 0; return m_idCounter++;}

};


#endif //PEWPEW_HELPER_H