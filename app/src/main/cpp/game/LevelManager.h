#ifndef PEW_PEW_LEVELMANAGER_H
#define PEW_PEW_LEVELMANAGER_H

#include <vector>
#include <game/enemies/Enemy.h>
#include <game/weapons/Weapon.h>
#include "Powerup.h"


struct LevelData
{
    LevelData(): m_startTime(0), m_spawnTimeMax(0.0f), m_spawnTimeMin(0.0f), m_spawnMax(0), m_powerupDropChance(0)
    {
        for (int i = 0; i < (int)Enemy::EnemyType::SIZE; ++i)
        {
            m_spawnChances[i] = 0;
        }
        for (int i = 0; i < (int)Powerup::PowerupType::SIZE; ++i)
        {
            m_powerupChances[i] = 0;
        }
    }

    int m_startTime;
    float m_spawnTimeMin, m_spawnTimeMax;
    int m_spawnMax;
    int m_spawnChances[(int)Enemy::EnemyType::SIZE];
    float m_powerupDropChance;
    int m_powerupChances[(int)Powerup::PowerupType::SIZE];
};

struct Level
{
    const LevelData* m_data;
    int m_currentLevel;

};

class LevelManager
{
public:
    LevelManager();
    ~LevelManager();

    void Load();
    void FixedUpdate(double a_timeStep);

    inline const LevelData& GetCurrentLevelData() { return m_levelData[m_currentLevel]; }

private:
    std::vector<LevelData> m_levelData;
    int m_currentLevel;

    float m_timeElapse, m_spawnElapse;
    float m_spawnTime;

    void GetSpawnTime();
};

#endif //PEW_PEW_LEVELMANAGER_H