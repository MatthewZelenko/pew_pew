#ifndef PEW_PEW_BG_H
#define PEW_PEW_BG_H

#include <Entity.h>
#include <string>
#include <external/glm/vec2.hpp>

class Game;

class BG : public Entity<BG>
{
public:
    BG();
    ~BG();

    void Load(const std::string& a_spriteName, const glm::vec2& a_size, float a_z);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

private:
};

#endif //PEW_PEW_BG_H