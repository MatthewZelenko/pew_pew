#include "Powerup.h"
#include "CollisionTags.h"
#include <components/TransformComponent.h>
#include <components/SpriteComponent.h>
#include <components/CircleColliderComponent.h>
#include <engine/EntityManager.h>
#include <game/scenes/GameScene.h>
#include <game/scenes/SceneManager.h>

Powerup::Powerup() : Entity(), m_elapse(0.0f)
{

}
Powerup::~Powerup()
{

}

void Powerup::Load(PowerupType a_type, const glm::vec2& a_pos, const std::string& a_name)
{
    m_powerupType = a_type;

    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(a_pos);
    transformComponent->SetZIndex(-50.0f);

    m_spriteComponent = AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->AddSprite(a_name);

    CircleColliderComponent* circle = AddComponent<CircleColliderComponent>();
    circle->SetCircle(Circle(0, 0, glm::min(m_spriteComponent->GetSprite().GetTextureRect().GetSize().x, m_spriteComponent->GetSprite().GetTextureRect().GetSize().y) * 0.5f));
    circle->SetTag(POWERUP);
    circle->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};


}
void Powerup::Unload()
{
}

void Powerup::Update(double a_deltaTime)
{
    m_elapse += a_deltaTime;
    if(m_elapse >= 10.0f)
    {
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
    }
}
void Powerup::FixedUpdate(double a_timeStep)
{
}


void Powerup::OnCollision(IEntity *a_entity, unsigned char a_tag)
{
    if(a_tag & PLAYER)
    {
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
        ((GameScene*)SceneManager::GetScene())->m_gameManager.UpdateScore(100.0f);
    }
}