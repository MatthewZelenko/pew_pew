#include "PauseButton.h"
#include <Camera.h>
#include "Input.h"
#include "PauseOverlay.h"
#include <components/TransformComponent.h>
#include <components/SpriteComponent.h>

PauseButton::PauseButton()
{

}
PauseButton::~PauseButton()
{

}

void PauseButton::Load()
{
    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(glm::vec2(16.0f, 16.0f));
    transformComponent->SetZIndex(-15.0f);

    SpriteComponent* spriteComponent = AddComponent<SpriteComponent>();
    spriteComponent->SetShader("simple");
    spriteComponent->SetSprite("settings_button");
    m_rect = Rect(transformComponent->GetPosition(), spriteComponent->GetSprite().GetTextureRect().GetSize());

    m_buttonComponent = AddComponent<ButtonComponent>(transformComponent, glm::vec2(), m_rect.GetSize());
    m_buttonComponent->OnPress = [this](){OnPress();};

    Engine::Get()->GetEventManager()->Subscribe("unpause", this);
}
void PauseButton::Unload()
{
    Engine::Get()->GetEventManager()->Unsubscribe("unpause", this);
}
void PauseButton::Update(double a_deltaTime)
{
}

void PauseButton::OnPress()
{
    Engine::Get()->GetEventManager()->Send("pause", nullptr);
    m_pauseOverlay = Engine::Get()->GetEntityManager()->CreateEntity<PauseOverlay>();
    m_buttonComponent->SetActive(false);
}
void PauseButton::OnEvent(const std::string &a_event, const void *a_data)
{
    //if(a_event == "unpause")
    m_buttonComponent->SetActive(true);
    Engine::Get()->GetEntityManager()->DestroyEntity(m_pauseOverlay->GetHandle());
}