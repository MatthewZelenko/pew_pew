#include <engine/components/SpriteComponent.h>
#include <game/scenes/SceneManager.h>
#include <game/scenes/MenuScene.h>
#include <game/scenes/GameScene.h>
#include <engine/Log.h>
#include "Button.h"

Button::Button()
{

}
Button::~Button()
{

}

void Button::Load(const glm::vec2& a_position, std::string a_name, std::function<void()> a_onPress)
{
    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(a_position);
    transformComponent->SetZIndex(-5.0f);

    SpriteComponent* spriteComponent = AddComponent<SpriteComponent>();
    spriteComponent->SetShader("simple");
    spriteComponent->SetSprite(a_name);
    glm::vec2 buttonPos = spriteComponent->GetSprite().GetOrigin() * spriteComponent->GetSprite().GetTextureRect().GetSize();
    m_rect = Rect(-buttonPos, spriteComponent->GetSprite().GetTextureRect().GetSize());

    m_buttonComponent = AddComponent<ButtonComponent>(transformComponent, m_rect.GetPosition(), m_rect.GetSize());
    m_buttonComponent->OnPress = a_onPress;
}
void Button::Unload()
{

}

void Button::Update(double a_deltaTime)
{

}
void Button::LateUpdate(double a_deltaTime)
{

}
void Button::FixedUpdate(double a_timeStep)
{

}