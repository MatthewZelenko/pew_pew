#ifndef PEWPEW_PLAYER_H
#define PEWPEW_PLAYER_H

#include <engine/Entity.h>
#include <external/glm/vec2.hpp>
#include <game/weapons/Weapon.h>
#include <EventManager.h>
#include <engine/components/SpriteComponent.h>
#include <engine/components/CircleColliderComponent.h>

class Player : public Entity<Player>, IEventListener
{
public:
    Player();
    ~Player();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void Move(float a_deltaTime, const glm::vec2& a_direction);
    void OnCollision(IEntity* a_entity, unsigned char a_tag);

    void TakeDamage();
    void ChangeSprite(bool a_idle);

    inline bool IsMoving() { return m_isMoving; }

    inline bool IsPaused() { return m_isPaused; }
private:
    CircleColliderComponent* m_circleColliderComponent;
    SpriteComponent* m_spriteComponent;

    bool m_shieldIsActive, m_isMoving, m_isPaused;

    float m_deathBlockElapse;
    unsigned int m_level;
    glm::vec2 m_heading;
    float m_rotation;
    float m_speed;

    std::vector<Weapon> m_weapons;

    void EnableLevel();
    void LevelUp();
    void LevelDown();
    void OnEvent(const std::string &a_event, const void *a_data) override;

    void ActivateShield(bool a_value = true);
};


#endif //PEWPEW_PLAYER_H