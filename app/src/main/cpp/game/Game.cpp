#include "Game.h"
#include "game/scenes/MenuScene.h"
#include <engine/Log.h>
#include <engine/Shader.h>
#include <engine/ResourceManager.h>
#include <engine/systems/SpriteRenderSystem.h>
#include <engine/Camera.h>

Game::Game()
{

}
Game::~Game()
{

}

void Game::Load()
{
    //TODO:: Have a way to load multiple entities for different scenes
    SceneManager::LoadScene(new MenuScene());
}
void Game::UnLoad()
{
    SceneManager::Destroy();
}
void Game::FixedUpdate(double a_timeStep)
{
    SceneManager::FixedUpdate(a_timeStep);
}
void Game::Update(double a_deltaTime)
{
    SceneManager::Update(a_deltaTime);
}
void Game::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    SceneManager::Render();
}
void Game::PostRender()
{
    SceneManager::PostRender();
}