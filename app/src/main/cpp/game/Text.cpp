#include "Text.h"
#include <components/TransformComponent.h>
#include <engine/Camera.h>

Text::Text()
{

}
Text::~Text()
{

}

void Text::Load(const std::string& a_fontName, const glm::vec2& a_position, FontComponent::Alignment a_alignment)
{
    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(a_position);
    transformComponent->SetScale(glm::vec2(120.0f, 120.0f));
    transformComponent->SetZIndex(-950.0f);

    m_fontComponent = AddComponent<FontComponent>();
    m_fontComponent->SetFont(a_fontName);
    m_fontComponent->SetSpacing(6.0f);
    m_fontComponent->SetShader("simple");
    m_fontComponent->SetAlignment(a_alignment);
    //m_fontComponent->SetUniform("u_colour", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
}
void Text::Unload()
{
}
void Text::Update(double a_deltaTime)
{
}
void Text::FixedUpdate(double a_timeStep)
{
}