#ifndef PEW_PEW_WEAPON_H
#define PEW_PEW_WEAPON_H


#include <string>
#include <glm/vec2.hpp>
#include <unordered_map>
#include <functional>
#include <vector>

class Bullet;

class Weapon
{
public:
    enum class WeaponType
    {
        PISTOL,
        SHOTGUN,
        MACHINEGUN,
        SIZE
    };

    enum class WeaponPosition
    {
        LEFT,
        CENTER,
        RIGHT
    };

    Weapon();
    Weapon(WeaponType a_weapon, const glm::vec2& a_localPosition, bool a_isActive);
    virtual ~Weapon();

    void Load();

    std::vector<Bullet*> ShootUpdate(float a_deltaTime, const glm::vec2& a_position, const glm::vec2& a_heading);
    std::vector<Bullet*> ShootPistol(const glm::vec2& a_position, const glm::vec2& a_heading);
    std::vector<Bullet*> ShootShotgun(const glm::vec2& a_position, const glm::vec2& a_heading);
    std::vector<Bullet*> ShootMachineGun(const glm::vec2& a_position, const glm::vec2& a_heading);


    void ChangeWeapon(WeaponType a_weapon);
    WeaponType CurrentWeapon() { return m_currentWeapon; }
    WeaponPosition CurrentPosition();
    bool IsActive() { return m_isActive; }
    void SetActive(bool a_active) { m_isActive = a_active; }

    glm::vec2 GetAddedPosition(const glm::vec2 &a_playerPosition, float a_rotation);

private:
    bool m_isActive;
    WeaponType m_currentWeapon;
    float m_reloadTime, m_reloadElapse;

    glm::vec2 m_localPosition;

    typedef std::function<std::vector<Bullet*>(const glm::vec2& a_position, const glm::vec2& a_heading)> ShootFunction;

    std::unordered_map<int, ShootFunction> m_shootFunctions;
};


#endif //PEW_PEW_WEAPON_H