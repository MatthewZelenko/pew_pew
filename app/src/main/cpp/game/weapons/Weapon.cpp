#include <engine/Log.h>
#include <engine/Utilities.h>
#include <external/glm/fwd.hpp>
#include "Weapon.h"
#include "Bullet.h"
#include <external/glm/gtx/rotate_vector.hpp>
#include "EntityManager.h"

Weapon::Weapon()
{

}
Weapon::Weapon(Weapon::WeaponType a_weapon, const glm::vec2& a_localPosition, bool a_isActive) :
        m_reloadTime(1.0f),
        m_reloadElapse(0.0f),
        m_isActive(a_isActive),
        m_localPosition(a_localPosition)
{
    ChangeWeapon(a_weapon);
}
Weapon::~Weapon()
{

}

void Weapon::Load()
{
    m_shootFunctions[(int)WeaponType::PISTOL] = [this](const glm::vec2& a_position, const glm::vec2& a_heading){ return ShootPistol(a_position, a_heading); };
    m_shootFunctions[(int)WeaponType::SHOTGUN] =  [this](const glm::vec2& a_position, const glm::vec2& a_heading){ return ShootShotgun(a_position, a_heading); };
    m_shootFunctions[(int)WeaponType::MACHINEGUN] =  [this](const glm::vec2& a_position, const glm::vec2& a_heading){ return ShootMachineGun(a_position, a_heading); };
}


std::vector<Bullet *> Weapon::ShootUpdate(float a_deltaTime, const glm::vec2 &a_position, const glm::vec2 &a_heading)
{
    if(m_reloadElapse < m_reloadTime)
    {
        m_reloadElapse += a_deltaTime;
    }
    else
    {
        m_reloadElapse = 0;
        return m_shootFunctions[(int)m_currentWeapon](a_position, a_heading);
    }
    return std::vector<Bullet *>();
}
std::vector<Bullet *> Weapon::ShootPistol(const glm::vec2 &a_position, const glm::vec2 &a_heading)
{
    glm::vec2 pos = GetAddedPosition(a_position, -atan2(a_heading.x, a_heading.y));
    int bulletDamage = 8;

    std::vector<Bullet*> arr;
    Bullet* bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, 1.5f, 250.0f, pos, glm::vec2(4.0f, 8.0f), a_heading);
    arr.push_back(bullet);
    return arr;
}
std::vector<Bullet *> Weapon::ShootShotgun(const glm::vec2 &a_position, const glm::vec2 &a_heading)
{
    float bulletSpeed1 = 200.0f + (FastRand() % 20) - 10;
    float bulletSpeed2 = 200.0f + (FastRand() % 20) - 10;
    float bulletLifespan1 = 0.5f + ((FastRand() % 50) * 0.01f) - 0.25f;
    float bulletLifespan2 = 0.5f + ((FastRand() % 50) * 0.01f) - 0.25f;
    int bulletDamage = 8;



    glm::vec2 pos = GetAddedPosition(a_position, -atan2(a_heading.x, a_heading.y));

    std::vector<Bullet*> arr;

    if(m_localPosition.x < 0)
    {
        Bullet* bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan1,
                                                                                 bulletSpeed1,
                                                                                 pos,
                                                                                 glm::vec2(5.0f, 10.0f),
                                                                                 glm::rotate(a_heading, -glm::radians((float)(FastRand() % 10 - 16))));
        arr.push_back(bullet);

        bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan2,
                                                                         bulletSpeed2,
                                                                         pos,
                                                                         glm::vec2(5.0f, 10.0f),
                                                                         glm::rotate(a_heading, -glm::radians((float)(FastRand() % 10 - 16))));
        arr.push_back(bullet);
    }
    else if(m_localPosition.x > 0)
    {
        Bullet* bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan1,
                                                                                 bulletSpeed1,
                                                                                 pos,
                                                                                 glm::vec2(5.0f, 10.0f),
                                                                                 glm::rotate(a_heading, glm::radians((float)(FastRand() % 10 - 16))));
        arr.push_back(bullet);

        bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan2,
                                                                         bulletSpeed2,
                                                                         pos,
                                                                         glm::vec2(5.0f, 10.0f),
                                                                         glm::rotate(a_heading, glm::radians((float)(FastRand() % 10 - 16))));
        arr.push_back(bullet);
    }
    else
    {
        Bullet* bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan1,
                                                                                 bulletSpeed1,
                                                                                 pos,
                                                                                 glm::vec2(5.0f, 10.0f),
                                                                                 glm::rotate(a_heading, glm::radians((float)(FastRand() % 10 - 5))));
        arr.push_back(bullet);

        bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan2,
                                                                         bulletSpeed2,
                                                                         pos,
                                                                         glm::vec2(5.0f, 10.0f),
                                                                         glm::rotate(a_heading, glm::radians((float)(FastRand() % 10 - 5))));
        arr.push_back(bullet);
    }

    return arr;
}
std::vector<Bullet *> Weapon::ShootMachineGun(const glm::vec2 &a_position, const glm::vec2 &a_heading)
{
    float bulletSpeed1 = 300.0f + (FastRand() % 50) - 25.0f;
    float bulletLifespan1 = 0.6f + ((FastRand() % 10) * 0.01f) - 0.005f;
    int bulletDamage = 4;

    glm::vec2 pos = GetAddedPosition(a_position, -atan2(a_heading.x, a_heading.y));

    std::vector<Bullet*> arr;

    if(m_localPosition.x < 0)
    {
        Bullet *bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan1, bulletSpeed1, pos,
                                                                                 glm::vec2(3.0f, 6.0f),
                                                                                 glm::rotate( a_heading, -glm::radians((float)(FastRand() % 60 - 50))));
        arr.push_back(bullet);
    }
    else if(m_localPosition.x > 0)
    {
        Bullet *bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan1, bulletSpeed1, pos,
                                                                                 glm::vec2(3.0f, 6.0f),
                                                                                 glm::rotate( a_heading, glm::radians((float)(FastRand() % 50 - 25))));
        arr.push_back(bullet);
    }
    else
    {
        Bullet *bullet = Engine::Get()->GetEntityManager()->CreateEntity<Bullet>(bulletDamage, bulletLifespan1, bulletSpeed1, pos,
                                                                                 glm::vec2(3.0f, 6.0f),
                                                                                 glm::rotate( a_heading, glm::radians((float)(FastRand() % 60 - 50))));
        arr.push_back(bullet);
    }
    return arr;
}

void Weapon::ChangeWeapon(Weapon::WeaponType a_weapon)
{
    switch(a_weapon)
    {
        case WeaponType::PISTOL:
        {
            m_reloadTime = 0.5f;
            break;
        }
        case WeaponType::SHOTGUN:
        {
            m_reloadTime = 1.25f;
            break;
        }
        case WeaponType::MACHINEGUN:
        {
            m_reloadTime = 0.1f;
            break;
        }
        default:
            m_reloadTime = 0.1f;
            break;
    }
    m_currentWeapon = a_weapon;
}

Weapon::WeaponPosition Weapon::CurrentPosition()
{
    if(m_localPosition.x < 0)
        return WeaponPosition ::LEFT;
    else if(m_localPosition.x > 0)
        return WeaponPosition ::RIGHT;
    return WeaponPosition ::CENTER;
}

glm::vec2 Weapon::GetAddedPosition(const glm::vec2 &a_playerPosition, float a_rotation)
{
    glm::vec2 pos = glm::rotate(m_localPosition, a_rotation);
    return a_playerPosition + pos;
}
