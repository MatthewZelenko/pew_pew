#include "Bullet.h"
#include <game/components/BulletComponent.h>
#include <engine/components/TransformComponent.h>
#include <engine/components/SpriteComponent.h>
#include <engine/Log.h>
#include <game/CollisionTags.h>
#include <engine/components/CircleColliderComponent.h>
#include "EntityManager.h"
Bullet::Bullet() : m_elapse(0.0f)
{

}
Bullet::~Bullet()
{

}

void Bullet::Load(int a_damage, float a_lifeTime, float m_speed, const glm::vec2 &a_position, const glm::vec2 &a_size, const glm::vec2 &a_heading)
{
    TransformComponent* trans = GetTransformComponent();
    trans->SetPosition(a_position);
    trans->SetZIndex(-500.0f);
    trans->SetRotationRadians(-atan2(a_heading.x, a_heading.y));

    SpriteComponent* sprite = AddComponent<SpriteComponent>();
    sprite->SetShader("simple");
    sprite->AddSprite("bullet");
    sprite->SetColour(glm::vec4(1.0f,0.0f,1.0f,1.0f));

    AddComponent<BulletComponent>(m_speed, a_heading);

    CircleColliderComponent* circle = AddComponent<CircleColliderComponent>();
    circle->SetCircle(Circle(0, 0, 1.5f));
    circle->SetTag(BULLET);
    circle->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};

    m_lifeTime = a_lifeTime;
    m_damage = a_damage;
}
void Bullet::Unload()
{
}

void Bullet::Update(double a_deltaTime)
{
    m_elapse += a_deltaTime;
    if(m_elapse >= m_lifeTime)
    {
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
    }
}
void Bullet::FixedUpdate(double a_timeStep)
{
}

void Bullet::OnCollision(IEntity *a_enttiy, unsigned char a_tag)
{
    if(a_tag & ENEMY)
    {
        //KillSelf
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
    }
}