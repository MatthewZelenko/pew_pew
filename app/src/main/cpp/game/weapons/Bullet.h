#ifndef PEW_PEW_BULLET_H
#define PEW_PEW_BULLET_H

#include <engine/Entity.h>
#include <external/glm/vec2.hpp>

class Bullet : public Entity<Bullet>
{
public:
    Bullet();
    ~Bullet();

    void Load(int a_damage, float a_lifeTime, float a_speed, const glm::vec2 &a_position, const glm::vec2 &a_size, const glm::vec2 &a_heading);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    inline int GetDamage() { return m_damage; }

private:
    int m_damage;
    float m_lifeTime, m_elapse;

    void OnCollision(IEntity *a_enttiy, unsigned char a_tag);

};


#endif //PEW_PEW_BULLET_H