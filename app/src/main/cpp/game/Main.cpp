#include <Game.h>
#include <memory>


void android_main(struct android_app* a_state)
{
    Game* game = new Game();
    game->Run(a_state);
    delete game;
}