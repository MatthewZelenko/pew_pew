#ifndef PEW_PEW_COLLISIONTAGS_H
#define PEW_PEW_COLLISIONTAGS_H

enum : unsigned char
{
    PLAYER  = 1 << 1,
    ENEMY   = 1 << 2,
    BULLET  = 1 << 3,
    POWERUP = 1 << 4
};

#endif //PEW_PEW_COLLISIONTAGS_H