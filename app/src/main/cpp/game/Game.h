#ifndef PEWPEW_GAME_H
#define PEWPEW_GAME_H

#include "game/scenes/SceneManager.h"
#include "engine/Application.h"

class Game : public Application
{
public:
    Game();
    ~Game();

protected:
    void Load() override;
    void UnLoad() override;

    void FixedUpdate(double a_timeStep) override;
    void Update(double a_deltaTime) override;
    void Render() override;
    void PostRender() override;

private:
};
#endif //PEWPEW_GAME_H