#include <Camera.h>
#include "CollisionGrid.h"


Grid::Grid(const glm::vec2 &a_position, const glm::vec2 &a_size, int a_cols, int a_rows) :
        m_size(a_size), m_position(a_position),
        m_rows(a_rows), m_cols(a_cols)
{
    m_tiles = new Tile*[m_rows];
    for (int y = 0; y < m_rows; ++y)
    {
        m_tiles[y] = new Tile[m_cols];
        for (int x = 0; x < m_cols; ++x)
        {
            m_tiles[y][x] = Tile(x, y);
        }
    }
}

Grid::~Grid()
{
    for (int y = 0; y < m_rows; ++y)
    {
        delete[] m_tiles[y];
    }
    delete[] m_tiles;
    m_tiles = nullptr;
}


CollisionGrid::CollisionGrid()
{
    m_grid = new Grid(glm::vec2(), Camera::MainCamera()->GetGameSize(), 16, 9);
}
CollisionGrid::~CollisionGrid()
{
    delete m_grid;
    m_grid = nullptr;
}


Tile::Tile() : m_x(0), m_y(0)
{

}
Tile::Tile(int a_x, int a_y):
    m_x(a_x), m_y(a_y)
{

}
Tile::~Tile()
{

}

void Tile::AddEntity(IEntity *a_entity)
{
    m_entities.push_back(a_entity);
}
