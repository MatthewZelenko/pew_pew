#include "GameManager.h"
#include "Powerup.h"
#include <Utilities.h>
#include <EntityManager.h>
#include <engine/Camera.h>
#include <android/asset_manager.h>
#include <engine/Application.h>
#include <external/rapidjson/document.h>
#include <engine/Log.h>
#include <engine/Utilities.h>
#include <external/rapidjson/stringbuffer.h>
#include <external/rapidjson/writer.h>

GameManager::GameManager() : m_score(0), m_updateToScore(0), m_levelUpThreshold(100), m_highScore(0)
{

}
GameManager::~GameManager()
{

}

void GameManager::Load()
{
    Engine::Get()->GetEventManager()->Subscribe("on_save", this);
    LoadHighScore();
    m_levelManager.Load();
}

void GameManager::UpdateScore(double a_score)
{
    m_updateToScore += a_score;
    if(m_updateToScore >= 1.0)
    {
        m_score += glm::floor(m_updateToScore);
        m_updateToScore -= glm::floor(m_updateToScore);
        m_scoreText->GetFontComponent()->SetText(std::to_string(m_score));
        if(m_score >= m_levelUpThreshold)
        {
            m_levelUpThreshold += 100;
            Engine::Get()->GetEventManager()->Send("level_up", nullptr);
        }
    }
}

void GameManager::EnemyDestroyed(Enemy::EnemyType a_type, const glm::vec2& a_pos, int a_multiplier)
{
    switch(a_type)
    {
        case Enemy::EnemyType::BASIC:
        {
            UpdateScore(10.0f * a_multiplier);
            break;
        }
        case Enemy::EnemyType::CHAIN:
        {
            UpdateScore(5.0f * a_multiplier);
            break;
        }
        case Enemy::EnemyType::MINE:
        {
            UpdateScore(50.0f * a_multiplier);
            break;
        }
        case Enemy::EnemyType::ZIP:
        {
            UpdateScore(75.0f * a_multiplier);
            break;
        }
        default:
            break;
    }
    SpawnPowerup(a_pos);
}
void GameManager::FixedUpdate(double a_timeStep)
{
    m_levelManager.FixedUpdate(a_timeStep);
    UpdateScore(a_timeStep);
}

void GameManager::SpawnPowerup(const glm::vec2& a_pos)
{
    const LevelData& levelData = m_levelManager.GetCurrentLevelData();

    if(((FastRand() % 100000) / 100000.0f) > levelData.m_powerupDropChance)
        return;

    int rng = FastRand() % 1000;

    int min = 1;
    int max = levelData.m_powerupChances[(int)Powerup::PowerupType::MACHINEGUN];

    if (rng >= min && rng < max)
    {
        Engine::Get()->GetEntityManager()->CreateEntity<Powerup>(Powerup::PowerupType::MACHINEGUN, a_pos, "powerup_machinegun");
    }
    else
    {
        min += levelData.m_powerupChances[(int)Powerup::PowerupType::MACHINEGUN];
        max += levelData.m_powerupChances[(int)Powerup::PowerupType::SHOTGUN];
        if (rng >= min && rng < max)
        {
            Engine::Get()->GetEntityManager()->CreateEntity<Powerup>(Powerup::PowerupType::SHOTGUN, a_pos, "powerup_shotgun");
        }
        else
        {
            min += levelData.m_powerupChances[(int)Powerup::PowerupType::SHOTGUN];
            max += levelData.m_powerupChances[(int)Powerup::PowerupType::SHIELD];
            if (rng >= min && rng < max)
            {
                Engine::Get()->GetEntityManager()->CreateEntity<Powerup>(Powerup::PowerupType::SHIELD, a_pos, "powerup_shield");
            }
        }
    }
}

void GameManager::SetPlayer(Player *a_player)
{
    m_player = a_player;
}

void GameManager::PlayerTookDamage()
{
    m_levelUpThreshold = m_score + 100000;
}

void GameManager::LoadHighScore()
{
    std::string localDataPath(Application::m_androidState->activity->internalDataPath);

    if(!DirectoryExists(localDataPath))
    {
        assert(CreateDirectory(localDataPath));
    }

    if(FileExists(localDataPath + "/" + SAVE_FILE))
    {
        long fileLen = 0;
        char* data = nullptr;

        //LOAD DATA
        FILE* f = fopen((localDataPath + "/" + SAVE_FILE).c_str(), "r");
        assert(f);

        fseek(f, 0, SEEK_END);
        if((fileLen = ftell(f)) == 0)
        {
            fclose(f);
            return;
        }
        rewind(f);

        data = new char[fileLen + 1];
        fread(data, fileLen, 1, f);
        data[fileLen] = 0;
        fclose(f);


        //Parse JSON
        rapidjson::Document doc;
        doc.Parse(data);
        if(doc.IsObject())
        {
            if (doc.HasMember("highscore"))
            {
                m_highScore = doc["highscore"].GetUint64();
            }
        }
        delete[] data;
    }
    else
    {
        //LOAD NOTHING
        m_highScore = 0;
    }
    m_highscoreText->GetFontComponent()->SetText(std::to_string(m_highScore));
}
void GameManager::SaveScore()
{
    if(m_score > m_highScore)
    {
        //Create File
        std::string localDataPath(Application::m_androidState->activity->internalDataPath);
        if(!DirectoryExists(localDataPath))
        {
            assert(CreateDirectory(localDataPath));
        }
        FILE* f = fopen((localDataPath + "/" + SAVE_FILE).c_str(), "w");
        assert(f);



        rapidjson::Document doc;
        doc.SetObject();
        rapidjson::Value v(m_score);
        doc.AddMember("highscore", v, doc.GetAllocator());


        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        doc.Accept(writer);

        fwrite(buffer.GetString(), buffer.GetSize(), 1, f);
        fclose(f);
        m_highScore = m_score;
        m_highscoreText->GetFontComponent()->SetText(std::to_string(m_highScore));
    }
}
void GameManager::OnEvent(const std::string &a_event, const void *a_data)
{
    if(a_event == "on_save")
        SaveScore();
}

void GameManager::Unload()
{
    Engine::Get()->GetEventManager()->Unsubscribe("on_save", this);
}