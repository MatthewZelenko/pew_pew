#include "TimedSprite.h"
#include <components/SpriteComponent.h>
#include <components/TransformComponent.h>
#include <Utilities.h>
#include <EntityManager.h>

TimedSprite::TimedSprite()
{

}
TimedSprite::~TimedSprite()
{

}

void TimedSprite::Load(const glm::vec2& a_position, const std::string& a_animationName)
{
    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(a_position);
    transformComponent->SetZIndex(-8.0f);
    transformComponent->SetRotationDegrees(FastRand() % 359);

    m_spriteComponent = AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->LoadAnimation(a_animationName, true);
}
void TimedSprite::Unload()
{

}
void TimedSprite::FixedUpdate(double a_timeStep)
{
    if(!m_spriteComponent->IsPlaying())
    {
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
    }
}