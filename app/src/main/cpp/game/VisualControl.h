#ifndef PEW_PEW_VISUALCONTROL_H
#define PEW_PEW_VISUALCONTROL_H

#include <Entity.h>
#include <glm/vec2.hpp>
#include <string>

class SpriteComponent;

class VisualControl : public Entity<VisualControl>
{
public:
    VisualControl();
    ~VisualControl();

    void Load(const std::string& a_spriteName, const glm::vec2& a_size);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void SetSpriteActive(bool a_val);
    bool IsSpriteActive();

private:
    SpriteComponent* m_spriteComponent;


};

#endif //PEW_PEW_VISUALCONTROL_H