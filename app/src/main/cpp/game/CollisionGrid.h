#ifndef PEW_PEW_COLLISIONGRID_H
#define PEW_PEW_COLLISIONGRID_H

#include <glm/vec2.hpp>
#include <IEntity.h>

class Tile
{
public:
    Tile();
    Tile(int a_x, int a_y);
    ~Tile();

    void AddEntity(IEntity* a_entity);

private:
    std::vector<IEntity*> m_entities;
    int m_x, m_y;

};

class Grid
{
public:
    Grid(const glm::vec2& a_position, const glm::vec2& a_size, int a_cols, int a_rows);
    ~Grid();



private:
    glm::vec2 m_position;
    glm::vec2 m_size;

    int m_rows, m_cols;

    Tile** m_tiles;
};

class CollisionGrid
{
public:
    CollisionGrid();
    ~CollisionGrid();



private:
    Grid* m_grid;

};

#endif //PEW_PEW_COLLISIONGRID_H