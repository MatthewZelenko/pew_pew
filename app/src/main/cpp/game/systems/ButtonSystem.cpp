#include <Engine.h>
#include "ButtonSystem.h"
#include "../components/ButtonComponent.h"
#include <ComponentManager.h>

ButtonSystem::ButtonSystem()
{

}
ButtonSystem::~ButtonSystem()
{

}
void ButtonSystem::Update(double a_deltaTime)
{
    auto iter = Engine::Get()->GetComponentManager()->Begin<ButtonComponent>();
    auto end = Engine::Get()->GetComponentManager()->End<ButtonComponent>();
    for (;iter != end; ++iter)
    {
        if(!(*iter))
            continue;

        (*iter)->Update(a_deltaTime);
    }
}