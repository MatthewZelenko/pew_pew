#include <components/TransformComponent.h>
#include <Camera.h>
#include <Log.h>
#include <engine/Application.h>
#include "BulletSystem.h"
#include "../components/BulletComponent.h"
#include "EntityManager.h"

BulletSystem::BulletSystem()
{

}
BulletSystem::~BulletSystem()
{

}

void BulletSystem::Load()
{

}
void BulletSystem::Unload()
{

}

void BulletSystem::Update(double a_deltaTime)
{
    auto iter = Engine::Get()->GetComponentManager()->Begin<BulletComponent>();
    auto end = Engine::Get()->GetComponentManager()->End<BulletComponent>();
    for (;iter != end; ++iter)
    {
        if(!(*iter))
            continue;

        (*iter)->Update(a_deltaTime);
        TransformComponent* trans = (*iter)->GetParent()->GetTransformComponent();
        glm::vec2 pos = trans->GetPosition();
        glm::vec2 screen = Camera::MainCamera()->GetGameSize();

        if(pos.x < 0 || pos.y > screen.x ||
           pos.y < 0 || pos.y > screen.y)
        {
            Engine::Get()->GetEntityManager()->DestroyEntity((*iter)->GetParentHandle());
        }
    }
}
void BulletSystem::LateUpdate(double a_deltaTime)
{

}
void BulletSystem::Render()
{

}