#include "EnemySystem.h"
#include <glm/common.hpp>
#include <glm/vec2.hpp>
#include <Camera.h>
#include <EntityManager.h>
#include <Engine.h>
#include <Player.h>
#include <engine/Log.h>
#include <components/FontComponent.h>
#include <game/Text.h>
#include <engine/Utilities.h>
#include <android/asset_manager.h>
#include <engine/Application.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <game/LevelManager.h>
#include "enemies/Enemy.h"
#include "components/BasicComponent.h"
#include "components/ZipComponent.h"
#include "components/MineComponent.h"
#include "components/ChainComponent.h"

EnemySystem::EnemySystem() : m_player(nullptr), m_spawnElapse(0.0f), m_cellWidth(50),
                             m_cellHeight(50), m_numOfCells(144), m_cols(16), m_rows(9)
{

}

EnemySystem::~EnemySystem()
{

}

void EnemySystem::Load()
{
    srand(time(NULL));

    m_startHeatmap = new int[m_numOfCells];
    for (int i = 0; i < m_numOfCells; ++i)
    {
        m_startHeatmap[i] = i;
    }
    Engine::Get()->GetEventManager()->Subscribe("spawn", this);
    Engine::Get()->GetEventManager()->Subscribe("pause", this);
    Engine::Get()->GetEventManager()->Subscribe("unpause", this);
}

void EnemySystem::Unload()
{
    Engine::Get()->GetEventManager()->Unsubscribe("unpause", this);
    Engine::Get()->GetEventManager()->Unsubscribe("pause", this);
    Engine::Get()->GetEventManager()->Unsubscribe("spawn", this);
    delete[] m_startHeatmap;
}

void EnemySystem::Update(double a_deltaTime)
{
    if(m_isPaused)
        return;


    std::vector<IComponent*> vec = Engine::Get()->GetComponentManager()->GetVector<BasicComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->Update(a_deltaTime);
    }

    vec = Engine::Get()->GetComponentManager()->GetVector<ZipComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->Update(a_deltaTime);
    }

    vec = Engine::Get()->GetComponentManager()->GetVector<MineComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->Update(a_deltaTime);
    }

    vec = Engine::Get()->GetComponentManager()->GetVector<ChainComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->Update(a_deltaTime);
    }
}
void EnemySystem::FixedUpdate(double a_timestep)
{
    if(m_isPaused)
        return;


    std::vector<IComponent*> vec = Engine::Get()->GetComponentManager()->GetVector<BasicComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->FixedUpdate(a_timestep);
    }

    vec = Engine::Get()->GetComponentManager()->GetVector<ZipComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->FixedUpdate(a_timestep);
    }

    vec = Engine::Get()->GetComponentManager()->GetVector<MineComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->FixedUpdate(a_timestep);
    }

    vec = Engine::Get()->GetComponentManager()->GetVector<ChainComponent>();
    for (int i = 0; i < vec.size(); ++i)
    {
        if(!vec[i])
            continue;

        vec[i]->FixedUpdate(a_timestep);
    }
}
void EnemySystem::Spawn(const LevelData *a_levelData)
{
    UpdateHeatmap();

    glm::vec2 pos = GetSpawnPoint();

    int random = FastRand() % 1000;
    int chance = a_levelData->m_spawnChances[0];


    if (random < chance)
    {
        Enemy* enemy = Engine::Get()->GetEntityManager()->CreateEntity<Enemy>(Enemy::EnemyType::BASIC, 20);
        enemy->SetTarget(m_player, false);

        IEnemyComponent* component =  enemy->AddComponent<BasicComponent>(enemy, pos);
        enemy->AddEnemyComponent(component);
    }
    else
    {
        chance += a_levelData->m_spawnChances[1];
        if (random < chance)
        {
            Enemy* enemy = Engine::Get()->GetEntityManager()->CreateEntity<Enemy>(Enemy::EnemyType::ZIP, 10);
            enemy->SetTarget(m_player, false);

            IEnemyComponent*  component =  enemy->AddComponent<ZipComponent>(enemy, pos);
            enemy->AddEnemyComponent(component);
        }
        else
        {
            chance += a_levelData->m_spawnChances[2];
            if (random < chance)
            {
                Enemy* enemy = Engine::Get()->GetEntityManager()->CreateEntity<Enemy>(Enemy::EnemyType::MINE, 50);
                enemy->SetTarget(m_player, false);

                IEnemyComponent* component = enemy->AddComponent<MineComponent>(enemy, pos);
                enemy->AddEnemyComponent(component);
            }
            else
            {
                chance += a_levelData->m_spawnChances[3];
                if (random < chance)
                {
                    int numberOfChains = FastRand() % 5 + 5;

                    Enemy* enemy = Engine::Get()->GetEntityManager()->CreateEntity<Enemy>(Enemy::EnemyType::CHAIN, 20);
                    ChainComponent* component = enemy->AddComponent<ChainComponent>(enemy, pos, m_player, m_player, false, 0);
                    enemy->AddEnemyComponent(component);

                    for (int i = 1; i < numberOfChains; ++i)
                    {
                        Enemy* newEnemy = Engine::Get()->GetEntityManager()->CreateEntity<Enemy>(Enemy::EnemyType::CHAIN, 20);

                        //Add to old enemy
                        component->AddFollower(newEnemy);

                        //Create new enemy compnent
                        component = newEnemy->AddComponent<ChainComponent>(enemy, pos, m_player, enemy, true, i);
                        newEnemy->AddEnemyComponent(component);
                        enemy = newEnemy;
                    }
                }
            }
        }
    }
}

void EnemySystem::UpdateHeatmap()
{
    ResetHeatmap();
    glm::vec2 pos = m_player->GetTransformComponent()->GetPosition();
    int xPos = floor(pos.x / m_cellWidth); //10
    int yPos = floor(pos.y / m_cellHeight); //5

    for (int y = 3; y >= -3; --y)
    {
        for (int x = 3; x >= -3; --x)
        {
            int yy = yPos + y;
            int xx = xPos + x;
            if (xx < 0 || xx >= m_cols || yy < 0 || yy >= m_rows)
                continue;

            int index = yy * m_cols + xx;
            m_heatmap.erase(m_heatmap.begin() + index);
        }
    }

}

void EnemySystem::ResetHeatmap()
{
    m_heatmap.clear();
    m_heatmap.insert(m_heatmap.begin(), m_startHeatmap, m_startHeatmap + m_numOfCells);
}

glm::vec2 EnemySystem::GetSpawnPoint()
{
    int index = FastRand() % m_heatmap.size();
    index = m_heatmap[index];
    int x = index % m_cols;
    int y = index / m_cols;

    glm::vec2 pos(x * m_cellWidth, y * m_cellHeight);

    float widthP = m_cellWidth * 0.1f;
    float heightP = m_cellHeight * 0.1f;


    pos.x += ((FastRand() % ((int) widthP * 10)) * 0.1f) +
             widthP; // (random between 10%) and(+) (90% of cellwidth)
    pos.y += ((FastRand() % ((int) heightP * 10)) * 0.1f) +
             heightP;// (random between 10) and(+) (90% of cellheight)
    return pos;
}

void EnemySystem::OnEvent(const std::string &a_event, const void *a_data)
{
    if (a_event == "spawn")
    {
        const LevelData* levelData = (const LevelData*) a_data;
        Spawn(levelData);
    }
    else if(a_event == "pause")
    {
        m_isPaused = true;
    }
    else if(a_event == "unpause")
    {
        m_isPaused = false;
    }
}