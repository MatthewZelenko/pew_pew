#include <components/BoxColliderComponent.h>
#include <Engine.h>
#include <components/TransformComponent.h>
#include <engine/components/CircleColliderComponent.h>
#include <external/glm/gtx/rotate_vector.hpp>
#include "CollisionSystem.h"
#include "ComponentManager.h"
#include "IEntity.h"
#include "CollisionTags.h"

CollisionSystem::CollisionSystem()
{

}
CollisionSystem::~CollisionSystem()
{

}

void CollisionSystem::Load()
{
}
void CollisionSystem::Unload()
{

}

void CollisionSystem::FixedUpdate(double a_timeStep)
{
    std::vector<IComponent*> vector = Engine::Get()->GetComponentManager()->GetVector<CircleColliderComponent>();
    size_t size = vector.size();

    //TODO: check tags first

    for (int i = 0; i < size; ++i)
    {
        if(!vector[i])
            continue;

        CircleColliderComponent* circleCompA = (CircleColliderComponent*)vector[i];
        TransformComponent* transA = vector[i]->GetParent()->GetTransformComponent();

        Circle circleA = circleCompA->GetCircle();
        circleA.SetPosition(glm::rotate(circleA.GetPosition(), transA->GetRotation()) + transA->GetPosition());
        for (int j = i + 1; j < size; ++j)
        {
            if(!vector[j])
                continue;

            CircleColliderComponent* circleCompB = (CircleColliderComponent*)vector[j];
            TransformComponent* transB = vector[j]->GetParent()->GetTransformComponent();

            Circle circleB = circleCompB->GetCircle();
            circleB.SetPosition(glm::rotate(circleB.GetPosition(), transB->GetRotation()) + transB->GetPosition());

            if(circleA.Intersects(circleB))
            {
                if(circleCompA->OnCollision)
                    circleCompA->OnCollision(vector[j]->GetParent(), circleCompB->GetTag());

                if(circleCompB->OnCollision)
                    circleCompB->OnCollision(vector[i]->GetParent(), circleCompA->GetTag());
            }
        }
    }
}