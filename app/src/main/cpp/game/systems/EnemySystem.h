#ifndef PEW_PEW_ENEMYSYSTEM_H
#define PEW_PEW_ENEMYSYSTEM_H


#include <systems/System.h>
#include <external/glm/vec2.hpp>
#include <vector>
#include <engine/EventManager.h>
#include <game/LevelManager.h>

class Player;

class EnemySystem : public System<EnemySystem>, IEventListener
{
public:
    EnemySystem();
    ~EnemySystem();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

private:
    void OnEvent(const std::string &a_event, const void *a_data) override;

public:

    void SetPlayer(Player* a_player) { m_player = a_player; }

private:
    bool m_isPaused;

    int m_cellWidth, m_cellHeight, m_numOfCells, m_cols, m_rows;

    int* m_startHeatmap;
    std::vector<int> m_heatmap;

    Player* m_player;
    float m_spawnElapse;


    void Spawn(const LevelData* a_levelData);
    void ResetHeatmap();
    glm::vec2 GetSpawnPoint();
    void UpdateHeatmap();
};


#endif //PEW_PEW_ENEMYSYSTEM_H