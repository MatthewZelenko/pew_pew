#include "PlayerControllerSystem.h"
#include <components/TransformComponent.h>
#include <Camera.h>
#include <Log.h>
#include <Application.h>
#include "EntityManager.h"
#include "../components/PlayerControllerComponent.h"
#include "Player.h"

PlayerControllerSystem::PlayerControllerSystem()
{

}

PlayerControllerSystem::~PlayerControllerSystem()
{

}

void PlayerControllerSystem::Load()
{

}
void PlayerControllerSystem::Unload()
{

}

void PlayerControllerSystem::Update(double a_deltaTime)
{
    auto iter = Engine::Get()->GetComponentManager()->Begin<PlayerControllerComponent>();
    auto end = Engine::Get()->GetComponentManager()->End<PlayerControllerComponent>();
    for (;iter != end; ++iter)
    {
        if(!(*iter))
            continue;

        (*iter)->Update(a_deltaTime);
    }
}
void PlayerControllerSystem::LateUpdate(double a_deltaTime)
{

}
void PlayerControllerSystem::Render()
{

}
