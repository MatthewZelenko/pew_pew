#ifndef PEW_PEW_BULLETSYSTEM_H
#define PEW_PEW_BULLETSYSTEM_H

#include "systems/System.h"

class BulletSystem : public System<BulletSystem>
{
public:
    BulletSystem();
    ~BulletSystem();

private:
    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void LateUpdate(double a_deltaTime) override;
    void Render() override;

private:


};

#endif //PEW_PEW_BULLETSYSTEM_H