#ifndef PEW_PEW_BUTTONSYSTEM_H
#define PEW_PEW_BUTTONSYSTEM_H


#include <systems/System.h>

class ButtonSystem : public System<ButtonSystem>
{
public:
    ButtonSystem();
    ~ButtonSystem();

    void Update(double a_deltaTime) override;


private:


};


#endif //PEW_PEW_BUTTONSYSTEM_H