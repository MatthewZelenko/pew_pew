#ifndef PEW_PEW_PLAYERCONTROLLERSYSTEM_H
#define PEW_PEW_PLAYERCONTROLLERSYSTEM_H

#include "systems/System.h"

class PlayerControllerSystem : public System<PlayerControllerSystem>
{
public:
    PlayerControllerSystem();
    ~PlayerControllerSystem();

private:
    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void LateUpdate(double a_deltaTime) override;
    void Render() override;

private:


};

#endif //PEW_PEW_PLAYERCONTROLLERSYSTEM_H
