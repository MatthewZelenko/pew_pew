#ifndef PEW_PEW_COLLISIONSYSTEM_H
#define PEW_PEW_COLLISIONSYSTEM_H

#include <vector>
#include <unordered_map>
#include "systems/System.h"

class CollisionSystem : public System<CollisionSystem>
{
public:
    CollisionSystem();
    ~CollisionSystem();

    void Load() override;
    void Unload() override;

    void FixedUpdate(double a_timeStep) override;

private:
};


#endif //PEW_PEW_COLLISIONSYSTEM_H