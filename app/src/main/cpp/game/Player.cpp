#include "Player.h"
#include "engine/components/SpriteComponent.h"
#include "engine/components/TransformComponent.h"
#include "engine/Camera.h"
#include "external/glm/gtc/quaternion.hpp"
#include "Input.h"
#include "glm/gtc/constants.hpp"
#include "CollisionTags.h"
#include "Powerup.h"
#include "TimedSprite.h"
#include <engine/Log.h>
#include <game/components/PlayerControllerComponent.h>
#include <engine/components/BoxColliderComponent.h>
#include <game/scenes/SceneManager.h>
#include <game/scenes/MenuScene.h>
#include <engine/components/CircleColliderComponent.h>
#include <game/scenes/GameScene.h>
#include <EventManager.h>
#include <EntityManager.h>

Player::Player() : m_speed(150.0f), m_level(1), m_rotation(0), m_deathBlockElapse(0.0f), m_shieldIsActive(false), m_isMoving(false), m_isPaused(false)
{
}
Player::~Player()
{
}

void Player::Load()
{
    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(Camera::MainCamera()->GetGameSize() * 0.5f);
    transformComponent->SetZIndex(-20.0f);
    m_heading = glm::vec2(0.0f, 1.0f);

    m_spriteComponent = AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_circleColliderComponent = AddComponent<CircleColliderComponent>();
    m_circleColliderComponent->SetTag(PLAYER);
    m_circleColliderComponent->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};
    ChangeSprite(true);


    PlayerControllerComponent* pcc = AddComponent<PlayerControllerComponent>();


    m_weapons.push_back(Weapon(Weapon::WeaponType::PISTOL, glm::vec2(-16, 0), false));
    m_weapons.push_back(Weapon(Weapon::WeaponType::PISTOL, glm::vec2(-5, 8), true));
    m_weapons.push_back(Weapon(Weapon::WeaponType::PISTOL, glm::vec2(0, 4), false));
    m_weapons.push_back(Weapon(Weapon::WeaponType::PISTOL, glm::vec2(5, 8), true));
    m_weapons.push_back(Weapon(Weapon::WeaponType::PISTOL, glm::vec2(16, 0), false));

    for (int i = 0; i < m_weapons.size(); ++i)
    {
        m_weapons[i].Load();
    }

    Engine::Get()->GetEventManager()->Subscribe("level_up", this);
    Engine::Get()->GetEventManager()->Subscribe("hit_player", this);
    Engine::Get()->GetEventManager()->Subscribe("pause", this);
    Engine::Get()->GetEventManager()->Subscribe("unpause", this);

}
void Player::Unload()
{
    Engine::Get()->GetEventManager()->Unsubscribe("unpause", this);
    Engine::Get()->GetEventManager()->Unsubscribe("pause", this);
    Engine::Get()->GetEventManager()->Unsubscribe("hit_player", this);
    Engine::Get()->GetEventManager()->Unsubscribe("level_up", this);
}

void Player::Update(double a_deltaTime)
{
    if(m_isPaused)
        return;

    if(m_deathBlockElapse > 0.0f)
        m_deathBlockElapse -= a_deltaTime;

    for (int i = 0; i < m_weapons.size(); ++i)
    {
        if(m_weapons[i].IsActive())
            m_weapons[i].ShootUpdate(a_deltaTime, GetTransformComponent()->GetPosition(), m_heading);
    }
}
void Player::FixedUpdate(double a_timeStep)
{
}

void Player::Move(float a_deltaTime, const glm::vec2& a_direction)
{
    TransformComponent* trans = GetTransformComponent();
    m_heading = a_direction;
    m_rotation = -glm::atan(m_heading.x, m_heading.y);
    trans->SetRotationRadians(m_rotation);

    glm::vec2 pos = trans->GetPosition() + m_heading * (m_speed * a_deltaTime);

    pos.x = glm::clamp(pos.x, 0.0f, Camera::MainCamera()->GetGameSize().x);
    pos.y = glm::clamp(pos.y, 0.0f, Camera::MainCamera()->GetGameSize().y);
    trans->SetPosition(pos);
}

void Player::OnCollision(IEntity *a_entity, unsigned char a_tag)
{
    if(a_tag & POWERUP)
    {
        Powerup::PowerupType type = ((Powerup *) a_entity)->GetType();
        for (int i = 0; i < m_weapons.size(); ++i)
        {
            switch (type)
            {
                case Powerup::PowerupType::MACHINEGUN:
                {
                    m_weapons[i].ChangeWeapon(Weapon::WeaponType::MACHINEGUN);
                    break;
                }
                case Powerup::PowerupType::SHOTGUN:
                {
                    m_weapons[i].ChangeWeapon(Weapon::WeaponType::SHOTGUN);
                    break;
                }
                case Powerup::PowerupType::SHIELD:
                {
                    ActivateShield();
                    break;
                }
                default:
                    break;
            }
        }
    }
}

void Player::LevelUp()
{
    m_level++;
    if(m_level > 3) m_level = 3;
    else EnableLevel();
    ChangeSprite(!m_isMoving);
}

void Player::LevelDown()
{
    m_level--;
    if(m_level < 1) m_level = 1;
    else EnableLevel();
    ChangeSprite(!m_isMoving);
}

void Player::EnableLevel()
{
    SpriteComponent* spriteComponent = GetComponent<SpriteComponent>();
    switch(m_level)
    {
        case 1:
        {
            m_weapons[0].SetActive(false);
            m_weapons[1].SetActive(true);
            m_weapons[2].SetActive(false);
            m_weapons[3].SetActive(true);
            m_weapons[4].SetActive(false);
            spriteComponent->SetSprite("player_1");
            break;
        }
        case 2:
        {
            m_weapons[0].SetActive(true);
            m_weapons[1].SetActive(true);
            m_weapons[2].SetActive(false);
            m_weapons[3].SetActive(true);
            m_weapons[4].SetActive(true);
            spriteComponent->SetSprite("player_2");
            break;
        }
        case 3:
        {
            m_weapons[0].SetActive(true);
            m_weapons[1].SetActive(true);
            m_weapons[2].SetActive(true);
            m_weapons[3].SetActive(true);
            m_weapons[4].SetActive(true);
            spriteComponent->SetSprite("player_3");
            break;
        }
        default:
            break;
    }
}

void Player::TakeDamage()
{
    if(m_deathBlockElapse > 0.0f)
        return;

    if(m_shieldIsActive)
    {
        ActivateShield(false);
        Engine::Get()->GetEntityManager()->CreateEntity<TimedSprite>(GetTransformComponent()->GetPosition(), "shield_hurt");
        return;
    }

    if(m_level == 1)
    {
        Engine::Get()->GetEventManager()->Send("player_died", nullptr);
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
        //TODO:Remove loadscene
        SceneManager::LoadScene(new MenuScene());
    }
    else
    {
        Engine::Get()->GetEventManager()->Send("player_damaged", nullptr);
        Engine::Get()->GetEntityManager()->CreateEntity<TimedSprite>(GetTransformComponent()->GetPosition(), "player_hurt");
    }
    LevelDown();
    m_deathBlockElapse = 2.0f;
}

void Player::OnEvent(const std::string &a_event, const void *a_data)
{
    if(a_event == "level_up")
        LevelUp();
    else if(a_event == "hit_player")
        TakeDamage();
    else if(a_event == "pause")
    {
        m_isPaused = true;
    }
    else if(a_event == "unpause")
    {
        m_isPaused = false;
    }
}

void Player::ActivateShield(bool a_value)
{
    m_shieldIsActive = a_value;
    if(a_value)
        GetComponent<SpriteComponent>()->SetColour(glm::vec4(0.25f, 0.5f, 0.85f, 1.0f));
    else
        GetComponent<SpriteComponent>()->SetColour(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
}

void Player::ChangeSprite(bool a_idle)
{
    std::string animationName = "player_";
    animationName += std::to_string(m_level);
    if(a_idle)
        animationName += "_idle";
    else
        animationName += "_thrust";

    m_spriteComponent->LoadAnimation(animationName, true, true);
    m_circleColliderComponent->SetCircle(Circle(0, 0, glm::min(m_spriteComponent->GetSprite().GetTextureRect().GetSize().x, m_spriteComponent->GetSprite().GetTextureRect().GetSize().y) * 0.5f));

    m_isMoving = !a_idle;
}