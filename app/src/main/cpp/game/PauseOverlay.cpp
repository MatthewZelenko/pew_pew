#include "PauseOverlay.h"
#include <Camera.h>
#include <components/TransformComponent.h>
#include <components/SpriteComponent.h>
#include <game/scenes/MenuScene.h>
#include <game/Button.h>
#include <game/scenes/SceneManager.h>
#include <engine/EventManager.h>

PauseOverlay::PauseOverlay()
{

}
PauseOverlay::~PauseOverlay()
{

}

void PauseOverlay::Load()
{
    glm::vec2 gameSize = Camera::MainCamera()->GetGameSize();

    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(glm::vec2(0.0f, 0.0f));
    transformComponent->SetScale(gameSize);
    transformComponent->SetZIndex(-10.0f);

    SpriteComponent* spriteComponent = AddComponent<SpriteComponent>();
    spriteComponent->SetShader("simple");
    spriteComponent->SetSprite("black_bg");
    spriteComponent->SetColour(glm::vec4(0.0f,0.0f,0.0f,0.5f));

    m_resumeButton = Engine::Get()->GetEntityManager()->CreateEntity<Button>(glm::vec2(gameSize.x * 0.5f, gameSize.y * 0.75f), "resume", [this](){
        Engine::Get()->GetEventManager()->Send("unpause", nullptr);
        Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
    });
    m_settingsButton = Engine::Get()->GetEntityManager()->CreateEntity<Button>(glm::vec2(gameSize.x * 0.5f, gameSize.y * 0.5f), "settings", [](){/*TODO::Open Settings*/});
    m_quitButton = Engine::Get()->GetEntityManager()->CreateEntity<Button>(glm::vec2(gameSize.x * 0.5f, gameSize.y * 0.25f), "quit", [](){SceneManager::LoadScene(new MenuScene());});
}
void PauseOverlay::Unload()
{
    Engine::Get()->GetEntityManager()->DestroyEntity(m_resumeButton->GetHandle());
    Engine::Get()->GetEntityManager()->DestroyEntity(m_quitButton->GetHandle());
    Engine::Get()->GetEntityManager()->DestroyEntity(m_settingsButton->GetHandle());
}
void PauseOverlay::Update(double a_deltaTime)
{

}