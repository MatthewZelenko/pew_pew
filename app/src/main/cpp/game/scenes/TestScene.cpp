#include <systems/SpriteRenderSystem.h>
#include <systems/TransformSystem.h>
#include "TestScene.h"
#include "Camera.h"
#include "Shader.h"
#include "ResourceManager.h"
#include "BG.h"
#include "Input.h"
#include "SceneManager.h"
#include <systems/FontRenderSystem.h>
#include "Game.h"
#include "../BG.h"
#include "../Text.h"
#include <systems/SystemManager.h>

TestScene::TestScene() : Scene("Test"),
                         m_beginBox(175, 81, 50, 21),
                         m_settingsBox(164, 57, 74, 21),
                         m_elapse(0.0f)
{

}
TestScene::~TestScene()
{

}

void TestScene::Load()
{
    Engine::Get()->GetSystemManager()->AddSystem<FontRenderSystem>(10000);
    Engine::Get()->GetSystemManager()->AddSystem<TransformSystem>(9000);

    m_camera = new Camera();
    m_camera->SetProjection(0.0f, 400.0f, 0.0f, 225.0f);
    Camera::SetMainCamera(m_camera);

    Shader* shader = Engine::Get()->GetResourceManager()->LoadShader("shader/", "simple.shader");
    shader->SetViewProjectionLocation("u_viewProjection");
    Engine::Get()->GetResourceManager()->LoadFont("fonts/", "font.png");

    Text* text = Engine::Get()->GetEntityManager()->CreateEntity<Text>("font", glm::vec2(0, 0), FontComponent::Alignment::RIGHT);
    text->GetFontComponent()->SetText("0123456789");
}
void TestScene::Unload()
{
    delete m_camera;
    m_camera = nullptr;
}
void TestScene::Update(double a_deltaTime)
{
}
void TestScene::FixedUpdate(double a_timeStep)
{
}
void TestScene::Render()
{
    //SpriteRenderSystem::ClearBuffer();
}