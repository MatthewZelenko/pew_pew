#ifndef PEW_PEW_TESTSCENE_H
#define PEW_PEW_TESTSCENE_H


#include <Rect.h>
#include <Camera.h>
#include "Scene.h"

class Game;

class TestScene : public Scene
{
public:
    TestScene();
    ~TestScene();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;
    void Render() override;

private:
    Rect m_beginBox, m_settingsBox;
    Camera* m_camera;
    float m_elapse;
};


#endif //PEW_PEW_TESTSCENE_H