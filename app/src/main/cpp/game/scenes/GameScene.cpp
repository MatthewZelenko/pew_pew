#include "GameScene.h"
#include "Camera.h"
#include "Shader.h"
#include "EntityManager.h"
#include "ResourceManager.h"
#include "Engine.h"
#include "../BG.h"
#include "../Player.h"
#include "systems/SystemManager.h"
#include <game/systems/BulletSystem.h>
#include <game/systems/PlayerControllerSystem.h>
#include <game/systems/EnemySystem.h>
#include <game/systems/CollisionSystem.h>
#include <engine/systems/SpriteRenderSystem.h>
#include <engine/systems/TransformSystem.h>
#include <string>
#include <engine/systems/FontRenderSystem.h>
#include <game/Text.h>
#include <engine/Clock.h>
#include <game/systems/ButtonSystem.h>

GameScene::GameScene() : Scene("Game")
{

}
GameScene::~GameScene()
{

}

void GameScene::Load()
{
    Engine::Get()->GetSystemManager()->AddSystem<SpriteRenderSystem>(10000);
    Engine::Get()->GetSystemManager()->AddSystem<FontRenderSystem>(9000);
    Engine::Get()->GetSystemManager()->AddSystem<TransformSystem>(9000);
    Engine::Get()->GetSystemManager()->AddSystem<BulletSystem>(100);
    Engine::Get()->GetSystemManager()->AddSystem<PlayerControllerSystem>(200);
    EnemySystem* enemySystem = Engine::Get()->GetSystemManager()->AddSystem<EnemySystem>(400);
    Engine::Get()->GetSystemManager()->AddSystem<CollisionSystem>(300);
    Engine::Get()->GetSystemManager()->AddSystem<ButtonSystem>(500);

    m_camera = new Camera();
    m_camera->SetProjection(0.0f, 800.0f, 0.0f, 450.0f);
    Camera::SetMainCamera(m_camera);

    Shader* shader = Engine::Get()->GetResourceManager()->LoadShader("shader/", "simple.shader");
    shader->SetViewProjectionLocation("u_viewProjection");

    Engine::Get()->GetResourceManager()->LoadFont("fonts/", "font.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/map/", "bg2.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/menu/", "black_bg.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/menu/", "pause_buttons.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/menu/", "settings_button.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/characters/", "player.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/characters/", "bullet.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/characters/enemies/", "basic.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/characters/enemies/", "chain.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/characters/enemies/", "mine.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/characters/enemies/", "zip.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/powerups/", "powerup.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/ui/", "visualControl.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/ui/", "visualJoystick.png");
    Engine::Get()->GetResourceManager()->LoadTexture("art/sfx/", "hurt.png");


    Engine::Get()->GetEntityManager()->CreateEntity<BG>("bg2", glm::vec2(800.0f, 450.0f), -999.0f);
    Player* player = Engine::Get()->GetEntityManager()->CreateEntity<Player>();

    enemySystem->SetPlayer(player);

    m_pauseButton = Engine::Get()->GetEntityManager()->CreateEntity<PauseButton>();

    Text* text = Engine::Get()->GetEntityManager()->CreateEntity<Text>("font", glm::vec2(20.0f, Camera::MainCamera()->GetGameSize().y - 40.0f));
    text->GetComponent<FontComponent>()->SetText("0");
    m_gameManager.SetScoreText(text);

    text = Engine::Get()->GetEntityManager()->CreateEntity<Text>("font", glm::vec2(Camera::MainCamera()->GetGameSize().x - 20.0f, Camera::MainCamera()->GetGameSize().y - 40.0f), FontComponent::Alignment::RIGHT);
    text->GetComponent<FontComponent>()->SetText("0");
    m_gameManager.SetHighscoreText(text);


    m_gameManager.SetPlayer(player);
    m_gameManager.Load();

    Engine::Get()->GetEventManager()->Subscribe("unpause", this);
    Engine::Get()->GetEventManager()->Subscribe("pause", this);
}
void GameScene::Unload()
{
    Engine::Get()->GetEventManager()->Unsubscribe("unpause", this);
    Engine::Get()->GetEventManager()->Unsubscribe("pause", this);

    Engine::Get()->GetEventManager()->Send("on_save", nullptr);
    m_gameManager.Unload();
    delete m_camera;
    m_camera = nullptr;
}

void GameScene::Update(double a_deltaTime)
{
    m_pauseButton->GetComponent<ButtonComponent>()->Update(a_deltaTime);
}
void GameScene::FixedUpdate(double a_timeStep)
{
    if(!m_isPaused) m_gameManager.FixedUpdate(a_timeStep);
}
void GameScene::PostRender()
{
}

void GameScene::OnEvent(const std::string &a_event, const void *a_data)
{
    m_isPaused = (a_event == "pause");
}

