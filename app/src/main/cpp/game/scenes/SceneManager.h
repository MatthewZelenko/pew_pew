#ifndef PEW_PEW_SCENEMANAGER_H
#define PEW_PEW_SCENEMANAGER_H

#include <vector>
#include <string>
#include <engine/Types.h>
#include <engine/Engine.h>

class Scene;
class SceneManager
{
public:

    static void Destroy();

    static void Update(double a_deltaTime);
    static void FixedUpdate(double a_timeStep);
    static void Render();
    static void PostRender();

    static void LoadScene(Scene *a_scene);

    static Scene* GetScene() { return m_currentScene; }

private:
    SceneManager();
    ~SceneManager();

    static Scene* m_currentScene;
    static Scene* m_newScene;

    static void _ProcessNewScene();
};


//Scene stack
//class SceneManager
//{
//public:
//    SceneManager();
//    ~SceneManager();
//
//    void Update(float a_deltaTime);
//    void Render();
//
//    int FindScene(const std::string& a_sceneName);
//
//    void PushScene(Scene* a_scene);
//    void PopScene();
//    void ReplaceScene(Scene* a_scene);
//    void RemoveScene(const std::string& a_sceneName);
//    void ClearScenes();
//    void ClearToScene(const std::string& a_sceneName);
//
//    Scene* GetScene();
//
//private:
//    int m_lastIndex;
//    std::vector<Scene*> m_scenes;
//    std::vector<std::string> m_sceneUpdates;
//    std::vector<Scene*> m_newScenes;
//
//    void ProcessSceneUpdates();
//
//    void _PushScene(Scene* a_scene);
//    void _PopScene();
//    void _ReplaceScene(Scene* a_scene);
//    void _RemoveScene(const std::string& a_sceneName);
//    void _ClearScenes();
//    void _ClearToScene(const std::string& a_sceneName);
//};


#endif //PEW_PEW_SCENEMANAGER_H