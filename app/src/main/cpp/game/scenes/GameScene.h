#ifndef PEW_PEW_GAMESCENE_H
#define PEW_PEW_GAMESCENE_H

#include <game/GameManager.h>
#include <game/LevelManager.h>
#include "PauseButton.h"
#include "scenes/Scene.h"
#include "Scene.h"

class Camera;

class GameScene : public Scene, IEventListener
{
public:
    GameScene();
    ~GameScene();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;
    void PostRender() override;

    GameManager m_gameManager;

    inline void Pause(bool a_pause) { m_isPaused = a_pause; }
    inline bool IsPaused() { return m_isPaused; }

private:
    void OnEvent(const std::string &a_event, const void *a_data) override;

private:
    Camera* m_camera;
    bool m_isPaused;
    PauseButton* m_pauseButton;
};

#endif //PEW_PEW_GAMESCENE_H