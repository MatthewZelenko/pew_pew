#ifndef PEW_PEW_MENUSCENE_H
#define PEW_PEW_MENUSCENE_H

#include "Scene.h"
#include <engine/Rect.h>

class Game;

class Camera;

class MenuScene : public Scene
{
public:
    MenuScene();
    ~MenuScene();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;
    void Render() override;

private:
    Rect m_beginBox, m_settingsBox;
    Camera* m_camera;
    float m_elapse;
};


#endif //PEW_PEW_MENUSCENE_H
