#ifndef PEW_PEW_SCENE_H
#define PEW_PEW_SCENE_H

#include <string>
#include <engine/Types.h>
#include <vector>
#include <engine/Engine.h>
#include "EntityManager.h"
#include "IEntity.h"

class Scene
{
public:
    Scene(const std::string& a_sceneName);
    virtual ~Scene();

    virtual void Load(){}
    virtual void Unload(){}
    virtual void Update(double a_deltaTime){}
    virtual void FixedUpdate(double a_timeStep){}
    virtual void Render(){}
    virtual void PostRender(){}

    //template <class T>
    //void CreateEntity();
    //void RemoveEntity(Handle a_entity);
    //void ClearEntities();

    const std::string GetName() const { return m_sceneName; }
    bool IsActive() { return m_isActive; }

private:
    friend class SceneManager;
    std::string m_sceneName;
    bool m_isActive;

    std::vector<Handle> m_entities;

};

//template <class T>
//void Scene::CreateEntity()
//{
//    m_entities.push_back(Engine::Get()->GetEntityManager()->CreateEntity<T>());
//}
//
//void Scene::RemoveEntity(Handle a_entity)
//{
//    Engine::Get()->GetEntityManager()->DestroyEntity(a_entity);
//
//    for (auto iter = m_entities.begin(); iter != m_entities.end(); ++iter)
//    {
//        if ((*iter) == a_entity)
//        {
//            m_entities.erase(iter);
//            return;
//        }
//    }
//}
//
//void Scene::ClearEntities()
//{
//    for (int i = 0; i < m_entities.size(); ++i)
//    {
//        Engine::Get()->GetEntityManager()->DestroyEntity(m_entities[i]);
//    }
//
//    m_entities.clear();
//}


#endif //PEW_PEW_SCENE_H