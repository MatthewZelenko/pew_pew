#include "MenuScene.h"
#include "Camera.h"
#include "Shader.h"
#include "ResourceManager.h"
#include "BG.h"
#include <game/scenes/GameScene.h>
#include <engine/Log.h>
#include <engine/systems/SpriteRenderSystem.h>
#include <engine/systems/TransformSystem.h>
#include "Input.h"
#include "SceneManager.h"
#include "Game.h"
#include <engine/systems/SystemManager.h>
#include <engine/systems/FontRenderSystem.h>
#include <engine/Audio/AudioManager.h>
#include <engine/Audio/MusicPlayer.h>
#include <engine/Audio/AudioPlayer.h>


MenuScene::MenuScene() : Scene("Menu"),
                                     m_beginBox(175, 81, 50, 21),
                                     m_settingsBox(164, 57, 74, 21),
                         m_elapse(0.0f)
{

}
MenuScene::~MenuScene()
{

}

void MenuScene::Load()
{
    Engine::Get()->GetSystemManager()->AddSystem<SpriteRenderSystem>(10000);
    Engine::Get()->GetSystemManager()->AddSystem<TransformSystem>(9000);

    m_camera = new Camera();
    m_camera->SetProjection(0.0f, 400.0f, 0.0f, 225.0f);
    Camera::SetMainCamera(m_camera);

    Shader* shader = Engine::Get()->GetResourceManager()->LoadShader("shader/", "simple.shader");
    shader->SetViewProjectionLocation("u_viewProjection");
    Engine::Get()->GetResourceManager()->LoadTexture("art/menu/", "bg.png");

    Engine::Get()->GetEntityManager()->CreateEntity<BG>("bg", glm::vec2(400.0f, 225.0f), 0.0f);

    //Engine::Get()->GetAudioManager()->PreloadAudio("audio/music/SFX_48.ogg");
    //Engine::Get()->GetAudioManager()->GetMusicPlayer("audio/music/SFX_48.ogg")->Play(true);

}
void MenuScene::Unload()
{
    Engine::Get()->GetAudioManager()->Clear();
    delete m_camera;
    m_camera = nullptr;
}
void MenuScene::Update(double a_deltaTime)
{
    if(m_elapse <= 1.0f)
    {
        m_elapse += a_deltaTime;
        //Engine::Get()->GetAudioManager()->AttachAudioToPlayer("audio/music/SFX_48.ogg")->Play();
    }
    else if(Engine::Get()->GetInput()->TouchReleased())
    {
        glm::vec2 pos = Camera::MainCamera()->DisplayToGame(Engine::Get()->GetInput()->GetCurrentPosition());
        if(m_beginBox.Contains(pos))
        {
            SceneManager::LoadScene(new GameScene());
        }
    }
}
void MenuScene::FixedUpdate(double a_timeStep)
{
}
void MenuScene::Render()
{
    //SpriteRenderSystem::ClearBuffer();
}