#include "SceneManager.h"
#include "scenes/Scene.h"
#include "EntityManager.h"
#include "ResourceManager.h"
#include <engine/systems/SystemManager.h>


Scene* SceneManager::m_currentScene = nullptr;
Scene* SceneManager::m_newScene = nullptr;

SceneManager::SceneManager()// : m_lastIndex(-1)
{

}
SceneManager::~SceneManager()
{
    Destroy();
}
void SceneManager::Destroy()
{
    if(m_currentScene)
    {
        m_currentScene->Unload();
        delete m_currentScene;
        m_currentScene = nullptr;
    }
    //_ClearScenes();
}


void SceneManager::Update(double a_deltaTime)
{
    if(m_currentScene)
        m_currentScene->Update(a_deltaTime);
}

void SceneManager::FixedUpdate(double a_timeStep)
{
    if(m_currentScene)
        m_currentScene->FixedUpdate(a_timeStep);
}

void SceneManager::Render()
{
    if(m_currentScene)
        m_currentScene->Render();
}

void SceneManager::PostRender()
{
    if(m_currentScene)
        m_currentScene->PostRender();
    _ProcessNewScene();
}

void SceneManager::_ProcessNewScene()
{
    if(!m_newScene)
        return;

    Engine::Get()->GetResourceManager()->Destroy();
    Engine::Get()->GetEntityManager()->Destroy();
    Engine::Get()->GetComponentManager()->Destroy();
    Engine::Get()->GetSystemManager()->Destroy();
    if(m_currentScene)
    {
        m_currentScene->Unload();
        delete m_currentScene;
        m_currentScene = nullptr;
    }
    m_currentScene = m_newScene;
    m_currentScene->Load();

    m_newScene = nullptr;
}

void SceneManager::LoadScene(Scene *a_scene)
{
    assert(a_scene);
    if (m_newScene)
    {
        delete m_newScene;
        m_newScene = nullptr;
    }
    m_newScene = a_scene;
}


//int SceneManager::FindScene(const std::string &a_sceneName)
//{
//    for (int i = 0; i < m_scenes.size(); ++i)
//    {
//        if(m_scenes[i]->GetName() == a_sceneName)
//        {
//            return i;
//        }
//    }
//    return -1;
//}
//
//void SceneManager::Update(float a_deltaTime)
//{
//    for (int i = 0; i < m_scenes.size(); ++i)
//    {
//        m_scenes[i]->Update(a_deltaTime);
//    }
//}
//void SceneManager::Render()
//{
//    for (int i = 0; i < m_scenes.size(); ++i)
//    {
//        m_scenes[i]->Render();
//    }
//    ProcessSceneUpdates();
//}
//void SceneManager::_PushScene(Scene *a_scene)
//{
//    if(m_lastIndex != -1)
//        m_scenes[m_lastIndex]->m_isActive = false;
//
//    m_scenes.push_back(a_scene);
//    a_scene->Load();
//    a_scene->m_isActive = true;
//
//    m_lastIndex++;
//}
//void SceneManager::_PopScene()
//{
//    if(m_lastIndex == -1)
//        return;
//
//    Scene* scene = m_scenes[m_lastIndex];
//    m_scenes.pop_back();
//    scene->Unload();
//    delete scene;
//
//    m_lastIndex--;
//    if(m_lastIndex != -1)
//    {
//        m_scenes[m_lastIndex]->m_isActive = true;
//    }
//}
//void SceneManager::_ReplaceScene(Scene *a_scene)
//{
//    _PopScene();
//    _PushScene(a_scene);
//}
//void SceneManager::_RemoveScene(const std::string &a_sceneName)
//{
//    int index = FindScene(a_sceneName);
//    if(index == -1)
//        return;
//
//    Scene* scene = m_scenes[index];
//    scene->Unload();
//    delete scene;
//    m_scenes.erase(m_scenes.begin() + index);
//
//    m_lastIndex--;
//    if(m_lastIndex != -1)
//        m_scenes[m_lastIndex]->m_isActive = true;
//}
//void SceneManager::_ClearScenes()
//{
//    for (int i = 0; i < m_scenes.size(); ++i)
//    {
//        Scene* scene = m_scenes[i];
//        scene->Unload();
//        delete scene;
//    }
//    m_scenes.clear();
//    m_lastIndex = -1;
//}
//void SceneManager::_ClearToScene(const std::string &a_sceneName)
//{
//    int index = FindScene(a_sceneName);
//    if(index == -1)
//        return;
//
//    for (size_t i = m_scenes.size() - 1; i > index; --i)
//    {
//        _PopScene();
//    }
//}
//
//
//
//void SceneManager::PushScene(Scene *a_scene)
//{
//    m_sceneUpdates.push_back("push");
//    m_newScenes.push_back(a_scene);
//}
//void SceneManager::PopScene()
//{
//    m_sceneUpdates.push_back("pop");
//}
//void SceneManager::ReplaceScene(Scene *a_scene)
//{
//    m_sceneUpdates.push_back("replace");
//    m_newScenes.push_back(a_scene);
//
//}
//void SceneManager::RemoveScene(const std::string &a_sceneName)
//{
//    m_sceneUpdates.push_back(std::string("remove") + a_sceneName);
//
//}
//void SceneManager::ClearScenes()
//{
//    m_sceneUpdates.push_back("clearscenes");
//
//}
//void SceneManager::ClearToScene(const std::string &a_sceneName)
//{
//    m_sceneUpdates.push_back(std::string("clearto") + a_sceneName);
//
//}
//void SceneManager::ProcessSceneUpdates()
//{
//    int newSceneCount = 0;
//    for (int i = 0; i < m_sceneUpdates.size(); ++i)
//    {
//        if(m_sceneUpdates[i] == "push")
//        {
//            _PushScene(m_newScenes[newSceneCount]);
//            newSceneCount++;
//        }
//        else if(m_sceneUpdates[i] == "pop")
//        {
//            _PopScene();
//        }
//        else if(m_sceneUpdates[i] == "replace")
//        {
//            _ReplaceScene(m_newScenes[newSceneCount]);
//            newSceneCount++;
//        }
//        else if(m_sceneUpdates[i] == "clearscenes")
//        {
//            _ClearScenes();
//        }
//        else
//        {
//            if(m_sceneUpdates[i].substr(0, 6) == "remove")
//            {
//                _RemoveScene(m_sceneUpdates[i].substr(6));
//            }
//            else if(m_sceneUpdates[i].substr(0, 7) == "clearto")
//            {
//                _ClearToScene(m_sceneUpdates[i].substr(7));
//            }
//        }
//    }
//
//    m_sceneUpdates.clear();
//    m_newScenes.clear();
//}
//Scene *SceneManager::GetScene()
//{
//    if(m_lastIndex != -1)
//        return m_scenes[m_lastIndex];
//    return nullptr;
//}
