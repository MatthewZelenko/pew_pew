#ifndef PEW_PEW_POWERUP_H
#define PEW_PEW_POWERUP_H


#include <Entity.h>
#include <external/glm/vec2.hpp>

class SpriteComponent;

class Powerup : public Entity<Powerup>
{
public:
    enum class PowerupType
    {
        SHOTGUN,
        MACHINEGUN,
        SHIELD,
        SIZE
    };

    Powerup();
    ~Powerup();

    void Load(PowerupType a_type, const glm::vec2& a_pos, const std::string& a_name);
    void Unload() override;
    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void OnCollision(IEntity* a_entity, unsigned char a_tag);

    PowerupType GetType(){return m_powerupType;}

private:
    PowerupType m_powerupType;
    SpriteComponent* m_spriteComponent;

    float m_elapse, m_flash;

};


#endif //PEW_PEW_POWERUP_H
