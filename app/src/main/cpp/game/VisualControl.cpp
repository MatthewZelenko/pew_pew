#include "VisualControl.h"
#include <components/TransformComponent.h>
#include <components/SpriteComponent.h>
#include <ComponentManager.h>

VisualControl::VisualControl()
{

}
VisualControl::~VisualControl()
{

}


void VisualControl::Load(const std::string &a_spriteName, const glm::vec2 &a_size)
{
    TransformComponent* trans = GetTransformComponent();
    trans->SetZIndex(0.0f);

    m_spriteComponent = AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->AddSprite(a_spriteName);
    m_spriteComponent->SetActive(false);
}
void VisualControl::Unload()
{
}

void VisualControl::Update(double a_deltaTime)
{
}
void VisualControl::FixedUpdate(double a_timeStep)
{
}

void VisualControl::SetSpriteActive(bool a_val)
{
    m_spriteComponent->SetActive(a_val);
}

bool VisualControl::IsSpriteActive()
{
    return m_spriteComponent->IsActive();
}
