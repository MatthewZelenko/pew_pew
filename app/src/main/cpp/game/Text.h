#ifndef PEW_PEW_TEXT_H
#define PEW_PEW_TEXT_H


#include <Entity.h>
#include <string>
#include <components/FontComponent.h>

class Text : public Entity<Text>
{
public:
    Text();
    ~Text();

    void Load(const std::string& a_fontName, const glm::vec2& a_position, FontComponent::Alignment a_alignment = FontComponent::Alignment::LEFT);
    void Unload() override;
    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    FontComponent* GetFontComponent(){return m_fontComponent;};

private:
    FontComponent* m_fontComponent;
};

#endif //PEW_PEW_TEXT_H