#include "BG.h"
#include <components/SpriteComponent.h>
#include <components/TransformComponent.h>

BG::BG()
{

}
BG::~BG()
{

}
void BG::Load(const std::string &a_spriteName, const glm::vec2 &a_size, float a_z)
{
    TransformComponent* transformComponent = GetTransformComponent();
    transformComponent->SetPosition(glm::vec2(0.0f, 0.0f));
    transformComponent->SetZIndex(a_z);
    transformComponent->SetScale(a_size);


    SpriteComponent* spriteComponent = AddComponent<SpriteComponent>();
    spriteComponent->SetShader("simple");
    spriteComponent->AddSprite(a_spriteName);
}
void BG::Unload()
{
}

void BG::Update(double a_deltaTime)
{
}
void BG::FixedUpdate(double a_timeStep)
{
}