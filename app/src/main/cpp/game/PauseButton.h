#ifndef PEW_PEW_PAUSEBUTTON_H
#define PEW_PEW_PAUSEBUTTON_H


#include <Entity.h>
#include <Rect.h>
#include <EventManager.h>
#include "components/ButtonComponent.h"
#include "PauseOverlay.h"


class PauseButton : public Entity<PauseButton>, public IEventListener
{
public:
    PauseButton();
    ~PauseButton();

    void Load() override;
    void Unload() override;
    void Update(double a_deltaTime) override;

    void OnPress();

private:
    void OnEvent(const std::string &a_event, const void *a_data) override;

private:
    Rect m_rect;
    ButtonComponent* m_buttonComponent;
    PauseOverlay* m_pauseOverlay;

};


#endif //PEW_PEW_PAUSEBUTTON_H