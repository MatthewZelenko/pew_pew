#ifndef PEW_PEW_GAMEMANAGER_H
#define PEW_PEW_GAMEMANAGER_H

#include <game/enemies/Enemy.h>
#include "Text.h"
#include "Player.h"
#include "LevelManager.h"

class GameManager : IEventListener
{
public:
    GameManager();
    ~GameManager();

    void Load();
    void Unload();
    void FixedUpdate(double a_timeStep);

    inline void SetScoreText(Text* a_scoreText) { m_scoreText = a_scoreText; }
    inline void SetHighscoreText(Text* a_highScoreText) { m_highscoreText = a_highScoreText; }
    void UpdateScore(double a_score);

    void EnemyDestroyed(Enemy::EnemyType a_type, const glm::vec2& a_pos, int a_multiplier);

    void SpawnPowerup(const glm::vec2& a_pos);
    void SetPlayer(Player* a_player);
    void PlayerTookDamage();


    void LoadHighScore();
    void SaveScore();
    void OnEvent(const std::string &a_event, const void *a_data) override;

private:
    const std::string SAVE_FILE = "evilsafe.dat";
    Player* m_player;
    double m_updateToScore;
    uint64_t m_score, m_highScore, m_levelUpThreshold;

    Text* m_scoreText, *m_highscoreText;

    LevelManager m_levelManager;
};


#endif //PEW_PEW_GAMEMANAGER_H