#ifndef PEW_PEW_PAUSEOVERLAY_H
#define PEW_PEW_PAUSEOVERLAY_H


#include <Entity.h>
#include "Button.h"

class PauseOverlay : public Entity<PauseOverlay>
{
public:
    PauseOverlay();
    ~PauseOverlay();

    void Load() override;
    void Unload() override;
    void Update(double a_deltaTime) override;

private:
    Button* m_resumeButton, *m_settingsButton, *m_quitButton;

};


#endif //PEW_PEW_PAUSEOVERLAY_H