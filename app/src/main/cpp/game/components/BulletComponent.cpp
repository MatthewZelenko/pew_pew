#include "BulletComponent.h"
#include "IEntity.h"
#include "EntityManager.h"
#include <engine/components/TransformComponent.h>
#include <game/weapons/Bullet.h>
#include <engine/Camera.h>
#include <engine/Log.h>

BulletComponent::BulletComponent()
{

}
BulletComponent::~BulletComponent()
{

}

void BulletComponent::Load(float a_speed, const glm::vec2 &a_heading)
{
    m_speed = a_speed;
    m_heading = a_heading;

}
void BulletComponent::Unload()
{
}

void BulletComponent::Update(double a_deltaTime)
{
    TransformComponent* trans = GetParent()->GetTransformComponent();
    trans->AddPosition(m_heading * (m_speed * (float)a_deltaTime));
}
void BulletComponent::FixedUpdate(double a_timeStep)
{
}