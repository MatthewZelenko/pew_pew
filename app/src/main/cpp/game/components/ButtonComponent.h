#ifndef PEW_PEW_BUTTONCOMPONENT_H
#define PEW_PEW_BUTTONCOMPONENT_H

#include <components/Component.h>
#include <glm/vec2.hpp>
#include <components/TransformComponent.h>
#include <Rect.h>
#include "functional"

class ButtonComponent : public Component<ButtonComponent>
{
public:
    ButtonComponent();
    ~ButtonComponent();

    void Load(TransformComponent* a_parentTransform, const glm::vec2& a_position, const glm::vec2& a_size);
    void Unload() override;
    void Update(double a_deltaTime) override;

    bool IsActive() { return m_isActive; }
    void SetActive(bool a_active) { m_isActive = a_active; }

    void SetSize(const glm::vec2& a_size) { m_size = a_size; }

    std::function<void()> OnPress, OnRelease;

private:
    bool m_isActive;
    TransformComponent* m_parentTransform;
    glm::vec2 m_position, m_size;
};


#endif //PEW_PEW_BUTTONCOMPONENT_H