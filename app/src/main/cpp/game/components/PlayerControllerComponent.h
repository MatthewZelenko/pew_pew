#ifndef PEW_PEW_PLAYERCONTROLLERCOMPONENT_H
#define PEW_PEW_PLAYERCONTROLLERCOMPONENT_H

#include <engine/IEntity.h>
#include <components/Component.h>
#include <external/glm/vec2.hpp>

class VisualControl;

class PlayerControllerComponent : public Component<PlayerControllerComponent>
{
public:
    PlayerControllerComponent();
    ~PlayerControllerComponent();

    void Load() override;
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

private:
    VisualControl* a_visualControl, *a_visualJoystick;

    void Move(float a_deltaTime, const glm::vec2& a_direction);

};

#endif //PEW_PEW_PLAYERCONTROLLERCOMPONENT_H