#include "ChainComponent.h"
#include <components/SpriteComponent.h>
#include <components/CircleColliderComponent.h>
#include <Utilities.h>
#include <Engine.h>
#include "CollisionTags.h"
#include "Player.h"
#include "weapons/Bullet.h"
#include <EventManager.h>
#include <external/glm/gtx/rotate_vector.hpp>
#include <game/TimedSprite.h>
#include <EntityManager.h>
#include "Player.h"

ChainComponent::ChainComponent() :
        m_player(nullptr),
        m_speed(50.0f),
        m_follower(nullptr),
        m_state(0)
{

}

ChainComponent::~ChainComponent()
{

}

void ChainComponent::Load(Enemy* a_parent, const glm::vec2 &a_pos, Player* a_player, IEntity* a_target, bool a_isEnemy, int a_currentIndex)
{
    m_parent = a_parent;
    m_parent->SetTarget(a_target, a_isEnemy);
    m_player = a_player;

    m_transformComponent = m_parent->GetTransformComponent();
    m_transformComponent->SetPosition(a_pos);
    m_transformComponent->SetZIndex(-200.0f + a_currentIndex * 2.5f);

    m_spriteComponent = m_parent->AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->LoadAnimation("chain_spawn", true);

    CircleColliderComponent* circle = m_parent->AddComponent<CircleColliderComponent>();
    circle->SetCircle(Circle(0, 0, 8.5f));
    circle->SetTag(ENEMY);
    circle->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};

    m_heading = glm::normalize(glm::vec2(FastRand() % 200 - 100, FastRand() % 200 - 100));
    m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));

    //TODO:: Create in enemysystem class
    /*
    if(a_currentIndex < a_numberInChain)
    {
        Chain* enemy = Engine::Get()->GetEntityManager()->CreateEntity<Chain>(a_pos, m_player, this, true, 7, a_currentIndex + 1);
        m_follower = enemy;
    }
     */

}

void ChainComponent::Unload()
{
}

void ChainComponent::Update(double a_deltaTime)
{
    if(m_state == 0)
    {
        if(!m_spriteComponent->IsPlaying())
        {
            m_state = 1;
            m_spriteComponent->LoadAnimation("chain_idle", true, true);
        }
    }
    else if(m_state == 1)
    {
        if (m_parent->IsFollowingEnemy())
        {
            UpdateFollowingEnemy(a_deltaTime);
        } else
        {
            UpdateFollowingPlayer(a_deltaTime);
        }
    }
    else if(m_state == 2)
    {
        if(!m_spriteComponent->IsPlaying())
        {
            m_parent->TakeDamage(100);
        }
    }
}

void ChainComponent::FixedUpdate(double a_timeStep)
{
}

void ChainComponent::OnCollision(IEntity *a_entity, unsigned char a_tag)
{if(m_state == 2)
        return;

    if(a_tag & BULLET)
    {
        Bullet* bullet = dynamic_cast<Bullet*>(a_entity);
        bool dead = m_parent->TakeDamage(bullet->GetDamage(), false);
        if(dead)
        {
            if (m_parent->IsFollowingEnemy())
            {
                m_parent->GetTarget()->GetComponent<ChainComponent>()->m_follower = nullptr;
            }
            if(m_follower)
            {
                m_follower->SetTarget(m_player, false);
                m_follower->AddMultipler(1);
            }
            m_spriteComponent->LoadAnimation("chain_death", true);
            m_state = 2;
        }
    }
    else if(a_tag & PLAYER)
    {
        bool dead = m_parent->TakeDamage(1000, false);
        if(dead)
        {
            if (m_parent->IsFollowingEnemy())
            {
                m_parent->GetTarget()->GetComponent<ChainComponent>()->m_follower = nullptr;
            }
            if(m_follower)
            {
                m_follower->SetTarget(m_player, false);
                m_follower->AddMultipler(1);
            }

            m_parent->GetComponent<SpriteComponent>()->LoadAnimation("chain_death", true);
            m_state = 2;
            Enemy::EnemyType type = m_parent->GetType();
            Engine::Get()->GetEventManager()->Send("hit_player", (void *) &type);
        }
    }
}

void ChainComponent::UpdateFollowingPlayer(float a_deltaTime)
{
    Player* player = (Player*)m_parent->GetTarget();
    assert(player && "BasicEnemy: Player target missing!");

    glm::vec2 targetPosition = player->GetTransformComponent()->GetPosition();
    glm::vec2 diff = targetPosition - m_transformComponent->GetPosition();
    float distanceSqr = (diff.x * diff.x) + (diff.y * diff.y);
    glm::vec2 heading = glm::normalize(diff);
    float dotProduct = glm::dot(m_heading, heading);

    int direction = 0;

    if (dotProduct != 1)
    {
        if(dotProduct >= 0.9999f)
        {
            m_heading = heading;
            m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));
        }
        else if (dotProduct == -1) // <- ->
        {
            direction = FastRand() % 3 - 1;
        }
        else
        {
            glm::vec3 cross = glm::cross(glm::vec3(m_heading.x, m_heading.y, 0.0f), glm::vec3(heading.x, heading.y, 0.0f));

            if (cross.z < 0) //Pointing away
            {
                direction = -1;
                //reverse
            }
            else
            {
                direction = 1;
            }
        }
    }

    if(direction != 0)
    {
        float rotateValue = (0.75f * glm::pi<float>()) * a_deltaTime * direction;
        m_heading = glm::rotate(m_heading, rotateValue);
        m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));
    }
    m_transformComponent->AddPosition(m_heading * m_speed * a_deltaTime);
}
void ChainComponent::UpdateFollowingEnemy(float a_deltaTime)
{
    ChainComponent* chain = m_parent->GetTarget()->GetComponent<ChainComponent>();
    assert(chain && "BasicEnemy: Player target missing!");

    glm::vec2 targetPosition = chain->m_transformComponent->GetPosition();

    glm::vec2 diff = targetPosition - m_transformComponent->GetPosition();
    if(diff.x == 0 && diff.y == 0)
        return;
    m_heading = glm::normalize(diff);
    targetPosition = targetPosition - m_heading * 8.0f;
    diff = targetPosition - m_transformComponent->GetPosition();

    //ROTATE TOWARDS ENEMY
    m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));


    float distanceSqr = (diff.x * diff.x) + (diff.y * diff.y);

    float speed = m_speed * a_deltaTime;

    if(speed * speed <= distanceSqr)
    {
        m_transformComponent->SetPosition(targetPosition);
    }
    else
    {
        m_transformComponent->AddPosition(m_heading * speed);
    }
}

void ChainComponent::AddMultipler(int a_value)
{
    m_parent->AddMultipler(a_value);
    if (m_follower)
    {
        m_follower->AddMultipler(a_value);
    }
}

void ChainComponent::OnDestroy()
{
    Engine::Get()->GetEntityManager()->CreateEntity<TimedSprite>(m_transformComponent->GetPosition(), "chain_hurt");
}

void ChainComponent::AddFollower(Enemy *a_follower)
{
    m_follower = a_follower;
}