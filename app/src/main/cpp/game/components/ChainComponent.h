#ifndef PEW_PEW_CHAINCOMPONENT_H
#define PEW_PEW_CHAINCOMPONENT_H

#include <components/Component.h>
#include <components/SpriteComponent.h>
#include <components/TransformComponent.h>
#include "IEnemyComponent.h"

class Player;

class ChainComponent : public Component<ChainComponent>, public IEnemyComponent
{
public:
    ChainComponent();
    ~ChainComponent();

    void Load(Enemy* a_parent, const glm::vec2 &a_pos, Player* a_player, IEntity* a_target, bool a_isEnemy, int a_currentIndex);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void AddFollower(Enemy* a_follower);

    void AddMultipler(int a_value);

    void OnDestroy() override;

private:
    TransformComponent* m_transformComponent;
    SpriteComponent* m_spriteComponent;

    int m_state;
    Player* m_player;
    Enemy* m_follower;

    float m_speed;
    glm::vec2 m_heading;

    void OnCollision(IEntity *a_entity, unsigned char a_tag);
    void UpdateFollowingPlayer(float a_deltaTime);
    void UpdateFollowingEnemy(float a_deltaTime);


};


#endif //PEW_PEW_CHAINCOMPONENT_H