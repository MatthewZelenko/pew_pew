#ifndef PEW_PEW_MINECOMPONENT_H
#define PEW_PEW_MINECOMPONENT_H


#include <components/Component.h>
#include <components/SpriteComponent.h>
#include <components/TransformComponent.h>
#include "IEnemyComponent.h"

class MineComponent : public Component<MineComponent>, public IEnemyComponent
{
public:
    MineComponent();
    ~MineComponent();

    void Load(Enemy* a_parent, const glm::vec2 &a_pos);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void OnDestroy() override;


private:
    TransformComponent* m_transformComponent;
    SpriteComponent* m_spriteComponent;

    float m_buildElapse;
    int m_state;

    void OnCollision(IEntity *a_entity, unsigned char a_tag);


};


#endif //PEW_PEW_MINECOMPONENT_H