#include <components/SpriteComponent.h>
#include <components/CircleColliderComponent.h>
#include <Utilities.h>
#include <Engine.h>
#include "MineComponent.h"
#include "CollisionTags.h"
#include "Player.h"
#include "weapons/Bullet.h"
#include <EventManager.h>
#include <game/TimedSprite.h>
#include <EntityManager.h>

MineComponent::MineComponent() : m_buildElapse(0.0f), m_state(0)
{

}

MineComponent::~MineComponent()
{

}

void MineComponent::Load(Enemy* a_parent, const glm::vec2 &a_pos)
{
    m_parent = a_parent;

    m_transformComponent = m_parent->GetTransformComponent();
    m_transformComponent->SetPosition(a_pos);
    m_transformComponent->SetZIndex(-250.0f);

    m_spriteComponent = m_parent->AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->LoadAnimation("mine_spawn", true);

    CircleColliderComponent* circle = m_parent->AddComponent<CircleColliderComponent>();
    circle->SetCircle(Circle(0, 0, 16.0f));
    circle->SetTag(ENEMY);
    circle->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};

    glm::vec2 heading = glm::normalize(glm::vec2(FastRand() % 200 - 100, FastRand() % 200 - 100));
    m_transformComponent->SetRotationRadians(-atan2(heading.x, heading.y));
}

void MineComponent::Unload()
{
}

void MineComponent::Update(double a_deltaTime)
{
    if(m_state == 0) //spawn
    {
        if(!m_spriteComponent->IsPlaying())
        {
            m_spriteComponent->LoadAnimation("mine_idle", true, true);
            m_state = 1;
        }
    }
    else if(m_state == 1) //idle
    {
        Player *player = (Player *) m_parent->GetTarget();
        glm::vec2 pos = m_transformComponent->GetPosition() - m_transformComponent->GetPosition();
        float lengthSquared = (pos.x * pos.x) + (pos.y * pos.y);
        if (lengthSquared < 84.0f * 84.0f)
        {
            m_spriteComponent->LoadAnimation("mine_timer", true, true);
            m_state = 2;
        }
    }
    else if(m_state == 2) //timer
    {
        m_buildElapse += a_deltaTime;
        if(m_buildElapse >= 2.5f)
        {
            m_spriteComponent->LoadAnimation("mine_death", true);
            m_state = 3;
        }
    }
    else if(m_state == 3) //die
    {
        if(!m_spriteComponent->IsPlaying())
        {
            m_parent->TakeDamage(1000);
        }
        else
        {
            Player *player = (Player *) m_parent->GetTarget();
            glm::vec2 pos = m_transformComponent->GetPosition() - m_transformComponent->GetPosition();
            float lengthSquared = (pos.x * pos.x) + (pos.y * pos.y);
            if (lengthSquared < 84.0f * 84.0f)
            {
                Enemy::EnemyType type = m_parent->GetType();
                Engine::Get()->GetEventManager()->Send("hit_player", (void *) &type);
            }
        }
    }
}

void MineComponent::FixedUpdate(double a_timeStep)
{
}

void MineComponent::OnCollision(IEntity *a_entity, unsigned char a_tag)
{
    if(m_state != 3)
    {
        if (a_tag & BULLET)
        {
            Bullet* bullet = dynamic_cast<Bullet*>(a_entity);
            if (m_parent->TakeDamage(bullet->GetDamage(), false))
            {
                m_spriteComponent->LoadAnimation("mine_death", true);
                m_state = 3;
            }
        }
        else if (a_tag & PLAYER)
        {
            if (m_parent->TakeDamage(1000, false))
            {
                m_state = 3;
                m_spriteComponent->LoadAnimation("mine_death", true);
                Enemy::EnemyType type = m_parent->GetType();
                Engine::Get()->GetEventManager()->Send("hit_player", (void *) &type);
            }
        }
    }
}

void MineComponent::OnDestroy()
{
    Engine::Get()->GetEntityManager()->CreateEntity<TimedSprite>(m_transformComponent->GetPosition(), "mine_hurt");
}
