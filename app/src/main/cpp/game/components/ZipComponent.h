#ifndef PEW_PEW_ZIPCOMPONENT_H
#define PEW_PEW_ZIPCOMPONENT_H


#include <components/Component.h>
#include <components/SpriteComponent.h>
#include <components/TransformComponent.h>
#include "IEnemyComponent.h"

class ZipComponent : public Component<ZipComponent>, public IEnemyComponent
{
public:
    ZipComponent();
    ~ZipComponent();

    void Load(Enemy* a_parent, const glm::vec2 &a_pos);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

    void OnDestroy() override;


private:
    TransformComponent* m_transformComponent;
    SpriteComponent* m_spriteComponent;

    int m_state;
    float m_elapse;
    float m_speed;
    float m_idleDuration;
    glm::vec2 m_heading;

    void OnCollision(IEntity *a_entity, unsigned char a_tag);
};


#endif //PEW_PEW_ZIPCOMPONENT_H