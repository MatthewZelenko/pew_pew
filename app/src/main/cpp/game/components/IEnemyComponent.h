#ifndef PEW_PEW_IENEMYCOMPONENT_H
#define PEW_PEW_IENEMYCOMPONENT_H

#include "enemies/Enemy.h"

class IEnemyComponent
{
public:
    IEnemyComponent() {}
    virtual ~IEnemyComponent() {}

    inline virtual void OnDestroy(){}


protected:
    Enemy* m_parent;


};


#endif //PEW_PEW_IENEMYCOMPONENT_H