#include <engine/Utilities.h>
#include <engine/components/CircleColliderComponent.h>
#include <game/CollisionTags.h>
#include <game/Player.h>
#include <game/TimedSprite.h>
#include <external/glm/gtx/rotate_vector.hpp>
#include "BasicComponent.h"
#include <EntityManager.h>
#include "weapons/Bullet.h"

BasicComponent::BasicComponent() : m_speed(200.0f), m_elapse(0.0f), m_state(0)
{
    m_idleDuration = (FastRand() % 100) / 100.0f + 0.5f;
}
BasicComponent::~BasicComponent()
{

}

void BasicComponent::Load(Enemy* a_parent, const glm::vec2 &a_pos)
{
    m_parent = a_parent;
    m_transformComponent = m_parent->GetTransformComponent();
    m_transformComponent->SetPosition(a_pos);
    m_transformComponent->SetZIndex(-100.0f);

    m_spriteComponent = m_parent->AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->LoadAnimation("basic_spawn", true, false);

    CircleColliderComponent* circle = m_parent->AddComponent<CircleColliderComponent>();
    circle->SetCircle(Circle(0, 0, 11.5f));
    circle->SetTag(ENEMY);
    circle->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};

    m_heading = glm::normalize(glm::vec2(FastRand() % 200 - 100, FastRand() % 200 - 100));
    m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));
}
void BasicComponent::Unload()
{
}

void BasicComponent::Update(double a_deltaTime)
{
    //Spawn
    if(m_state == 0)
    {
        if(!m_spriteComponent->IsPlaying())
        {
            m_state = 2;
            m_spriteComponent->LoadAnimation("basic_idle", true, true);
        }
    }
        //Swim
    else if(m_state == 1)
    {
        m_elapse -= a_deltaTime;
        if(m_elapse <= 0.0f)
        {
            m_elapse = m_idleDuration;
            m_state = 2;
            //TODO: Play idle animation
        }
        else
        {
            Player* player = (Player*)m_parent->GetTarget();
            assert(player && "BasicComponent: Player target missing!");

            int direction = 0;

            glm::vec2 heading = glm::normalize(player->GetTransformComponent()->GetPosition() - m_transformComponent->GetPosition());
            float dotProduct = glm::dot(m_heading, heading);
            if (dotProduct != 1)
            {
                if(dotProduct >= 0.9999f)
                {
                    m_heading = heading;
                    m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));
                }
                else if (dotProduct == -1) // <- ->
                {
                    direction = FastRand() % 3 - 1;
                }
                else
                {
                    glm::vec3 cross = glm::cross(glm::vec3(m_heading.x, m_heading.y, 0.0f), glm::vec3(heading.x, heading.y, 0.0f));

                    if (cross.z < 0) //Pointing away
                    {
                        direction = -1;
                        //reverse
                    }
                    else
                    {
                        direction = 1;
                    }
                }
            }

            if(direction != 0)
            {
                float rotateValue = (0.75f * glm::pi<float>()) * (float)a_deltaTime * direction * m_elapse;
                m_heading = glm::rotate(m_heading, rotateValue);
                m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));
            }
            m_transformComponent->AddPosition(m_heading * (m_speed * m_elapse) * (float)a_deltaTime);
        }
    }
    else if(m_state == 2) //Idle
    {
        m_elapse -= a_deltaTime;
        if(m_elapse <= 0.0f)
        {
            m_elapse = 1.0f;
            m_state = 1;
            //TODO: Play swim animation
        }
    }
}
void BasicComponent::FixedUpdate(double a_timeStep)
{
}

void BasicComponent::OnCollision(IEntity *a_entity, unsigned char a_tag)
{
    if(a_tag & BULLET)
    {
        Bullet* bullet = dynamic_cast<Bullet*>(a_entity);
        m_parent->TakeDamage(bullet->GetDamage());
        //KillSelf
    }
    else if(a_tag & PLAYER)
    {
        m_parent->TakeDamage(1000);
        Enemy::EnemyType type = m_parent->GetType();
        Engine::Get()->GetEventManager()->Send("hit_player", (void *) &type);
    }
}

void BasicComponent::OnDestroy()
{
    Engine::Get()->GetEntityManager()->CreateEntity<TimedSprite>(m_transformComponent->GetPosition(), "basic_hurt");
}
