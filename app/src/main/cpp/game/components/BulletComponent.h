#ifndef PEW_PEW_BULLETCOMPONENT_H
#define PEW_PEW_BULLETCOMPONENT_H

#include <external/glm/vec2.hpp>
#include "components/Component.h"
#include "IEntity.h"

class Bullet;

class BulletComponent : public Component<BulletComponent>
{
public:
    BulletComponent();
    ~BulletComponent();

    void Load(float a_speed, const glm::vec2 &a_heading);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

private:
    glm::vec2 m_heading;
    float m_speed;

};


#endif //PEW_PEW_BULLETCOMPONENT_H