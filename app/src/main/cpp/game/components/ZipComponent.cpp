#include <Utilities.h>
#include "Player.h"
#include "TimedSprite.h"
#include "CollisionTags.h"
#include "ZipComponent.h"
#include "weapons/Bullet.h"
#include <EntityManager.h>
#include <components/CircleColliderComponent.h>

ZipComponent::ZipComponent(): m_speed(300.0f), m_elapse(0.0f), m_state(0), m_idleDuration(2.0f)
{

}
ZipComponent::~ZipComponent()
{

}

void ZipComponent::Load(Enemy *a_parent, const glm::vec2 &a_pos)
{
    m_parent = a_parent;

    m_transformComponent = m_parent->GetTransformComponent();
    m_transformComponent->SetPosition(a_pos);
    m_transformComponent->SetZIndex(-150.0f);

    m_spriteComponent = m_parent->AddComponent<SpriteComponent>();
    m_spriteComponent->SetShader("simple");
    m_spriteComponent->LoadAnimation("zip_spawn", true);

    CircleColliderComponent* circle = m_parent->AddComponent<CircleColliderComponent>();
    circle->SetCircle(Circle(0, 0, 6.0f));
    circle->SetTag(ENEMY);
    circle->OnCollision = [this](IEntity* a_entity, unsigned char a_tag){this->OnCollision(a_entity, a_tag);};

    glm::vec2 heading = glm::normalize(glm::vec2(FastRand() % 200 - 100, FastRand() % 200 - 100));
    m_transformComponent->SetRotationRadians(-atan2(heading.x, heading.y));

}
void ZipComponent::Unload()
{
}

void ZipComponent::Update(double a_deltaTime)
{
    if(m_state == 0) //SPAWN
    {
        if(!m_spriteComponent->IsPlaying())
        {
            m_spriteComponent->LoadAnimation("zip_prepare", true);
            m_state = 1;
            m_elapse = m_idleDuration;
        }
    }
    else if(m_state == 1) //PREPARE
    {
        m_elapse -= a_deltaTime;

        Player* player = (Player*)m_parent->GetTarget();
        assert(player && "BasicEnemy: Player target missing!");
        m_heading = glm::normalize(player->GetTransformComponent()->GetPosition() - m_transformComponent->GetPosition());
        m_transformComponent->SetRotationRadians(-atan2(m_heading.x, m_heading.y));

        if(m_elapse <= 0.0f)
        {
            m_state = 2;
            m_spriteComponent->LoadAnimation("zip_pull", true, false);
        }
    }
    else if(m_state == 2) //PULL
    {
        if (!m_spriteComponent->IsPlaying())
        {
            m_spriteComponent->LoadAnimation("zip_idle", true);
            m_elapse = 0.25f;
            m_state = 3;
        }
        else
        {
            m_transformComponent->AddPosition(m_heading * m_speed * (float)a_deltaTime);
        }
    }
    else if(m_state == 3) //IDLE
    {
        m_elapse -= a_deltaTime;
        if(m_elapse <= 0.0f)
        {
            m_elapse = m_idleDuration;
            m_state = 1;
            m_spriteComponent->LoadAnimation("zip_prepare", true, false);
        }
        else
        {
            m_transformComponent->AddPosition(m_heading * m_speed * (float)a_deltaTime);
        }
    }
}
void ZipComponent::FixedUpdate(double a_timeStep)
{
    IComponent::FixedUpdate(a_timeStep);
}

void ZipComponent::OnCollision(IEntity *a_entity, unsigned char a_tag)
{
    if(a_tag & BULLET)
    {
        Bullet* bullet = dynamic_cast<Bullet*>(a_entity);
        m_parent->TakeDamage(bullet->GetDamage());
        //KillSelf
    }
    else if(a_tag & PLAYER)
    {
        m_parent->TakeDamage(1000);
        Enemy::EnemyType type = m_parent->GetType();
        Engine::Get()->GetEventManager()->Send("hit_player", (void *) &type);
    }
}

void ZipComponent::OnDestroy()
{
    Engine::Get()->GetEntityManager()->CreateEntity<TimedSprite>(m_transformComponent->GetPosition(), "zip_hurt");
}
