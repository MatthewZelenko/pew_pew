#include <Engine.h>
#include "ButtonComponent.h"
#include <Input.h>
#include <Camera.h>

ButtonComponent::ButtonComponent() : m_isActive(true), OnRelease(nullptr), OnPress(nullptr)
{

}
ButtonComponent::~ButtonComponent()
{

}

void ButtonComponent::Load(TransformComponent* a_parentTransform, const glm::vec2& a_position, const glm::vec2& a_size)
{
    m_parentTransform = a_parentTransform;
    m_position = a_position;
    m_size = a_size;
}
void ButtonComponent::Unload()
{
}
void ButtonComponent::Update(double a_deltaTime)
{
    //TODO::Change to input events in the input manager
    if(m_isActive)
    {
        if(OnPress)
        {
            if (Engine::Get()->GetInput()->TouchPressed())
            {
                glm::vec2 pos = Camera::MainCamera()->DisplayToGame(Engine::Get()->GetInput()->GetCurrentPosition());
                Rect rect(m_parentTransform->GetPosition() + m_position, m_size);
                if (rect.Contains(pos))
                {
                    OnPress();
                }
            }
        }
        else if(OnRelease)
        {
            if (Engine::Get()->GetInput()->TouchReleased())
            {
                glm::vec2 pos = Camera::MainCamera()->DisplayToGame(Engine::Get()->GetInput()->GetCurrentPosition());
                Rect rect(m_parentTransform->GetPosition() + m_position, m_size);
                if (rect.Contains(pos))
                {
                    OnRelease();
                }
            }
        }
    }
}