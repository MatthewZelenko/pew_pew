#include "PlayerControllerComponent.h"
#include <engine/Engine.h>
#include <Input.h>
#include <engine/Camera.h>
#include <engine/components/TransformComponent.h>
#include <game/VisualControl.h>
#include <engine/Log.h>
#include <game/Player.h>

PlayerControllerComponent::PlayerControllerComponent()
{

}
PlayerControllerComponent::~PlayerControllerComponent()
{

}

void PlayerControllerComponent::Load()
{
    a_visualControl = Engine::Get()->GetEntityManager()->CreateEntity<VisualControl>("visualControl", glm::vec2(32.0f, 32.0f));
    a_visualJoystick = Engine::Get()->GetEntityManager()->CreateEntity<VisualControl>("visualJoystick", glm::vec2(16.0f, 16.0f));
}
void PlayerControllerComponent::Unload()
{

}

void PlayerControllerComponent::Update(double a_deltaTime)
{
    Player* player = (Player*)GetParent();
    if(player->IsPaused())
        return;

    if(Engine::Get()->GetInput()->TouchDown())
    {
        glm::vec2 startPos = Engine::Get()->GetInput()->GetStartPosition();
        glm::vec2 currentPos = Engine::Get()->GetInput()->GetCurrentPosition();
        glm::vec2 diff = currentPos - startPos;
        float len = glm::length(diff);
        if(len == 0)
            return;

        if(a_visualControl->IsSpriteActive())
        {
            glm::vec2 norm = glm::normalize(diff);
            if(glm::length(diff) > 160.0f)
            {
                diff = norm * 160.0f;
            }
            a_visualJoystick->GetTransformComponent()->SetPosition(Camera::MainCamera()->DisplayToGame(startPos + diff));
            Move(a_deltaTime, norm);
        }
        else if(len >= 30.0f)
        {
            a_visualControl->SetSpriteActive(true);
            a_visualJoystick->SetSpriteActive(true);
            a_visualControl->GetTransformComponent()->SetPosition(Camera::MainCamera()->DisplayToGame(startPos));
            a_visualJoystick->GetTransformComponent()->SetPosition(Camera::MainCamera()->DisplayToGame(startPos));
        }
    }
    else if(Engine::Get()->GetInput()->TouchReleased())
    {
        a_visualControl->SetSpriteActive(false);
        a_visualJoystick->SetSpriteActive(false);
        Player* player = (Player*)GetParent();
        player->ChangeSprite(true);

    }
}
void PlayerControllerComponent::FixedUpdate(double a_timeStep)
{
}

void PlayerControllerComponent::Move(float a_deltaTime, const glm::vec2& a_direction)
{
    Player* player = (Player*)GetParent();
    player->Move(a_deltaTime, a_direction);
    if(!player->IsMoving())
        player->ChangeSprite(false);
}