#ifndef PEW_PEW_ENEMY_H
#define PEW_PEW_ENEMY_H

#include <engine/Entity.h>
#include <external/glm/vec2.hpp>

class IEnemyComponent;

class Enemy : public Entity<Enemy>
{
public:
    enum class EnemyType
    {
        BASIC,
        ZIP,
        MINE,
        CHAIN,
        SIZE
    };



    Enemy();
    virtual ~Enemy();

    void Load(EnemyType a_type, int a_health);
    void Unload() override;

    void Update(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;


    inline void AddEnemyComponent(IEnemyComponent* a_component) { m_enemyComponent = a_component; }


    EnemyType GetType() { return m_type; }

    inline int GetHealth() { return m_health; }
    inline void SetHealth(int a_hp) { m_health = a_hp; }
    bool TakeDamage(int a_hp, bool a_dieOnZero = true);

    virtual inline void AddMultipler(int a_value) { m_multiplier += a_value; }
    inline void SetMultiplier(int a_val) { m_multiplier = a_val; }
    inline int GetMultiplier() { return m_multiplier; }

    void DestroySelf();

    inline bool IsFollowingEnemy() { return m_followingEnemy; }


    inline void SetTarget(IEntity* a_target, bool a_isEnemy = false) { m_followingEnemy = a_isEnemy; m_target = a_target; }
    IEntity* GetTarget() { return m_target; }

    void OnDestroy();

private:
    IEnemyComponent* m_enemyComponent;

    EnemyType m_type;

    int m_health;
    int m_multiplier;

    bool m_followingEnemy;
    IEntity* m_target;
};
#endif //PEW_PEW_ENEMY_H