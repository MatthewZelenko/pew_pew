#include "Enemy.h"
#include <EntityManager.h>
#include "components/PlayerControllerComponent.h"
#include "scenes/GameScene.h"
#include "scenes/SceneManager.h"
#include <engine/Log.h>
#include "components/IEnemyComponent.h"
#include "CollisionTags.h"
#include "components/BasicComponent.h"
#include "components/ZipComponent.h"
#include "components/MineComponent.h"
#include "components/ChainComponent.h"

Enemy::Enemy() :
        m_type(EnemyType::BASIC),
        m_health(20),
        m_multiplier(1),
        m_target(nullptr),
        m_followingEnemy(false)
{

}
Enemy::~Enemy()
{

}

void Enemy::DestroySelf()
{
    OnDestroy();
    Engine::Get()->GetEntityManager()->DestroyEntity(GetHandle());
    ((GameScene*)SceneManager::GetScene())->m_gameManager.EnemyDestroyed(GetType(), GetTransformComponent()->GetPosition(), GetMultiplier());
}

bool Enemy::TakeDamage(int a_hp, bool a_dieOnZero)
{
    m_health -= a_hp;
    if (m_health <= 0)
    {
        if(a_dieOnZero) DestroySelf();
        return true;
    }
    return false;
}

void Enemy::Load(EnemyType a_type, int a_health)
{
    m_health = a_health;
    m_type = a_type;
}

void Enemy::Unload()
{

}

void Enemy::Update(double a_deltaTime)
{

}

void Enemy::FixedUpdate(double a_timeStep)
{

}

void Enemy::OnDestroy()
{
    m_enemyComponent->OnDestroy();
}