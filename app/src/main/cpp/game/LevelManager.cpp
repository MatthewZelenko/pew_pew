#include <android/asset_manager.h>
#include <Application.h>
#include <Log.h>
#include <rapidjson/document.h>
#include "LevelManager.h"
#include <EventManager.h>
#include <engine/Utilities.h>

LevelManager::LevelManager(): m_currentLevel(0), m_timeElapse(0), m_spawnTime(0), m_spawnElapse(0)
{

}

LevelManager::~LevelManager()
{

}

void LevelManager::Load()
{

    AAsset *file = AAssetManager_open(Application::m_androidState->activity->assetManager,
                                   "data/levels.dat", AASSET_MODE_BUFFER);
    if(file == nullptr)
        Log::Error("Level file not find: data/levels.dat");
    off_t size = AAsset_getLength(file);
    char *bytes = new char[size + 1];
    AAsset_read(file, bytes, size);
    bytes[size] = '\0';
    AAsset_close(file);

    m_levelData.clear();

    rapidjson::Document doc;
    doc.Parse(bytes);

    //ITERATE all level objects
    for (auto iter = doc.Begin(); iter != doc.End(); ++iter)
    {
        const rapidjson::Value& object = *iter;

        m_levelData.push_back(LevelData());

        if(object.HasMember("start_time"))
        {
            m_levelData[m_levelData.size() - 1].m_startTime = object["start_time"].GetInt();
        }
        if(object.HasMember("spawn_time_min"))
        {
            m_levelData[m_levelData.size() - 1].m_spawnTimeMin = object["spawn_time_min"].GetFloat();
        }
        if(object.HasMember("spawn_time_max"))
        {
            m_levelData[m_levelData.size() - 1].m_spawnTimeMax = object["spawn_time_max"].GetFloat();
        }
        if(object.HasMember("spawn_max"))
        {
            m_levelData[m_levelData.size() - 1].m_spawnMax = object["spawn_max"].GetInt();
        }
        if(object.HasMember("spawn"))
        {
            const auto spawnChancesArray = object["spawn"].GetArray();
            //ITERATE spawn enemies
            for (auto iterChances = spawnChancesArray.Begin(); iterChances != spawnChancesArray.End(); ++iterChances)
            {
                const rapidjson::Value& spawnObject = *iterChances;
                std::string name = spawnObject["name"].GetString();
                if(name == "basic")
                {
                    m_levelData[m_levelData.size() - 1].m_spawnChances[(int)Enemy::EnemyType::BASIC] = spawnObject["chance"].GetInt();
                }
                else if(name == "zip")
                {
                    m_levelData[m_levelData.size() - 1].m_spawnChances[(int)Enemy::EnemyType::ZIP] = spawnObject["chance"].GetInt();
                }
                else if(name == "mine")
                {
                    m_levelData[m_levelData.size() - 1].m_spawnChances[(int)Enemy::EnemyType::MINE] = spawnObject["chance"].GetInt();
                }
                else if(name == "chain")
                {
                    m_levelData[m_levelData.size() - 1].m_spawnChances[(int)Enemy::EnemyType::CHAIN] = spawnObject["chance"].GetInt();
                }
            }
        }
        if(object.HasMember("powerup_drop_chance"))
        {
            m_levelData[m_levelData.size() - 1].m_powerupDropChance = object["powerup_drop_chance"].GetFloat();
        }
        if(object.HasMember("powerups"))
        {
            const auto spawnChancesArray = object["powerups"].GetArray();
            //ITERATE spawn enemies
            for (auto iterChances = spawnChancesArray.Begin(); iterChances != spawnChancesArray.End(); ++iterChances)
            {
                const rapidjson::Value& powerupObject = *iterChances;
                std::string name = powerupObject["name"].GetString();
                if(name == "shotgun")
                {
                    m_levelData[m_levelData.size() - 1].m_powerupChances[(int)Powerup::PowerupType::SHOTGUN] = powerupObject["chance"].GetInt();
                }
                else if(name == "machinegun")
                {
                    m_levelData[m_levelData.size() - 1].m_powerupChances[(int)Powerup::PowerupType::MACHINEGUN] = powerupObject["chance"].GetInt();
                }
                else if(name == "shield")
                {
                    m_levelData[m_levelData.size() - 1].m_powerupChances[(int)Powerup::PowerupType::SHIELD] = powerupObject["chance"].GetInt();
                }
            }
        }
    }

    GetSpawnTime();

    delete[] bytes;
}

void LevelManager::FixedUpdate(double a_timeStep)
{
    if(m_currentLevel < m_levelData.size() - 1)
    {
        m_timeElapse += a_timeStep;
        if (m_timeElapse >= m_levelData[m_currentLevel + 1].m_startTime)
        {
            m_currentLevel++;
            m_spawnElapse = 0;
            GetSpawnTime();
        }
    }

    m_spawnElapse += a_timeStep;
    if (m_spawnElapse >= m_spawnTime)
    {
        m_spawnElapse -= m_spawnTime;
        GetSpawnTime();


        LevelData& data = m_levelData[m_currentLevel];
        Engine::Get()->GetEventManager()->Send("spawn", (void *) &data);
    }
}

void LevelManager::GetSpawnTime()
{
    float diff = m_levelData[m_currentLevel].m_spawnTimeMax - m_levelData[m_currentLevel].m_spawnTimeMin;
    m_spawnTime = m_levelData[m_currentLevel].m_spawnTimeMin + ((FastRand() % (int)(diff * 1000000000)) / 1000000000);
}