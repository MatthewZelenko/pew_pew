#ifndef PEW_PEW_BUTTON_H
#define PEW_PEW_BUTTON_H


#include "Entity.h"
#include "components/ButtonComponent.h"
#include <string>
#include <glm/vec2.hpp>

class Button : public Entity<Button>
{
public:
    Button();
    ~Button();

    void Load(const glm::vec2& a_position, std::string a_name, std::function<void()> a_onPress);
    void Unload() override;
    void Update(double a_deltaTime) override;
    void LateUpdate(double a_deltaTime) override;
    void FixedUpdate(double a_timeStep) override;

private:
    ButtonComponent* m_buttonComponent;
    Rect m_rect;
};


#endif //PEW_PEW_BUTTON_H