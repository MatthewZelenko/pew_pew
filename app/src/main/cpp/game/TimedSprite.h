#ifndef PEW_PEW_TIMEDSPRITE_H
#define PEW_PEW_TIMEDSPRITE_H

#include <Entity.h>
#include <glm/vec2.hpp>
#include <string>

class SpriteComponent;

class TimedSprite : public Entity<TimedSprite>
{
public:
    TimedSprite();
    ~TimedSprite();

    void Load(const glm::vec2& a_position, const std::string& a_animationName);
    void Unload() override;
    void FixedUpdate(double a_timeStep) override;

private:
    SpriteComponent* m_spriteComponent;

};


#endif //PEW_PEW_TIMEDSPRITE_H